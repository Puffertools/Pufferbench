# What is Pufferbench?

Pufferbench is a modular benchmark designed to measure the duration of commission and decommission operations on a given platform.
This benchmark has been designed with two goals in mind: 
*  Evaluate the viability of distributed storage system malleability on a given platform (more informations [here](https://gitlab.inria.fr/Puffertools/Pufferbench/wikis/PlatformEvaluation));
*  Help optimizing rescaling mechanisms to improve the malleability of distributed storage systems (quickly create prototypes of reconfiguration mechanisms by customizing Pufferbench's components).

These goals are achieved by emulating a distributed storage system, executing only the inputs and outputs needed to commission (add) or decommission (remove) storage nodes.
Pufferbench is independent of any distributed storage system, and thus can be used to quickly prototype rescaling mechanisms before implementing them into actual distributed storage systems. 

# How does Pufferbench work?

## The master node simulates a rescaling operation

The master node of Pufferbench (MPI rank 0) first uses a [MetadataGenerator](https://gitlab.inria.fr/Puffertools/Pufferbench/wikis/MetadataGenerator) to generate the number and size of objects that are stored on the emulated distributed storage system.
A [DataDistributionGenerator](https://gitlab.inria.fr/Puffertools/Pufferbench/wikis/DataDistributionGenerator) component then places the objects on virtual nodes to ensure that the replication factor of the data is matched.
With this data distribution, a [DataTransferScheduler](https://gitlab.inria.fr/Puffertools/Pufferbench/wikis/DataTransferScheduler) component executes the rescaling algorithms (commission and/or decommission) and records the sequence of I/Os (read, send, receive, write) that each node has to execute in order to complete this operation.

If one only wants to validate the post-conditions of their rescaling algorithms (replication factor, load balancing, etc.) they can stop at this point and Pufferbench only needs one node to run.

## The worker nodes replay the I/Os

To emulate the rescaling operations of a distributed storage system, the master node sends the sequences of I/Os to each worker node. The worker nodes then execute the sequence of I/Os without manipulating real objects: they read and write dummy data from storage, and transfer the data across the network.

The [Storage](https://gitlab.inria.fr/Puffertools/Pufferbench/wikis/Storage) component is in charge of reading and writing data from the storage. Before replaying the sequence of I/Os, it initializes some dummy data in the backend storage device in order to be able to read from it.
The [Network](https://gitlab.inria.fr/Puffertools/Pufferbench/wikis/Network) component is in charge of the communication with other worker nodes.
The [IODispatcher](https://gitlab.inria.fr/Puffertools/Pufferbench/wikis/IODispatcher) dispatches the sequence of I/Os to the [Storage](https://gitlab.inria.fr/Puffertools/Pufferbench/wikis/Storage) and the [Network](https://gitlab.inria.fr/Puffertools/Pufferbench/wikis/Network) components.

Information about each component can be found on their respective page.

# How to build and install

## Dependencies 

Pufferbench depends on the following:
1. CMake (3.6 or greater)
2. An MPI compiler and runtime
3. yaml-cpp (0.6.0 or greater)
4. (optional) cppunit (1.13 or greater)

## Building and installing

```
git clone git@gitlab.inria.fr:Pufferbench/Pufferbench.git
cd Pufferbench
mkdir build
cd build
cmake .. -DCMAKE_CXX_COMPILER=mpicxx -DCMAKE_INSTALL_PREFIX=/where/you/want/to/install
make
make install
```

## Using Spack to install Pufferbench

[Spack](https://spack.io/) can be used to install Pufferbench and its dependencies.

```
git clone https://gitlab.inria.fr/Pufferbench/Pufferbench-Spack.git
spack repo add Pufferbench-Spack
spack install Pufferbench
```

Note that this installation procedure is for users wanting to use Pufferbench without developping new components.

## Unit tests 

To run the unit test after customizing some components, simply use the following commands.
```
cmake .. -DCMAKE_CXX_COMPILER=mpicxx -DENABLE_TESTS=true
make
ctest --output-on-failure
```

# Running

To launch Pufferbench, simply type:
```
mpirun -np <X> -f <hostfile> Pufferbench -c <configuration file>
```

`<X>` is the number of MPI processes. `<host file>` is the list of nodes that should be used by Pufferbench.
`<configuration file>` is the configuration of Pufferbench, and is detailed in the next section.

Pufferbench has two other options.
To check the validity of a configuration file, the option to use is:
```
-t <configuration file>
```
To restart from a specific experiment:
```
-from <experiment number>
```

## Results

The data and results generated during the run of Pufferbench can be found in the `output_folder` of the configuration (Results by default).
Each time Pufferbench is started, it generates a subfolder with the date and time.
Within this folder, the file `results.csv` contains the durations of all the replays.
Each experiment also has its own folder that contains many informations about it: its configuration, the statistics of the replay, the logs, etc.  


|More informations can be found on [the wiki of the project](https://gitlab.inria.fr/Puffertools/Pufferbench/wikis/home)|
|---|


