/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include <iostream>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>


#include "Object.hpp"

using namespace std;
	
//USING_POINTERS;

class DataStructureTest : public CppUnit::TestFixture {
private:
	Object* obj1;

public:
	DataStructureTest() {}

	virtual ~DataStructureTest() {}
	
	void setUp(){
		obj1 = new Object(1,64);
	}

	void tearDown() {
		delete obj1;
	}

	static CppUnit::Test* GetTestSuite() {
		CppUnit::TestSuite *suiteOfTests = 
			new CppUnit::TestSuite("Template Test");
		
		suiteOfTests->addTest(new CppUnit::TestCaller<DataStructureTest>(
				"Object test",
				&DataStructureTest::ObjectTest ));

		return suiteOfTests;
	}

protected:
	void ObjectTest() {
		CPPUNIT_ASSERT(obj1->get_objectID() == 1);
		CPPUNIT_ASSERT(obj1->get_size() == 64);
	}
};

