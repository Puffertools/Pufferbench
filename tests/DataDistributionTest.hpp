/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include <iostream>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>

#include <vector>
#include <map>
#include <memory>

#include "Object.hpp"
#include "Node.hpp"
#include "DataSet.hpp"
#include "DataDistribution.hpp"
#include "ClusterInfo.hpp"

using namespace std;
	
class DataDistributionTest : public CppUnit::TestFixture {
private:
	ClusterInfo cluster_info = ClusterInfo(3);
	DataSet dataset = DataSet();
	vector<shared_ptr<Object>> objects;
	vector<shared_ptr<Node>> nodes;

public:
	DataDistributionTest() {}

	virtual ~DataDistributionTest() {}
	
	void setUp(){
		dataset.add_object(64);
		dataset.add_object(64);
		dataset.add_object(64);
		objects = dataset.get_objects();
		nodes = cluster_info.get_nodes();
	}

	void tearDown() {}

	static CppUnit::Test* GetTestSuite() {
		CppUnit::TestSuite *suiteOfTests = 
			new CppUnit::TestSuite("Template Test");
		
		suiteOfTests->addTest(new CppUnit::TestCaller<DataDistributionTest>(
				"No Data Host Test",
				&DataDistributionTest::NoData ));
		
		suiteOfTests->addTest(new CppUnit::TestCaller<DataDistributionTest>(
				"Set Host Test",
				&DataDistributionTest::SetHost ));

		suiteOfTests->addTest(new CppUnit::TestCaller<DataDistributionTest>(
				"Set Throw Test",
				&DataDistributionTest::SetThrow ));

		suiteOfTests->addTest(new CppUnit::TestCaller<DataDistributionTest>(
				"Multi Set Host Test",
				&DataDistributionTest::MultiSetHost ));

		suiteOfTests->addTest(new CppUnit::TestCaller<DataDistributionTest>(
				"Set Remove Host Test",
				&DataDistributionTest::SetRemoveHost ));

		suiteOfTests->addTest(new CppUnit::TestCaller<DataDistributionTest>(
				"Remove Throw Test",
				&DataDistributionTest::RemoveThrow ));

		suiteOfTests->addTest(new CppUnit::TestCaller<DataDistributionTest>(
				"Internals Test",
				&DataDistributionTest::Internals ));

		return suiteOfTests;
	}

protected:


	void NoData() {
		DataDistribution dd(cluster_info,dataset,3);

		CPPUNIT_ASSERT(!dd.host(nodes[0],objects[0]));
		CPPUNIT_ASSERT(!dd.host(nodes[1],objects[0]));
		CPPUNIT_ASSERT(!dd.host(nodes[2],objects[0]));
		CPPUNIT_ASSERT(!dd.host(nodes[0],objects[1]));
		CPPUNIT_ASSERT(!dd.host(nodes[1],objects[1]));
		CPPUNIT_ASSERT(!dd.host(nodes[2],objects[1]));
		CPPUNIT_ASSERT(!dd.host(nodes[0],objects[2]));
		CPPUNIT_ASSERT(!dd.host(nodes[1],objects[2]));
		CPPUNIT_ASSERT(!dd.host(nodes[2],objects[2]));
	}

	void SetThrow() {
		DataDistribution dd(cluster_info,dataset,3);	
		dd.set(objects[0],nodes[0]);
		CPPUNIT_ASSERT_THROW(dd.set(objects[0],nodes[0]),DataDistribution::DataAlreadyOnNodeException);
	}
	
	void RemoveThrow() {
		DataDistribution dd(cluster_info,dataset,3);	
		CPPUNIT_ASSERT_THROW(dd.remove(objects[0],nodes[0]),DataDistribution::DataNotPresentOnNodeException);
	}

	void SetRemoveHost() {
		DataDistribution dd(cluster_info,dataset,3);	

		dd.set(objects[0],nodes[1]);
		dd.remove(objects[0],nodes[1]);

		CPPUNIT_ASSERT(!dd.host(nodes[1],objects[0]));
	}

	void SetHost() {
		DataDistribution dd(cluster_info,dataset,3);	

		dd.set(objects[0],nodes[1]);

		CPPUNIT_ASSERT(dd.host(nodes[1],objects[0]));
	}

	void MultiSetHost() {
		DataDistribution dd(cluster_info,dataset,3);	

		dd.set(objects[0],nodes[1]);
		dd.set(objects[1],nodes[1]);
		dd.set(objects[2],nodes[2]);

		CPPUNIT_ASSERT(dd.host(nodes[1],objects[0]));
		CPPUNIT_ASSERT(dd.host(nodes[1],objects[1]));
		CPPUNIT_ASSERT(dd.host(nodes[2],objects[2]));
	}

	void Internals() {
		DataDistribution dd(cluster_info,dataset,3);	

		dd.set(objects[0],nodes[1]);
		dd.set(objects[1],nodes[1]);
		dd.set(objects[2],nodes[2]);

		dd.remove(objects[0],nodes[1]);
		dd.remove(objects[1],nodes[1]);
		dd.remove(objects[2],nodes[2]);

		CPPUNIT_ASSERT(dd.get_nodes_hosting_object(objects[0]).size() == 0);
		CPPUNIT_ASSERT(dd.get_nodes_hosting_object(objects[1]).size() == 0);
		CPPUNIT_ASSERT(dd.get_nodes_hosting_object(objects[2]).size() == 0);
		CPPUNIT_ASSERT(dd.get_objects_on_node(nodes[0]).size() == 0);
		CPPUNIT_ASSERT(dd.get_objects_on_node(nodes[1]).size() == 0);
		CPPUNIT_ASSERT(dd.get_objects_on_node(nodes[2]).size() == 0);
		
		dd.set(objects[0],nodes[1]);
		dd.set(objects[0],nodes[0]);
		dd.set(objects[1],nodes[1]);
		dd.set(objects[2],nodes[2]);
		
		CPPUNIT_ASSERT(dd.get_nodes_hosting_object(objects[0]).size() == 2);
		CPPUNIT_ASSERT(dd.get_nodes_hosting_object(objects[1]).size() == 1);
		CPPUNIT_ASSERT(dd.get_nodes_hosting_object(objects[2]).size() == 1);
		CPPUNIT_ASSERT(dd.get_objects_on_node(nodes[0]).size() == 1);
		CPPUNIT_ASSERT(dd.get_objects_on_node(nodes[1]).size() == 2);
		CPPUNIT_ASSERT(dd.get_objects_on_node(nodes[2]).size() == 1);
	}

};

