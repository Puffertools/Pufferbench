/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include <iostream>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <climits>
#include <memory>

#include "Operation.hpp"
#include "Order.hpp"
#include "InternalBuffer.hpp"

using namespace std;
	
class OperationTest : public CppUnit::TestFixture {
private:
	
public:
	OperationTest() {}

	virtual ~OperationTest() {}
	
	void setUp(){
	
	}

	void tearDown() {
	
	}

	static CppUnit::Test* GetTestSuite() {
		CppUnit::TestSuite *suiteOfTests = 
			new CppUnit::TestSuite("Template Test");
		
		suiteOfTests->addTest(new CppUnit::TestCaller<OperationTest>(
				"Operation Basic Tests",
				&OperationTest::BasicTests ));
		suiteOfTests->addTest(new CppUnit::TestCaller<OperationTest>(
				"Operation Buffer Tests",
				&OperationTest::LinkedOperationsTests ));
		suiteOfTests->addTest(new CppUnit::TestCaller<OperationTest>(
				"Operation Linked Operations Tests",
				&OperationTest::LinkedOperationsTests ));
		
		return suiteOfTests;
	}

protected:

	void BasicTests(){
		Order order;

		srand(time(NULL));

		for (int i = 0; i < 1000; ++i){
			OperationType type = RECV;
			int t = rand()%5;
			switch (t){
				case 0:
					type = RECV;
					break;
				case 1:
					type = SEND;
					break;
				case 2:
					type = READ;
					break;
				case 3: 
					type = WRITE;
					break;
				case 4:
					type = GET_BUFFER;
					break;
			}
			int tag = rand()%INT_MAX;
			int size = rand()%INT_MAX;
			int offset = rand()%INT_MAX;

			Operation* op = new Operation(&order, type, size, offset, tag);

			CPPUNIT_ASSERT(op->get_type() == type);
			CPPUNIT_ASSERT(op->get_tag() == tag);
			CPPUNIT_ASSERT(op->get_size() == size);
			CPPUNIT_ASSERT(op->get_offset() == offset);
			CPPUNIT_ASSERT(op->get_order() == &order);
			
			delete op;
		}
		// Buffer of size 0 invalid
		CPPUNIT_ASSERT_THROW(new Operation(&order,RECV,0,20,5),Operation::InvalidParametersException);
		// Buffer of negative size invalid
		CPPUNIT_ASSERT_THROW(new Operation(&order,RECV,-25,20,5),Operation::InvalidParametersException);
		// Negative offset, invalid
		CPPUNIT_ASSERT_THROW(new Operation(&order,RECV,20,-5,5),Operation::InvalidParametersException);
		// no order
		CPPUNIT_ASSERT_THROW(new Operation(nullptr,RECV,20,20,5),Operation::InvalidParametersException);
	}

	void LinkedOperationsTests(){
		Order order;
	
		shared_ptr<Operation> op1(new Operation(&order, RECV, 40,0,0));
		shared_ptr<Operation> op2(new Operation(&order, RECV, 40,0,0));
		shared_ptr<Operation> op3(new Operation(&order, RECV, 40,0,0));
		shared_ptr<Operation> op4(new Operation(&order, RECV, 40,0,0));
		shared_ptr<Operation> op5(new Operation(&order, RECV, 40,0,0));

		// All can be scheduled
		CPPUNIT_ASSERT(op1->can_be_scheduled());
		CPPUNIT_ASSERT(op2->can_be_scheduled());
		CPPUNIT_ASSERT(op3->can_be_scheduled());
		CPPUNIT_ASSERT(op4->can_be_scheduled());
		CPPUNIT_ASSERT(op5->can_be_scheduled());

		// 
		op4->add_next(op3);
		op5->add_next(op3);
		op3->add_next(op1);
		op5->add_next(op2);
		
		// Nexts
		CPPUNIT_ASSERT(op1->get_nexts().size() == 0);
		CPPUNIT_ASSERT(op2->get_nexts().size() == 0);
		CPPUNIT_ASSERT(op3->get_nexts().size() == 1);
		CPPUNIT_ASSERT(op4->get_nexts().size() == 1);
		CPPUNIT_ASSERT(op5->get_nexts().size() == 2);


		// 3 can be scheduled
		CPPUNIT_ASSERT(!op1->can_be_scheduled());
		CPPUNIT_ASSERT(!op2->can_be_scheduled());
		CPPUNIT_ASSERT(!op3->can_be_scheduled());
		CPPUNIT_ASSERT(op4->can_be_scheduled());
		CPPUNIT_ASSERT(op5->can_be_scheduled());
		
		op4->mark_finished();
		
		// 2 can be scheduled
		CPPUNIT_ASSERT(!op1->can_be_scheduled());
		CPPUNIT_ASSERT(!op2->can_be_scheduled());
		CPPUNIT_ASSERT(!op3->can_be_scheduled());
		CPPUNIT_ASSERT(op5->can_be_scheduled());
	
		op5->mark_finished();
		
		// 1 can be scheduled
		CPPUNIT_ASSERT(!op1->can_be_scheduled());
		CPPUNIT_ASSERT(op2->can_be_scheduled());
		CPPUNIT_ASSERT(op3->can_be_scheduled());

		op3->mark_finished();
		
		// 1 can be scheduled
		CPPUNIT_ASSERT(op1->can_be_scheduled());
		CPPUNIT_ASSERT(op2->can_be_scheduled());

	}
	
	void BufferTests(){
		Order order;
		char* buf = new char[40];
		shared_ptr<InternalBuffer> ib(new InternalBuffer(buf,40));
		
		Operation op(&order,GET_BUFFER,40,0,0);
		
		CPPUNIT_ASSERT(!op.is_buffer_initialized());
		CPPUNIT_ASSERT(!op.get_buffer());

		shared_ptr<InternalBuffer> ib2;
		CPPUNIT_ASSERT_THROW(op.set_buffer(ib2), Operation::InvalidParametersException);

		op.set_buffer(ib);
		
		CPPUNIT_ASSERT(op.is_buffer_initialized());
		CPPUNIT_ASSERT(op.get_buffer() == ib);
	
		CPPUNIT_ASSERT_THROW(op.set_buffer(ib), Operation::BufferAlreadyInitializedException);
		
		CPPUNIT_ASSERT_NO_THROW(op.mark_finished());
		CPPUNIT_ASSERT_THROW(op.mark_finished(), Operation::AlreadyMarkedFinishedException);
			
		delete[] buf;
	}

};

