/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include <iostream>
#include <memory>

#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>


#include "GlobalConfiguration.hpp"
#include "Config.hpp"

using namespace std;
	
class ConfigTest : public CppUnit::TestFixture {
private:
	GlobalConfiguration* gconf;

public:
	ConfigTest() {}

	virtual ~ConfigTest() {}
	
	void setUp(){
		gconf = new GlobalConfiguration();
		gconf->load(gconf->read("configuration_test.yml").c_str());	
	}

	void tearDown() {
		delete gconf;
	}

	static CppUnit::Test* GetTestSuite() {
		CppUnit::TestSuite *suiteOfTests = 
			new CppUnit::TestSuite("Template Test");
		
		suiteOfTests->addTest(new CppUnit::TestCaller<ConfigTest>(
				"Config Read test",
				&ConfigTest::SingleParameterTest ));
		
		suiteOfTests->addTest(new CppUnit::TestCaller<ConfigTest>(
				"Config generation test",
				&ConfigTest::ConfigGenerationTest ));

		return suiteOfTests;
	}

protected:

	void SingleParameterTest(){
		gconf->reset();

		gconf->get_next_configuration();
		gconf->get_next_configuration();
		gconf->get_next_configuration();
		Config c = gconf->get_next_configuration();
		
		cout << "read:" << c.get_parameter<int>({"tests", "initial_number_of_nodes"},-1) << endl;
		
		CPPUNIT_ASSERT(c.get_parameter<int>({"tests", "initial_number_of_nodes"},-1) == 4);
		CPPUNIT_ASSERT(c.get_parameter<int>({"initial_number_of_nodes"},-1) == -1);
		CPPUNIT_ASSERT(c.get_parameter<int>({"tests", "initial_number_of_nodes","truc","machin"},-1) == -1);
	
		CPPUNIT_ASSERT(c.get_parameter<string>({"components", "metadata_generator"},"") == "SimpleMetadataGenerator");
		CPPUNIT_ASSERT(c.get_parameter<bool>({"tests", "test_commission"},false));
		CPPUNIT_ASSERT(!c.get_parameter<bool>({"full_auto_test"},true));
		
	}

	void ConfigGenerationTest(){
		gconf->reset();
		int count = 0;
		while (gconf->has_next()){
			count++;
			gconf->get_next_configuration();
		}
		cout << count << endl;
		CPPUNIT_ASSERT(count == 36);
		gconf->reset();
		count = 0;
		while (gconf->has_next()){
			count++;
			gconf->get_next_configuration();
		}
		CPPUNIT_ASSERT(count == 36);
	}
};

