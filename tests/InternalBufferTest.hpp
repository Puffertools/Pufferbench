/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include <iostream>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <memory>

#include "InternalBuffer.hpp"

using namespace std;
	
class InternalBufferTest : public CppUnit::TestFixture {
private:
	
public:
	InternalBufferTest() {}

	virtual ~InternalBufferTest() {}
	
	void setUp(){
	
	}

	void tearDown() {
	
	}

	static CppUnit::Test* GetTestSuite() {
		CppUnit::TestSuite *suiteOfTests = 
			new CppUnit::TestSuite("Template Test");
		
		suiteOfTests->addTest(new CppUnit::TestCaller<InternalBufferTest>(
				"Internal Buffer Basic Tests",
				&InternalBufferTest::BasicTests ));
		suiteOfTests->addTest(new CppUnit::TestCaller<InternalBufferTest>(
				"Internal Buffer Multiple Children Tests",
				&InternalBufferTest::MultipleChildrenTests ));
		suiteOfTests->addTest(new CppUnit::TestCaller<InternalBufferTest>(
				"Internal Buffer Creation Tests",
				&InternalBufferTest::CreationTests ));
		
		return suiteOfTests;
	}

protected:

	void BasicTests(){
		char* buffer = new char[40];
		shared_ptr<InternalBuffer> ib(new InternalBuffer(buffer,40));

		// Basic tests related to recorded fields
		CPPUNIT_ASSERT(ib->get_size() == 40);
		CPPUNIT_ASSERT(ib->get_buffer_space() == buffer);
		// Internal buffer should be reserved at creation
		CPPUNIT_ASSERT(ib->get_number_token() == 1);
		// It is its own root
		CPPUNIT_ASSERT(ib->is_root());
		// It does not have any parent
		CPPUNIT_ASSERT(!ib->get_parent());

		ib->release_access();
		// Now it shouldn't have any token
		CPPUNIT_ASSERT(ib->get_number_token() == 0);
		// Cannot release too many times	
		CPPUNIT_ASSERT_THROW(ib->release_access(),InternalBuffer::AlreadyReleasedException);
		
		delete[] buffer;
		//*/
	}

	void MultipleChildrenTests(){
		char* buffer = new char[40];
		shared_ptr<InternalBuffer> ib(new InternalBuffer(buffer,40));

		// Get child 
		shared_ptr<InternalBuffer> ibc1(new InternalBuffer(ib,20,0));
		shared_ptr<InternalBuffer> ibc2(new InternalBuffer(ib,20,20));
		shared_ptr<InternalBuffer> ibc3(new InternalBuffer(ibc2,10,10));
		
		// Checkbuffer space
		CPPUNIT_ASSERT(ibc3->get_buffer_space() == buffer+30);
		CPPUNIT_ASSERT(ibc2->get_buffer_space() == buffer+20);
		CPPUNIT_ASSERT(ibc1->get_buffer_space() == buffer);

		// Check parents
		CPPUNIT_ASSERT(ibc3->get_parent() == ibc2);
		CPPUNIT_ASSERT(ibc2->get_parent() == ib);
		CPPUNIT_ASSERT(ibc1->get_parent() == ib);
	
		// Check sizes
		CPPUNIT_ASSERT(ib->get_size() == 40);
		CPPUNIT_ASSERT(ibc1->get_size() == 20);
		CPPUNIT_ASSERT(ibc2->get_size() == 20);
		CPPUNIT_ASSERT(ibc3->get_size() == 10);
		
		// is_root
		CPPUNIT_ASSERT(ib->is_root());
		CPPUNIT_ASSERT(!ibc1->is_root());
		CPPUNIT_ASSERT(!ibc2->is_root());
		CPPUNIT_ASSERT(!ibc3->is_root());

		// tokens 
		CPPUNIT_ASSERT(ib->get_number_token() == 4);
		CPPUNIT_ASSERT(ibc1->get_number_token() == 1);
		CPPUNIT_ASSERT(ibc2->get_number_token() == 2);
		CPPUNIT_ASSERT(ibc3->get_number_token() == 1);

		// ibc2 release access
		ibc2->release_access();

		// tokens 
		CPPUNIT_ASSERT(ib->get_number_token() == 3);
		CPPUNIT_ASSERT(ibc1->get_number_token() == 1);
		CPPUNIT_ASSERT(ibc2->get_number_token() == 1);
		CPPUNIT_ASSERT(ibc3->get_number_token() == 1);
		
		// ibc3 release access
		ibc3->release_access();
		CPPUNIT_ASSERT(ib->get_number_token() == 2);
		CPPUNIT_ASSERT(ibc1->get_number_token() == 1);
		CPPUNIT_ASSERT(ibc2->get_number_token() == 0);
		CPPUNIT_ASSERT(ibc3->get_number_token() == 0);

		delete[] buffer;
	}
	
	void CreationTests(){
		char* buffer = new char[40];

		// Buffer of size 0 is not accepted
		CPPUNIT_ASSERT_THROW(new InternalBuffer(buffer,0),InternalBuffer::InvalidParametersException);
		// Buffer of negative size is not accepted
		CPPUNIT_ASSERT_THROW(new InternalBuffer(buffer,-1),InternalBuffer::InvalidParametersException);
		// Buffer cannot be initialized with nullptr
		CPPUNIT_ASSERT_THROW(new InternalBuffer(nullptr,40),InternalBuffer::InvalidParametersException);
		
		shared_ptr<InternalBuffer> ib(new InternalBuffer(buffer,40));
		
		// Child cannot be larger than the parent
		CPPUNIT_ASSERT_THROW(new InternalBuffer(ib,41,0),InternalBuffer::InvalidParametersException);
		// Child cannot be defined outside of parent
		CPPUNIT_ASSERT_THROW(new InternalBuffer(ib,20,21),InternalBuffer::InvalidParametersException);
		// Child cannot be of size 0
		CPPUNIT_ASSERT_THROW(new InternalBuffer(ib,0,21),InternalBuffer::InvalidParametersException);
		// Child's offset cannot be negative
		CPPUNIT_ASSERT_THROW(new InternalBuffer(ib,20,-21),InternalBuffer::InvalidParametersException);

		delete[] buffer;
	}

};

