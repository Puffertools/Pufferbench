cmake_minimum_required (VERSION 3.6)
project (Pufferbench)

macro (use_cxx14)
	if (CMAKE_VERSION VERSION_LESS "3.1")
		if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
			set (CMAKE_CXX_FLAGS "--std=gnu++14 ${CMAKE_CXX_FLAGS}")
		else ()
			set (CMAKE_CXX_FLAGS "--std=c++14 ${CMAKE_CXX_FLAGS}")
		endif ()
	else ()
		set (CMAKE_CXX_STANDARD 14)
	endif ()
endmacro (use_cxx14)

use_cxx14 ()

find_package(Threads)

find_package(MPI)
include_directories(SYSTEM ${MPI_INCLUDE_PATH})


# Set module path
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} 
		"${Pufferbench_SOURCE_DIR}/cmake/Modules/")

# Find Yaml-CPP
find_package(YamlCpp)
if(YAMLCPP_FOUND)
	include_directories(${YAMLCPP_INCLUDE_DIR})
else(YAMLCPP_FOUND)
	message(ERROR "YamlCpp not found")
endif(YAMLCPP_FOUND)

# Find cppunit
find_package(CppUnit)
if(CPPUNIT_FOUND)
	message(STATUS "Found CPPUNIT " ${CPPUNIT_INCLUDE_DIR})
	include_directories(${CPPUNIT_INCLUDE_DIR})
	enable_testing()
else(CPPUNIT_FOUND)
	message(STATUS "CppUnit not found, unit tests will not be compiled")
endif(CPPUNIT_FOUND)

# check if Doxygen is installed
find_package(Doxygen)
if (DOXYGEN_FOUND)
    # set input and output files
    set(DOXYGEN_IN ${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile)
    set(DOXYGEN_OUT ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile)

    # request to configure the file
    configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)

    # note the option ALL which allows to build the docs together with the application
    add_custom_target( Documentation 
        COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Generating API documentation with Doxygen"
        VERBATIM )
else (DOXYGEN_FOUND)
	message("Doxygen need to be installed to generate the doxygen documentation")
endif (DOXYGEN_FOUND)

if (ENABLE_TESTS)
	set (CMAKE_CXX_FLAGS "-Wall -Wextra ${CMAKE_CXX_FLAGS}")
else (ENABLE_TESTS)
	set (CMAKE_CXX_FLAGS "-DNEDBUG -o3 ${CMAKE_CXX_FLAGS}")
endif(ENABLE_TESTS)

file(GLOB_RECURSE SRCS ${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src
					${CMAKE_CURRENT_SOURCE_DIR}/src/Master
					${CMAKE_CURRENT_SOURCE_DIR}/src/Master/DataStructures
					${CMAKE_CURRENT_SOURCE_DIR}/src/Master/DataDistributionValidator
					${CMAKE_CURRENT_SOURCE_DIR}/src/Master/MetadataGenerator
					${CMAKE_CURRENT_SOURCE_DIR}/src/Master/DataDistributionGenerator
					${CMAKE_CURRENT_SOURCE_DIR}/src/Master/DataTransferScheduler
					${CMAKE_CURRENT_SOURCE_DIR}/src/ReplayManager
					${CMAKE_CURRENT_SOURCE_DIR}/src/ReplayManager/IODispatcher
					${CMAKE_CURRENT_SOURCE_DIR}/src/ReplayManager/CommonReplay
					${CMAKE_CURRENT_SOURCE_DIR}/src/ReplayManager/Network
					${CMAKE_CURRENT_SOURCE_DIR}/src/ReplayManager/Storage
					${CMAKE_CURRENT_SOURCE_DIR}/src/Common
					${CMAKE_CURRENT_SOURCE_DIR}/src/Util)


list(FILTER SRCS EXCLUDE REGEX ".*pufferbench.cpp")

add_executable(Pufferbench src/pufferbench.cpp ${SRCS} ${HEADERS})

target_link_libraries(Pufferbench ${YAMLCPP_LIBRARY} ${MPI_C_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})

if (CPPUNIT_FOUND AND ENABLE_TESTS)
	message(STATUS "CppUnit found, unit tests will be compiled")
	add_subdirectory(tests)
endif(CPPUNIT_FOUND AND ENABLE_TESTS)

install(TARGETS Pufferbench RUNTIME DESTINATION bin)
