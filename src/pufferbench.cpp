/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include <sstream>
#include <iostream>

#include <mpi.h>

#include <exception>
#include <string>
#include <memory>

#include "Log.hpp"

#include "Config.hpp"
#include "GlobalConfiguration.hpp"
#include "ConfigurationKeys.hpp"

#include "ConfigurationBroadcaster.hpp"
#include "FileManager.hpp"

#include "Manager.hpp"


using namespace std;


string RESULT_FILE = "results.csv";

void usage(char * soft_name){
	cerr << "(usage) " << soft_name << " <options>" << endl;
	cerr << "\tOne of the following options MUST be chosen" << endl;
	cerr << "\t -c <configuration_file> Choose a configuration file." << endl;
	cerr << "\t -t <configuration_file> Test a configuration file." << endl;
	cerr << "\tOptional parameters" << endl; 
	cerr << "\t -from <exp_number> Start from an an experiment number other than 0." << endl;
}

int main(int argc, char * argv[]){

	MPI_Init(NULL,NULL);
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	
	bool test_conf = false;
	string conf_file = "";
	int start_from = 0;
	
	int wrong_args = 0;

	if (rank == 0){
		// Check options
		for (int i = 1; i < argc; ){
			if (strcmp(argv[i],"-c") == 0){
				if (i+1 == argc){
					usage(argv[0]);
					wrong_args = 1;
					break;
				}
				conf_file = string(argv[i+1]);
				i+=2;
				continue;
			}
			if (strcmp(argv[i],"-t") == 0){
				if (i+1 == argc){
					usage(argv[0]);
					wrong_args = 1;
					break;
				}
				conf_file = string(argv[i+1]);
				test_conf = true;
				Log::instance()->addLog(&cout, CONFIG | ERROR | WARN | IMPORTANT | INFO | TRACE, true);
				Log::instance()->initialize();
				i += 2;
				continue;
			}
			if (strcmp(argv[i],"-from") == 0){
				if (i+1 == argc){
					usage(argv[0]);
					wrong_args = 1;
					break;
				}
				start_from = stoi(argv[i+1]);
				i += 2;
				continue;
			}
			i++;
		}

		if (conf_file == ""){
			usage(argv[0]);
			wrong_args = 1;
		}
	} 

	// Broadcast the validity of the arguments
	MPI_Bcast(&wrong_args,1,MPI_INT,0,MPI_COMM_WORLD); 
	if (wrong_args != 0){
		MPI_Finalize();
		Log::instance()->closeLogs();
		return -1;
	}


	try {
		LogInfo("Using configuration file "+conf_file);

		// Load and broadcast the configuration
		GlobalConfiguration gconf = ConfigurationBroadcaster::instance()->bcast_config(conf_file.c_str());

		if (test_conf){			
			while (gconf.has_next()){
				Config conf = gconf.get_next_configuration();
				LogTrace(conf.get_csv());
			}

			LogInfo("Configuration file "+conf_file+" tested.");
			Log::instance()->closeLogs();
			return 0;
		}
		
		Config conf = gconf.get_next_configuration();

		// Read the configuration for the logs
		if (rank == 0){
			int log_level = Log::instance()->to_loglevel(
					conf.get_parameter<string>({ConfigurationKeys::LOG_SECTION,
												ConfigurationKeys::LOG_MASTER_KEY},
												ConfigurationKeys::LOG_MASTER_DEFAULT)
				);
			Log::instance()->addLog(&cout, log_level, true);	
		} else {
			int log_level = Log::instance()->to_loglevel(
					conf.get_parameter<string>({ConfigurationKeys::LOG_SECTION,
												ConfigurationKeys::LOG_SLAVES_KEY},
												ConfigurationKeys::LOG_SLAVES_DEFAULT)
				);
			Log::instance()->addLog(&cout, log_level, true);
		}
		
		int log_level = Log::instance()->to_loglevel(
				conf.get_parameter<string>({ConfigurationKeys::LOG_SECTION,
											ConfigurationKeys::LOG_FILES_KEY},
											ConfigurationKeys::LOG_FILES_DEFAULT)
			);
		ostringstream internal_log;
		Log::instance()->addLog(&internal_log,log_level, true);
		bool forced_sync = conf.get_parameter<bool>({ConfigurationKeys::LOG_SECTION,
											ConfigurationKeys::LOG_FORCED_SYNC_KEY},
											ConfigurationKeys::LOG_FORCED_SYNC_DEFAULT);
		Log::instance()->set_force_sync(forced_sync);
		
		// Create the output folders
		string output_dir = conf.get_parameter<string>({ConfigurationKeys::OUTPUT_DIR_KEY},
															ConfigurationKeys::OUTPUT_DIR_DEFAULT);
		FileManager::instance()->create_output_dir(output_dir);
		FileManager::instance()->add_local_log(log_level);

		// Read conf file for log coloring
		bool log_coloring = conf.get_parameter<bool>({ConfigurationKeys::LOG_SECTION,
											ConfigurationKeys::LOG_COLOR_KEY},
											ConfigurationKeys::LOG_COLOR_DEFAULT);

		// Indicate to the logs that it has been configured
		Log::instance()->initialize(log_coloring);

		gconf.reset();

		if (rank == 0){
			// Print conf 
			FileManager::instance()->print_global_file("configuration.yml", 
														gconf.read(conf_file.c_str()));
		}

		// Init global results on master
		int rank;
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);

		// Iterate on the configuration
		Manager manager(log_level,RESULT_FILE);
		
		LogInfo("Starting the manager");

		manager.run(gconf,start_from);

		LogInfo("Iteration on the configuration finished");

		// Retreive conf	
		LogTrace("Sending logs to master");
		Log::instance()->flush();
		FileManager::instance()->gather_logs(&internal_log);

	} catch (exception& e) {
		LogError("Exception caught:");
		LogError(e.what());
	}

	MPI_Finalize();
	
	FileManager::instance()->close_local_log();

	Log::instance()->closeLogs();

	return 0;
}


/**
 * \mainpage[Pufferbench]
 *
 * \section overview What is Pufferbench?
 *
 * Pufferbench is a modular benchmark designed to measure the duration of commission and decommission operations on a given platform.
 * This benchmark has been designed with two goals in mind: 
 * - Evaluate the viability of distributed storage system malleability on a given platform (more informations here);
 * - Help optimizing migration mechanisms to improve the malleability of distributed storage systems (quickly create prototypes of reconfiguration mechanisms by customizing Pufferbench's components).
 *
 *
 * These goals are achieved by emulating a distributed storage system, executing only the inputs and outputs needed to commission (add) or decommission (remove) storage nodes.
 * Pufferbench is independent of any distributed storage system, and thus can be used to quickly prototype rescaling mechanisms before implementing them into actual distributed storage systems.
 *
 * More informations can be found at https://gitlab.inria.fr/cheriere/Pufferbench/wikis/home.
 */
