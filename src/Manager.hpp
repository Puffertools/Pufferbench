/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef MANAGER_HPP
#define MANAGER_HPP

#include <string>

class Config;
class GlobalConfiguration;
/**
 * \brief Oversee the various runs of Pufferbench
 *
 * 
 */
class Manager {
private:
	/// Level of details requested for the logs
	int log_levels;
	/// Name of the file with the summary of the results
	std::string result_file;
	/// Total number of experiments
	int total_nb_experiments;

	/**
	 * Run one run
	 *
	 * \param conf Configuration of this run
	 */
	void run_one(Config& conf);

public:
	/**
	 * Constructor 
	 *
	 * \param log_levels Level of detail requested for the logs
	 * \param result_file Name of the file with the summary of the results.
	 */
	Manager(int log_levels, const std::string& result_file);

	/**
	 * Launch successively all runs configured in the configuration.
	 *
	 * \param gconf Global configuration
	 * \param start_from Start from the experiment with that number
	 */
	void run(GlobalConfiguration& gconf, int start_from);
};

#endif
