/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "Operation.hpp"

#include "Log.hpp"

#include "Order.hpp"
#include "InternalBuffer.hpp"

using namespace std;

OperationType Operation::get_type(){
	return type;
}

vector<shared_ptr<Operation>>& Operation::get_nexts(){
	return next;
}

Operation::Operation(Order* _order, OperationType t, int _size, int _offset, int _tag):
				order(_order), offset(_offset), size(_size), tag(_tag), type(t){
	if (!order){
		LogError("The pointer to order must be initialized.");
		throw InvalidParametersException();
	}
	if (size <= 0){
		LogError("The size can not be negative or null.");
		throw InvalidParametersException();
	}
	if (offset < 0){
		LogError("The offset cannot be negative.");
		throw InvalidParametersException();
	}

}

void Operation::set_buffer(const shared_ptr<InternalBuffer>& _buffer){
	if (!_buffer){
		LogError("The buffer must be initialized.");
		throw InvalidParametersException();
	}
	if (buffer){
		LogError("The operation already has a buffer set.");
		throw BufferAlreadyInitializedException();
	}
	buffer=_buffer;
}

bool Operation::is_buffer_initialized(){
	return !(!buffer);
}

int Operation::get_tag(){
	return tag;
}

int Operation::get_size(){
	if (!buffer){
		return size;
	}	
	return buffer->get_size();
}

int Operation::get_offset(){
	return offset;
}

void Operation::add_next(const shared_ptr<Operation>& op){
	op->new_prerequisite();
	next.push_back(op);
}

Order* Operation::get_order(){
	return order;
}

const shared_ptr<InternalBuffer>& Operation::get_buffer(){
	return buffer;
}

void Operation::new_prerequisite(){
	prerequisites_to_finish++;
}

void Operation::prerequisite_finished(){
	prerequisites_to_finish--;
	if (prerequisites_to_finish < 0){
		LogError("Function prerequisite_finished has been called more than new_prerequisite.");
		throw TooManyPrerequisitesFinishedException();
	}
}

bool Operation::can_be_scheduled(){
	return prerequisites_to_finish == 0;
}

void Operation::mark_finished(){
	if (marked){
		LogError("The operation has already been marked as finished.");
		throw AlreadyMarkedFinishedException();	
	}
	vector<shared_ptr<Operation>>::iterator it;
	for (it = next.begin(); it != next.end(); ++it){
		(*it)->prerequisite_finished();
	}
	marked = true;
}
