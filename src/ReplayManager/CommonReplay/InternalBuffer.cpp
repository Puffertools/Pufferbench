/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "InternalBuffer.hpp"

#include "Log.hpp"

using namespace std;

InternalBuffer::InternalBuffer(char* _buffer,int _size):buffer(_buffer),size(_size){
	if (buffer == nullptr){
		LogError("InternalBuffer's buffer cannot be initialized to nullptr.");
		throw InvalidParametersException();
	}
	if (size <= 0){
		LogError("InternalBuffer's size should not be null nor negative.");
		LogError(" - InternalBuffer's size: "+to_string(size));
		throw InvalidParametersException();
	}
}

InternalBuffer::InternalBuffer(const std::shared_ptr<InternalBuffer>& _parent, int _size, int offset){
	if (!_parent){
		LogError("The parent buffer must be initialized.");
		throw InvalidParametersException();
	}
	if (offset+_size > _parent->get_size()){
		LogError("The requested child do not fit in the parent InternalBuffer.");
		LogError(" - Parent size: "+to_string(size));
		LogError(" - Requested size: "+to_string(_size));
		LogError(" - Requested offset: "+to_string(offset));
		throw InvalidParametersException();
	}
	if (_size <= 0){
		LogError("The requested size for the InternalBuffer child ("+to_string(_size)+
			") is invalid.");
		throw InvalidParametersException();
	}
	if (offset < 0){
		LogError("The requested offset for the InternalBuffer child ("+to_string(offset)+
			") is invalid.");
		throw InvalidParametersException();
	}
	size = _size;
	buffer = _parent->get_buffer_space()+offset;
	parent = _parent;
	parent->reserve_access();
}

InternalBuffer::~InternalBuffer(){}

char* InternalBuffer::get_buffer_space(){
	return buffer;
}

void InternalBuffer::reserve_access(){
	if (parent != nullptr){
		parent->reserve_access();
	}
	token++;
}

void InternalBuffer::release_access(){
	if (parent != nullptr){
		parent->release_access();
	}
	token--;
	if (token < 0){
		LogError("InternalBuffer has been released too many times.");
		throw AlreadyReleasedException();
	}
}

int InternalBuffer::get_number_token(){
	return token;
}

int InternalBuffer::get_size(){
	return size;
}


bool InternalBuffer::is_root(){
	return !parent;
}

const shared_ptr<InternalBuffer>& InternalBuffer::get_parent(){
	return parent;
}
