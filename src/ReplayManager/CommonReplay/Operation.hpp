/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef OPERATION_HPP
#define OPERATION_HPP

#include <vector>
#include <exception>
#include <memory>

struct Order;
class InternalBuffer;

/**
 * Type of the operation, it can either be a read from storage,
 * a send through the network, a receive by the network, 
 * a write to storage, or to get a buffer from the storage.
 */
enum OperationType {
		READ, 
		SEND, 
		RECV, 
		WRITE, 

		/**
		 * Type Get_Buffer gets a buffer for all following operations,
		 * Warning, the offsets of the buffers of subsequent operations 
		 * must be relative to this main buffer.
		 */
		GET_BUFFER,
		/// Send an acknowledgement
		ACK_SEND,
		/// Receive an acknowledgement
		ACK_RECV};

/**
 * \brief Basic operation used for the replay.
 *
 * Class describing an operation to be executed during the replay.
 */
class Operation {
private:
	/**
	 * Order that is at least partially executed by this operation.
	 */
	Order* order;

	/**
	 * Buffer.
	 */
	std::shared_ptr<InternalBuffer> buffer;
	
	/** 
	 * Offset to use to set the buffer when initialized.
	 */
	int offset;
	/**
	 * Size of the buffer to set when initialized.
	 */
	int size;

	/**
	 * Operations that can only start after this one has been completed.
	 * Typically, a write on storage after reception.
	 */
	std::vector<std::shared_ptr<Operation>> next;

	/**
	 * Number of operations that must be completed before launching this one.
	 */
	int prerequisites_to_finish = 0;

	/**
	 * Tag of the operation, usefull for the network operations.
	 */
	int tag;

	/**
	 * Type of the operation.
	 */
	OperationType type;

	/// Indicates whether the operation has been marked as finished
	bool marked = false;

public:
	/**
	 * Constructor of an operation without a buffer.
	 * Particularly useful to avoid needless copies in the case of an in memory storage.
	 *
	 * Throws InvalidParametersException if the parameters
	 * are invalid.
	 *
	 * \param _order Order executed by this operation
	 * \param t Type of the operation
	 * \param _size Size of the required buffer
	 * \param _offset Offset to use to initialize the buffer
	 * \param _tag tag of the operation
	 */
	Operation(Order* _order, OperationType t, int _size, int _offset, int _tag);	

	/**
	 * Initialize the buffer. Can only be done once.
	 * 
	 * Throws BufferAlreadyInitializedException if called twice.
	 *
	 * \param buffer InternalBuffer to be used by the operation.
	 */
	void set_buffer(const std::shared_ptr<InternalBuffer>& buffer);
	
	/** 
	 * Indicates whether the buffer is initialized.
	 * \return true if the buffer is initialized.
	 */
	bool is_buffer_initialized();

	/**
	 * Return the type of this operation.
	 * \return type of the operation.
	 */
	OperationType get_type();
	
	/**
	 * Return the vector of operations that can only start
	 * when this operation is completed.
	 * \return vector of operations 
	 */
    std::vector<std::shared_ptr<Operation>>& get_nexts(); 

	/**
	 * Return the tag associated to this operation,
	 * important for network operations.
	 * \return tag of the operation.
	 */
	int get_tag();

	/**
	 * Mark the operation as finished, and update 
	 * the number of prerequiered task of the tasks 
	 * that can only start after this one is completed.
	 */
	 void mark_finished();
	
	/**
	 * Indicates if the operation can be scheduled 
	 * i.e. if all prerequiered operations have been
	 * marked as finished.
	 * \return true if the operation can be scheduled.
	 */
	bool can_be_scheduled();

	/**
	 * Return the requested offset to initialize
	 * the buffer, which is the offset of that chunk
	 * in the object.
	 * \return the offset of the buffer
	 */
	int get_offset();
	/** 
	 * Return the size of the requested buffer,
	 * or of the actual buffer if initialized.
	 * \return size of the buffer.
	 */
	int get_size();

	/**
	 * Add another operation that can only be executed after this
	 * Operation is completed.
	 * \param op Operation to launch after this one is completed
	 */
	void add_next(const std::shared_ptr<Operation>& op);
	
	/**
	 * Return the order to which correspond the operation.
	 * \return order related to the operation.
	 */
	Order* get_order();

	/**
	 * Return the buffer used for the operation.
	 * \return the buffer used for the oepration.
	 */
    const std::shared_ptr<InternalBuffer>& get_buffer();

	/**
	 * \brief Exception raised when too many prerequisites are finished
	 *
	 * Exception thrown when more prerequisites are finished than 
	 * the number of prerequisites initialized.
	 */
	class TooManyPrerequisitesFinishedException: public std::exception {};
	
	/**
	 * \brief Exception raised when the buffer is initialized twice.
	 *
	 * Exception thrown when the buffer is initialized more than once.
	 */
	class BufferAlreadyInitializedException: public std::exception {};
	
	/**
	 * \brief Exception raised when invalid parameters are given
	 *
	 * Exception thrown when the parameters are invalid.
	 */
	class InvalidParametersException: public std::exception {};

	/**
	 * \brief Exception raised when the operation is marked finished twice.
	 *
	 * Exception thrown when the operation is marked as finished twice.
	 */
	class AlreadyMarkedFinishedException: public std::exception {};

protected:
	/**
	 * Count one more operation that must finish before launching this one.
	 * Only used in add_next().
	 */
	void new_prerequisite();

	/**
	 * Decrease the amount of prerequired operations by one.
	 * Only used in mark_finished()
	 *
	 * Throws TooManyPrerequisitesFinishedException if used
	 * more than new_prerequisite()
	 */
	void prerequisite_finished();
};

#endif
