/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef INTERNALBUFFER_HPP
#define INTERNALBUFFER_HPP

#include <exception>
#include <memory>

/**
 * \brief Buffer used during the replay to store the data.
 *
 * Buffer used in the replay
 */
class InternalBuffer {
private:
	/**
	 * Number of descendant using this InternalBuffer, including itself.
	 * The buffer can be safely deleted only when 
	 * all descendant and itself have been released.
	 */
	int token = 1;

	/** 
	 * Actual buffer for all kinds of IO
	 */
	char* buffer = nullptr;
	
	/**
	 * Size of the buffer, must be an int because of MPI
	 */
	int size;
	
	/**
	 * Direct parent of this InternalBuffer:
	 * this means that the buffer is part of the buffer of the 
	 * parent
	 */
	std::shared_ptr<InternalBuffer> parent;

public:

	/**
	 * Constructor of the InternalBuffer
	 *
	 * Raise the exception InvalidParametersException
	 * if the provided parameters cannot be satisfied.
	 *
	 * \param _buffer buffer
	 * \param _size size of the buffer
	 */
	InternalBuffer(char* _buffer,int _size);

	/**
	 * Creates an InternalBuffer of the requested size
	 * that is part of the parent with the
	 * specified offset.
	 *
	 * Raise the exception InvalidParametersException
	 * if the provided parameters cannot be satisfied.
	 *
	 * \param _parent Parent of the new InternalBuffer
	 * \param _size size of the sub InternalBuffer
	 * \param offset offset at which it must be created 
	 * in the buffer
	 */
	InternalBuffer(const std::shared_ptr<InternalBuffer>& _parent, int _size, int offset);

	/**
	 * Destructor of InternalBuffer
	 */
	~InternalBuffer();

	/**
	 * Return the size of the InternalBuffer
	 * \return size of the InternalBuffer
	 */
	int get_size();

	/** 
	 * Return the pointer to the actual buffer
	 * \return pointer to the buffer
	 */
	char* get_buffer_space();

	/**
	 * Return the number of descendant using this buffer,
	 * including itself.
	 * The InternalBuffer can be safely deleted if it returns 0.
	 * \return The number of InternalBuffers related to this one
	 */
	int get_number_token();
	
	/**
	 * Indicates that the InternalBuffer is not needed anymore.
	 * Must only be called once except by its children.
	 *
	 * Raise AlreadyReleasedException if the buffer is released 
	 * too many times.
	 */
	void release_access();

	/**
	 * Indicates if the InternalBuffer has no parent,
	 * in this case, the actual buffer (buffer) must also
	 * be freed
	 * \return true if the InternalBuffer has no parent
	 */
	bool is_root();

	/**
	 * Return a pointer to the parent or nullptr.
	 * \return pointer to the parent or nullptr
	 */
    const std::shared_ptr<InternalBuffer>& get_parent();

	/**
	 * \brief Exception raised when invalid parameters are given
	 *
	 * Exception raised if the parameters given are not 
	 * valid.
	 */
	class InvalidParametersException: public std::exception {};
	
	/**
	 * \brief Exception raised when parents are redefined.
	 *
	 * Exception raised if parents are set more than once.
	 */
	class AlreadyHasAParentException: public std::exception {};
	
	/**
	 * \brief Exception raised when the buffer is freed too many times.
	 *
	 * Exception raised if the buffer is released too many times.
	 */
	class AlreadyReleasedException: public std::exception {};

protected:
	/**
	 * Add a token, used only by children to update
	 * their ascendants number of tokens.
	 * Usend only in set_parent
	 */
	void reserve_access();
};

#endif
