/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifdef __linux__

#include "OnDriveStorageNoCache.hpp"

#include <assert.h>
#include <string>
#include <stdlib.h>
#include <fstream>
#include <vector>
#include <map>


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>


#include "Config.hpp"

#include "Order.hpp"

#include "Operation.hpp"
#include "InternalBuffer.hpp"

#include "Log.hpp"
#include "Defs.hpp"

using namespace std;

REGISTER_STORAGE(OnDriveStorageNoCache)

OnDriveStorageNoCache::OnDriveStorageNoCache(Config& conf):
	AbstractStorage(conf){}

void OnDriveStorageNoCache::init(){
	LogInfo("OnDriveStorageNoCache: Initialization");
	// Buffer in memory
	buffer_size = conf.get_size_parameter({CONF_SECTION,CONF_ALLOCATED_MEMORY},
													DEFAULT_ALLOCATED_MEMORY);
	LogInfo("|- Setting internal buffer size to "+Log::to_str(buffer_size)+".");
	assert(buffer_size > 0);
	buffer = (char*)malloc(buffer_size);
	memset(buffer,'a',buffer_size);
	assert(buffer != nullptr);

	// Space on drive
	path_to_file = conf.get_parameter<string>({CONF_SECTION,CONF_PATH_TO_FILE},
												DEFAULT_PATH_TO_FILE);
	file_size = conf.get_size_parameter({CONF_SECTION,CONF_ALLOCATED_DRIVE},
													DEFAULT_ALLOCATED_DRIVE);
	reuse_file = conf.get_parameter<bool>({CONF_SECTION,CONF_REUSE_FILE},
													DEFAULT_REUSE_FILE);
	LogInfo("|- File to use "+path_to_file+".");
	LogInfo("|- File size "+Log::to_str(file_size)+".");
	if (reuse_file){
		LogInfo("|- Reuse file.");
	} else {
		LogInfo("|- Do not reuse file.");
	}
	assert(file_size > 0);

	int fd;
	fd = open(path_to_file.c_str(), O_CREAT | O_WRONLY | O_SYNC | O_LARGEFILE, 
			S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (fd == -1){
		LogError("Could not open file");
		assert(false);
	}

	size_t size = lseek64(fd,0,SEEK_END);
	LogInfo("|- Existing file has a size of "+Log::to_str(size));

	if (size < file_size){
		size_t content_size = 128L*1024*1024;
		if (content_size > file_size)
			content_size = file_size;
	
		vector<char> content(content_size);
	
		size_t written = size;
		
		while (written < file_size){
			size_t to_write = content_size;
			if (to_write > file_size - written)
				to_write = file_size - written;
			size_t dw = write(fd,content.data(),to_write);
			written += dw;
		}
		size = lseek64(fd,0,SEEK_END);
		LogInfo("|- File initialized to a size of "+Log::to_str(size));
	}
	
	if (-1 == posix_fadvise64(fd, 0, file_size, POSIX_FADV_DONTNEED))
		LogImportant("fadvise error");
	
	close(fd);


	// Start thread
	LogInfo("Starting loop");
	main_loop = thread(&OnDriveStorageNoCache::loop, this);
}

void OnDriveStorageNoCache::terminate(){	
	LogInfo("OnDriveStorageNoCache: Termination");
	thread_run = false;
	// join thread;
	main_loop.join();	
	
	if (!reuse_file){
		if (remove(path_to_file.c_str()) != 0){
			LogError("|- Error while removing temporary file");	
		} else {
			LogInfo("|- File removed");
		}
	} else {
		LogInfo("|- File kept for next runs");
	}
	free(buffer);
	LogStorage("|-  There are "+to_string(active_buffers)+" active buffer(s).");
}

OnDriveStorageNoCache::~OnDriveStorageNoCache(){}

void OnDriveStorageNoCache::assign_buffer_children(const shared_ptr<Operation>& op, 
												const shared_ptr<InternalBuffer>& parent_buffer){
	for (shared_ptr<Operation> next_op : op->get_nexts()){
		// For each child op
		if (!next_op->is_buffer_initialized()){
			shared_ptr<InternalBuffer> child = make_shared<InternalBuffer>(parent_buffer,
												next_op->get_size(),next_op->get_offset());
			next_op->set_buffer(child);
			assign_buffer_children(next_op,parent_buffer);
		}
	}
}

void OnDriveStorageNoCache::loop(){
	
	map<int,vector<shared_ptr<Operation>>,std::greater<int>> operations;
	vector<shared_ptr<Operation>> to_add;

	map<ObjectID,size_t> object_offset;

	int fd;

	fd = open(path_to_file.c_str(), O_RDWR | O_LARGEFILE | O_NOATIME | O_DSYNC);
	if (fd == -1){
		LogError("Could not open file");
		assert(false);
	}

	loop_idle = false;

	while (thread_run){
		
		// Gather read operations into operations
		read_operations_mutex.lock();
		if (!read_operations.empty())
			loop_idle = false;
		read_operations.swap(to_add);
		read_operations_mutex.unlock();
	
		for (shared_ptr<Operation> op : to_add){
			int key = op->get_order()->priority;
			operations[key].push_back(op);
		}
		to_add.clear();
		
		// Gather write operations
		write_operations_mutex.lock();
		if (!write_operations.empty())
			loop_idle = false;
		write_operations.swap(to_add);
		write_operations_mutex.unlock();
	
		for (shared_ptr<Operation> op : to_add){
			int key = op->get_order()->priority;
			operations[key].push_back(op);
		}
		to_add.clear();
		
		if (operations.empty()){
			loop_idle = true;
			continue;
		}

		// Take the first element in operations 
		shared_ptr<Operation> op;
		assert(!operations.begin()->second.empty());
		
		op = operations.begin()->second[0];
		operations.begin()->second.erase(operations.begin()->second.begin());
	
		if (operations.begin()->second.empty()){
			// Remove entry in the map
			operations.erase(operations.begin());
		}

		// Set random offset for the object
		size_t offset = 0;
		ObjectID obj_id = op->get_order()->object_id;
		if (object_offset.find(obj_id)!=object_offset.end()){
			// Get recorded position of the object and add relative offset
			offset = object_offset[obj_id] + op->get_offset();
		} else {
			// Create new offset
			offset = (rand()%((file_size-op->get_order()->size)/4096))*4096;
			object_offset[obj_id] = offset;
			// Add relative position 
			offset += op->get_offset();
		}

		size_t chunk_size = op->get_size();
		assert(offset + chunk_size < file_size);
		size_t off = lseek64(fd, offset, SEEK_SET);
        (void) off; // so that compiler doesn't complain aboud unused variable
		assert(off == offset);
		

		// read / write
		if (op->get_type() == READ){
			assert(!op->is_buffer_initialized());
			if (!can_allocate(chunk_size)){
				assert(false);
				continue;
			}
			
			shared_ptr<InternalBuffer> parent = get_buffer(chunk_size);
			op->set_buffer(parent);
	
			// Set sub buffers to next operations
			assign_buffer_children(op,parent);
			
			// Read
			size_t dr = read(fd,op->get_buffer()->get_buffer_space(),chunk_size);
			(void)dr; // so that compiler doesn't complain aboud unused variable
			assert(dr == chunk_size);
			if (0 != posix_fadvise64(fd, offset, op->get_size(), POSIX_FADV_DONTNEED))
				LogImportant("fadvise error");

		} else if (op->get_type() == WRITE) {
			size_t dw = write(fd,op->get_buffer()->get_buffer_space(),chunk_size);
			(void)dw; // so that compiler doesn't complain aboud unused variable 
			assert(dw == chunk_size);
		} else {
			assert(false);
		}
		

		// set in finished_operations
		finished_operations_mutex.lock();
		finished_operations.push_back(op);
		finished_operations_mutex.unlock();
	}

	LogInfo("Ending OnDriveStorageNoCache Loop");
	
	if (-1 == posix_fadvise64(fd, 0, file_size, POSIX_FADV_DONTNEED))
		LogImportant("fadvise error");

	close(fd);
	//file.close();
	assert(operations.empty());
}

void OnDriveStorageNoCache::process(){
	if (read_operations.empty() 
			&& write_operations.empty()
			&& get_buffer_operations.empty())
		return ;
	
	/// Process only get_buffer_operations 
	vector<shared_ptr<Operation>> tmp_finished;
	vector<shared_ptr<Operation>> tmp_get_buffer;

	get_buffer_operations_mutex.lock();
	tmp_get_buffer.swap(get_buffer_operations);
	get_buffer_operations_mutex.unlock();
	
	int count = 0;
	vector<shared_ptr<Operation>>::iterator it;
	for (it = tmp_get_buffer.begin(); it != tmp_get_buffer.end();){
		shared_ptr<Operation> op = *it;

		assert(!op->is_buffer_initialized());
		if (!can_allocate(op->get_size())){
			++it;
			continue;
		}
			
		shared_ptr<InternalBuffer> parent = get_buffer(op->get_size());
		op->set_buffer(parent);

		// Set sub buffers to next operations
		assign_buffer_children(op,parent);

		tmp_finished.push_back(op);
		count++;
		
		it = tmp_get_buffer.erase(it);
	}

	if (!tmp_get_buffer.empty()){
		get_buffer_operations_mutex.lock();
		get_buffer_operations.insert(get_buffer_operations.begin(),
										tmp_get_buffer.begin(),
										tmp_get_buffer.end());
		get_buffer_operations_mutex.unlock();
	}
	
	finished_operations_mutex.lock();
	finished_operations.insert(finished_operations.end(),
									tmp_finished.begin(),
									tmp_finished.end());
	finished_operations_mutex.unlock();
}


bool OnDriveStorageNoCache::is_processing(){
	return !(write_operations.empty()
				&& read_operations.empty()
				&& finished_operations.empty()
				&& get_buffer_operations.empty()
				&& loop_idle);
}


bool OnDriveStorageNoCache::can_allocate(size_t size){
	if (size > buffer_size)
		assert(false);;
	return true;
}

shared_ptr<InternalBuffer> OnDriveStorageNoCache::get_buffer(size_t size){
	assert(size <= buffer_size);
	size_t offset = rand()%(buffer_size - size);
	char* b = buffer + offset;
	shared_ptr<InternalBuffer> ib = make_shared<InternalBuffer>(b,size); 
	active_buffers++;
	return ib;
}

void OnDriveStorageNoCache::release(const shared_ptr<InternalBuffer>& buffer){
	shared_ptr<InternalBuffer> root = buffer;
	while (root != nullptr){
		if (root->get_number_token() > 0){
			break;
		}
		shared_ptr<InternalBuffer> parent = root->get_parent();
		if (root->is_root()){
			active_buffers--;
		}
		root = parent;
	}
}
#endif
