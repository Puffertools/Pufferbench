/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "OnDriveStorage.hpp"

#include <assert.h>
#include <string>
#include <stdlib.h>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <map>

#include "Config.hpp"

#include "Order.hpp"

#include "Operation.hpp"
#include "InternalBuffer.hpp"

#include "Log.hpp"
#include "Defs.hpp"

using namespace std;

REGISTER_STORAGE(OnDriveStorage)

OnDriveStorage::OnDriveStorage(Config& conf):
	AbstractStorage(conf){}

void OnDriveStorage::init(){
	LogInfo("OnDriveStorage: Initialization");
	// Buffer in memory
	buffer_size = conf.get_size_parameter({CONF_SECTION,CONF_ALLOCATED_MEMORY},
													DEFAULT_ALLOCATED_MEMORY);
	LogInfo("|- Setting internal buffer size to "+Log::to_str(buffer_size)+".");
	assert(buffer_size > 0);
	buffer = (char*)malloc(buffer_size);
	memset(buffer,'a',buffer_size);
	assert(buffer != nullptr);

	// Space on drive
	path_to_file = conf.get_parameter<string>({CONF_SECTION,CONF_PATH_TO_FILE},
												DEFAULT_PATH_TO_FILE);
	file_size = conf.get_size_parameter({CONF_SECTION,CONF_ALLOCATED_DRIVE},
													DEFAULT_ALLOCATED_DRIVE);
	reuse_file = conf.get_parameter<bool>({CONF_SECTION,CONF_REUSE_FILE},
													DEFAULT_REUSE_FILE);
	LogInfo("|- File to use "+path_to_file+".");
	LogInfo("|- File size "+Log::to_str(file_size)+".");
	if (reuse_file){
		LogInfo("|- Reuse file.");
	} else {
		LogInfo("|- Do not reuse file.");
	}
	assert(file_size > 0);
	ofstream file_create(path_to_file, fstream::out | fstream::app);
	if (!file_create.is_open()){
		LogError("Could not create file");
		assert(false);
	} else {
		file_create.close();
	}
	
	fstream file(path_to_file, fstream::in | fstream::out | fstream::binary | fstream::ate);
	size_t size = file.tellp();

	LogInfo("|- Existing file has a size of "+Log::to_str(size));
	if (size < file_size){
		size_t content_size = 128L*1024*1024;
		if (content_size > file_size)
			content_size = file_size;
	
		vector<char> content(content_size);
	
		size_t written = file.tellp();
		file.seekp(written);
		LogInfo("|- Existing file has a size of "+Log::to_str(written));
		while (written < file_size){
			size_t to_write = content_size;
			if (to_write > file_size - written)
				to_write = file_size - written;
			file.write(content.data(),to_write);
			written += to_write;
		}
		file.flush();
		
		file.seekp(0,ios_base::end);
		size = file.tellp();
		LogInfo("|- File initialized to "+Log::to_str(size));
		assert(size >= file_size);
		file.close();
	}

	// Start thread
	LogInfo("Starting loop");
	main_loop = thread(&OnDriveStorage::loop, this);
}

void OnDriveStorage::terminate(){	
	LogInfo("OnDriveStorage: Termination");
	thread_run = false;
	// join thread;
	main_loop.join();	
	
	if (!reuse_file){
		if (remove(path_to_file.c_str()) != 0){
			LogError("|- Error while removing temporary file");	
		} else {
			LogInfo("|- File removed");
		}
	} else {
		LogInfo("|- File kept for next runs");
	}
	free(buffer);
	LogStorage("|-  There are "+to_string(active_buffers)+" active buffer(s).");
}

OnDriveStorage::~OnDriveStorage(){}

void OnDriveStorage::assign_buffer_children(const shared_ptr<Operation>& op, 
												const shared_ptr<InternalBuffer>& parent_buffer){
	for (shared_ptr<Operation>& next_op : op->get_nexts()){
		// For each child op
		if (!next_op->is_buffer_initialized()){
			shared_ptr<InternalBuffer> child = make_shared<InternalBuffer>(parent_buffer,
												next_op->get_size(),next_op->get_offset());
			next_op->set_buffer(child);
			assign_buffer_children(next_op,parent_buffer);
		}
	}
}

void OnDriveStorage::loop(){
	
	map<int,vector<shared_ptr<Operation>>,std::greater<int>> operations;
	vector<shared_ptr<Operation>> to_add;
	
	fstream file(path_to_file,fstream::out | fstream::in | fstream::binary);

	loop_idle = false;

	while (thread_run){
		
		// Gather read operations into operations
		read_operations_mutex.lock();
		if (!read_operations.empty())
			loop_idle = false;
		read_operations.swap(to_add);
		read_operations_mutex.unlock();
	
		for (const shared_ptr<Operation>& op : to_add){
			int key = op->get_order()->priority;
			operations[key].push_back(op);
		}
		to_add.clear();
		
		// Gather write operations
		write_operations_mutex.lock();
		if (!write_operations.empty())
			loop_idle = false;
		write_operations.swap(to_add);
		write_operations_mutex.unlock();
	
		for (const shared_ptr<Operation>& op : to_add){
			int key = op->get_order()->priority;
			operations[key].push_back(op);
		}
		to_add.clear();
		
		if (operations.empty()){
			loop_idle = true;
			continue;
		}

		// Take the first element in operations 
		shared_ptr<Operation> op;
		assert(!operations.begin()->second.empty());
		
		op = operations.begin()->second[0];
		operations.begin()->second.erase(operations.begin()->second.begin());
	
		if (operations.begin()->second.empty()){
			// Remove entry in the map
			operations.erase(operations.begin());
		}

		// Set random offset for the IO op
		size_t offset = rand()%((file_size-op->get_size())/4096);

		assert(offset*4096 + op->get_size() < file_size);

		// read / write
		if (op->get_type() == READ){
			assert(!op->is_buffer_initialized());
			if (!can_allocate(op->get_size())){
				assert(false);
				continue;
			}
			
			shared_ptr<InternalBuffer> parent = get_buffer(op->get_size());
			op->set_buffer(parent);
	
			// Set sub buffers to next operations
			assign_buffer_children(op,parent);
			
			// Read
			file.seekg(offset*4096);
			file.read(op->get_buffer()->get_buffer_space(),op->get_size());
		} else if (op->get_type() == WRITE) {
			file.seekp(offset*4096);
			file.write(op->get_buffer()->get_buffer_space(),op->get_size());
		} else {
			assert(false);
		}
		file.flush();

		// set in finished_operations
		finished_operations_mutex.lock();
		finished_operations.push_back(op);
		finished_operations_mutex.unlock();
	}

	LogInfo("Ending OnDriveStorage Loop");

	file.close();
	assert(operations.empty());
}

void OnDriveStorage::process(){
	if (read_operations.empty() 
			&& write_operations.empty()
			&& get_buffer_operations.empty())
		return ;
	
	/// Process only get_buffer_operations 
	vector<shared_ptr<Operation>> tmp_finished;
	vector<shared_ptr<Operation>> tmp_get_buffer;

	get_buffer_operations_mutex.lock();
	tmp_get_buffer.swap(get_buffer_operations);
	get_buffer_operations_mutex.unlock();
	
	int count = 0;
	vector<shared_ptr<Operation>>::iterator it;
	for (it = tmp_get_buffer.begin(); it != tmp_get_buffer.end();){
		shared_ptr<Operation> op = *it;

		assert(!op->is_buffer_initialized());
		if (!can_allocate(op->get_size())){
			++it;
			continue;
		}
			
		shared_ptr<InternalBuffer> parent = get_buffer(op->get_size());
		op->set_buffer(parent);

		// Set sub buffers to next operations
		assign_buffer_children(op,parent);

		tmp_finished.push_back(op);
		count++;
		
		it = tmp_get_buffer.erase(it);
	}

	if (!tmp_get_buffer.empty()){
		get_buffer_operations_mutex.lock();
		get_buffer_operations.insert(get_buffer_operations.begin(),
										tmp_get_buffer.begin(),
										tmp_get_buffer.end());
		get_buffer_operations_mutex.unlock();
	}
	
	finished_operations_mutex.lock();
	finished_operations.insert(finished_operations.end(),
									tmp_finished.begin(),
									tmp_finished.end());
	finished_operations_mutex.unlock();
}


bool OnDriveStorage::is_processing(){
	return !(write_operations.empty()
				&& read_operations.empty()
				&& finished_operations.empty()
				&& get_buffer_operations.empty()
				&& loop_idle);
}


bool OnDriveStorage::can_allocate(size_t size){
	if (size > buffer_size)
		assert(false);;
	return true;
}

shared_ptr<InternalBuffer> OnDriveStorage::get_buffer(size_t size){
	assert(size <= buffer_size);
	size_t offset = rand()%(buffer_size - size);
	char* b = buffer + offset;
	shared_ptr<InternalBuffer> ib = make_shared<InternalBuffer>(b,size); 
	active_buffers++;
	return ib;
}

void OnDriveStorage::release(const shared_ptr<InternalBuffer>& buffer){
	shared_ptr<InternalBuffer> root = buffer;
	while (root != nullptr){
		if (root->get_number_token() > 0){
			break;
		}
		shared_ptr<InternalBuffer> parent = root->get_parent();
		if (root->is_root()){
			active_buffers--;
		}
		root = parent;
	}
}

