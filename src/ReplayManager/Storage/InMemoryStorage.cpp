/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "InMemoryStorage.hpp"

#include <assert.h>
#include <string>
#include <stdlib.h>

#include "Config.hpp"

#include "Operation.hpp"
#include "InternalBuffer.hpp"

#include "Log.hpp"
#include "Defs.hpp"

using namespace std;

REGISTER_STORAGE(InMemoryStorage)

InMemoryStorage::InMemoryStorage(Config& conf):
	AbstractStorage(conf){}

void InMemoryStorage::init(){
	buffer_size = conf.get_size_parameter({CONF_SECTION,CONF_ALLOCATED_SPACE},buffer_size);
	LogInfo("InMemoryStorage: Setting internal buffer size to "+Log::to_str(buffer_size)+".");
	assert(buffer_size > 0);
	buffer = (char*)malloc(buffer_size);
	memset(buffer,'a',buffer_size);
	assert(buffer != nullptr);
}

InMemoryStorage::~InMemoryStorage(){
	free(buffer);
	LogStorage("There are "+to_string(active_buffers)+" active buffer(s).");
}

void InMemoryStorage::assign_buffer_children(const shared_ptr<Operation>& op, 
												const shared_ptr<InternalBuffer>& parent_buffer){
    for (shared_ptr<Operation>& next_op : op->get_nexts()){
		// For each child op
		if (!next_op->is_buffer_initialized()){
			shared_ptr<InternalBuffer> child = make_shared<InternalBuffer>(parent_buffer,
												next_op->get_size(),next_op->get_offset());
			next_op->set_buffer(child);
			assign_buffer_children(next_op,parent_buffer);
		}
	}
}

void InMemoryStorage::process(){
	if (read_operations.empty() 
			&& write_operations.empty()
			&& get_buffer_operations.empty())
		return ;
	
	LogStorage("InMemoryStorage");

	vector<shared_ptr<Operation>> tmp_finished;

	// Read
	// 	- Assign buffer
	//  - Divide it amount next operations
	read_operations_mutex.lock();
	vector<shared_ptr<Operation>>::iterator it;
	int count = 0;
	for (it = read_operations.begin(); it != read_operations.end();){
		shared_ptr<Operation>& op = *it;

		assert(!op->is_buffer_initialized());
		if (!can_allocate(op->get_size())){
			++it;
			continue;
		}
	
		shared_ptr<InternalBuffer> parent = get_buffer(op->get_size());
		op->set_buffer(parent);

		// Set sub buffers to next operations
		assign_buffer_children(op,parent);

		tmp_finished.push_back(op);
		count++;
		
		it = read_operations.erase(it);
	}
	read_operations_mutex.unlock();
	LogStorage(" - Reads: "+to_string(count));

	count = 0;
	get_buffer_operations_mutex.lock();
	for (it = get_buffer_operations.begin(); it != get_buffer_operations.end();){
		shared_ptr<Operation>& op = *it;

		assert(!op->is_buffer_initialized());
		if (!can_allocate(op->get_size())){
			++it;
			continue;
		}
			
		shared_ptr<InternalBuffer> parent = get_buffer(op->get_size());
		op->set_buffer(parent);

		// Set sub buffers to next operations
		assign_buffer_children(op,parent);

		tmp_finished.push_back(op);
		count++;
		
		it = get_buffer_operations.erase(it);
	}
	get_buffer_operations_mutex.unlock();
	LogStorage(" - Get buffer: "+to_string(count));

	// Write
	// 	-Do nothing
	LogStorage(" - Writes: "+to_string(write_operations.size()));
	

	finished_operations_mutex.lock();	
	finished_operations.insert(finished_operations.begin(),tmp_finished.begin(),tmp_finished.end());
	write_operations_mutex.lock();
	finished_operations.insert(finished_operations.begin(),write_operations.begin(),write_operations.end());
	finished_operations_mutex.unlock();
	write_operations.clear();
	write_operations_mutex.unlock();
}


bool InMemoryStorage::is_processing(){
	return !(write_operations.empty()
				&& read_operations.empty()
				&& finished_operations.empty()
				&& get_buffer_operations.empty());
}


bool InMemoryStorage::can_allocate(size_t size){
	if (size > buffer_size)
		assert(false);;
	return true;
}

shared_ptr<InternalBuffer> InMemoryStorage::get_buffer(size_t size){
	assert(size <= buffer_size);
	size_t offset = rand()%(buffer_size - size);
	char* b = buffer + offset;
	shared_ptr<InternalBuffer> ib = make_shared<InternalBuffer>(b,size);
	active_buffers++;
	return ib;
}

void InMemoryStorage::release(const shared_ptr<InternalBuffer>& buffer){
	shared_ptr<InternalBuffer> root = buffer;
	while (root != nullptr){
		if (root->get_number_token() > 0){
			break;
		}
		shared_ptr<InternalBuffer> parent = root->get_parent();
		if (root->is_root()){
			active_buffers--;
		}
		root = parent;
	}
}

