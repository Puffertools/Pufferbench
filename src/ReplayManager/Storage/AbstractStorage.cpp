/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "AbstractStorage.hpp"

#include <mpi.h>
#include <assert.h>
#include <string>

#include "Log.hpp"
#include "Order.hpp"
#include "Operation.hpp"

#include "Config.hpp"

using namespace std;

AbstractStorage::AbstractStorage(Config& conf):
	AbstractComponent(conf){}

AbstractStorage::~AbstractStorage(){
}

void AbstractStorage::add_to_write(vector<shared_ptr<Operation>>& to_add){
	double current_time = MPI_Wtime();
	for (shared_ptr<Operation> op : to_add){
		assert(op->get_type() == WRITE);
		if (op->get_order()->submission_write == 0){
			// If the submission time has not been set, then this is the first
			// submission of the order
			op->get_order()->submission_write = current_time;
		}
	}
	write_operations_mutex.lock();
	write_operations.insert(write_operations.end(),to_add.begin(),to_add.end());
	write_operations_mutex.unlock();
	to_add.clear();
}

void AbstractStorage::add_to_read(vector<shared_ptr<Operation>>& to_add){
	double current_time = MPI_Wtime();
	for (shared_ptr<Operation> op : to_add){
		assert(op->get_type() == READ);
		if (op->get_order()->submission_read == 0){
			// If the submission time has not been set, then this is the first
			// submission of the order
			op->get_order()->submission_read = current_time;
		}
	}
	read_operations_mutex.lock();
	read_operations.insert(read_operations.end(),to_add.begin(),to_add.end());
	read_operations_mutex.unlock();
	to_add.clear();
}

void AbstractStorage::add_get_buffer(vector<shared_ptr<Operation>>& to_add){
	for (shared_ptr<Operation> op : to_add){
		assert(op->get_type() == GET_BUFFER);
	}
	get_buffer_operations_mutex.lock();
	get_buffer_operations.insert(get_buffer_operations.end(),to_add.begin(),to_add.end());
	get_buffer_operations_mutex.unlock();
	to_add.clear();
}

vector<shared_ptr<Operation>> AbstractStorage::get_finished(){
	vector<shared_ptr<Operation>> tmp;
	finished_operations_mutex.lock();
	tmp.swap(finished_operations);
	finished_operations_mutex.unlock();
	double current_time = MPI_Wtime();
	for (shared_ptr<Operation> op : tmp){
		// Overwrite each time the end_time to get the last
		if (op->get_type() == READ){
			op->get_order()->end_read = current_time;
		} else if (op->get_type() == WRITE){
			op->get_order()->end_write = current_time;
		}
	}
	return tmp;
}

void AbstractStorage::set_objects_initially_on_node(
			std::shared_ptr<std::map<ObjectID,ObjectSize>> stored_obj){
	stored_objects = stored_obj;
}
