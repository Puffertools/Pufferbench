/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef ONDRIVESTORAGENOCACHE_HPP
#define ONDRIVESTORAGENOCACHE_HPP

#include "AbstractStorage.hpp"

#include <vector>
#include <string>
#include <mutex>
#include <thread>

#include "Defs.hpp"

class InternalBuffer;
class Config;

/**
 * \brief OnDrive storage
 *
 * Simple OnDrive storage.
 * Does not limit the amount of memory allocated.
 */
class OnDriveStorageNoCache : public AbstractStorage {
private: 
	/// Section in configuration
	const char* CONF_SECTION = "OnDriveStorageNoCache";
	/// Configuration parameter for the memory to reserve
	const char* CONF_ALLOCATED_MEMORY = "allocated_memory";
	/// Default file size
	const ObjectSize DEFAULT_ALLOCATED_MEMORY = 10L*1024*1024*1024;
	/// Configuration parameter for the size of the file
	const char* CONF_ALLOCATED_DRIVE = "allocated_drive";
	/// Default file size
	const ObjectSize DEFAULT_ALLOCATED_DRIVE = 10L*1024*1024*1024;
	/// Configuration parameter for the path of the file
	const char* CONF_PATH_TO_FILE = "path_to_file";
	/// Default path to file
	const std::string DEFAULT_PATH_TO_FILE = "/tmp/Pufferbench-tmp";
	/// Configuration parameter to reuse file
	const char* CONF_REUSE_FILE = "reuse_file";
	/// Default reuse parameter
	const bool DEFAULT_REUSE_FILE = true;

	/// count the number of allocted and not yet freed buffers
	int active_buffers = 0;

	/// Global buffer in which other buffers are taken
	char* buffer = nullptr;

	/// Size of the buffer
	size_t buffer_size;

	/// Size of the file
	size_t file_size;

	/// Path to the file
	std::string path_to_file;

	/// Should the file be reused
	bool reuse_file;

	/// Is the loop idle
	bool loop_idle = true;

	/// Should the loop continue
	bool thread_run = true;

	/// Thread used for the operations
	std::thread main_loop;

	/**
	 * Main loop doing the disk IO, 
	 * function launched as a thread in main_loop
	 */
	void loop();

	/**
	 * Assign buffer to all children of an operation
	 *
	 * \param op Parent operation
	 * \param parent_buffer Buffer from which to create children buffer
	 */
	void assign_buffer_children(const std::shared_ptr<Operation>& op, 
								const std::shared_ptr<InternalBuffer>& parent_buffer);
	
	/**
	 * Test if some amount of memory can be allocated
	 * \param size size of the requested buffer
	 * \return true if the buffer can be allocated
	 */
	bool can_allocate(size_t size);

	/**
	 * Get a buffer of requested size
	 * \param size size of the requested buffer
	 * \return InternalBuffer*
	 */
	std::shared_ptr<InternalBuffer> get_buffer(size_t size);

public:
	
	/**
	 * Constructor
	 *
	 * \param conf Configuration of the run
	 */
	OnDriveStorageNoCache(Config& conf);

	/** 
	 * Destructor
	 */
	 ~OnDriveStorageNoCache();

	/**
	 * Read the conf, initialize the buffer
	 */
	virtual void init();

	virtual void terminate();

	/**
	 * Process the operations (read and write data)		
	 */
	virtual void process();
	
	/**
	 * Indicate whether the storage is processing operations
	 * \return true if the storage is processing some operation
	 */
	virtual bool is_processing();

	/**
	 * Return a buffer to be released 
	 * \param buffer buffer that is not used anymore
	 */ 
	virtual void release(const std::shared_ptr<InternalBuffer>& buffer);
	
};

#endif
