/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef INMEMORYSTORAGE_HPP
#define INMEMORYSTORAGE_HPP

#include "AbstractStorage.hpp"

#include <vector>

class InternalBuffer;
class Config;

/**
 * \brief InMemory storage
 *
 * Simple InMemory storage.
 * Limit the amount of memory allocated.
 * Pre allocate the memory.
 */
class InMemoryStorage : public AbstractStorage {
private: 
	/// count the number of allocted and not yet freed buffers
	int active_buffers = 0;

	/// Global buffer in which other buffers are taken
	char* buffer = nullptr;

	/// Size of the buffer
	size_t buffer_size = 10L*1024*1024*1024;;

	/// Section in configuration
	const char* CONF_SECTION = "InMemoryStorage";

	/// Configuration parameter field
	const char* CONF_ALLOCATED_SPACE = "allocated_space";

	/**
	 * Assign buffer to all children of an operation
	 *
	 * \param op Parent operation
	 * \param parent_buffer Buffer from which to create children buffer
	 */
	void assign_buffer_children(const std::shared_ptr<Operation>& op, 
								const std::shared_ptr<InternalBuffer>& parent_buffer);
	
	/**
	 * Test if some amount of memory can be allocated
	 * \param size size of the requested buffer
	 * \return true if the buffer can be allocated
	 */
	bool can_allocate(size_t size);

	/**
	 * Get a buffer of requested size
	 * \param size size of the requested buffer
	 * \return InternalBuffer*
	 */
	std::shared_ptr<InternalBuffer> get_buffer(size_t size);

public:
	
	/**
	 * Constructor
	 *
	 * \param conf Configuration of the run
	 */
	InMemoryStorage(Config& conf);

	/** 
	 * Destructor
	 */
	 ~InMemoryStorage();

	/**
	 * Read the conf, initialize the buffer
	 */
	virtual void init();

	/**
	 * Process the operations (read and write data)		
	 */
	virtual void process();
	
	/**
	 * Indicate whether the storage is processing operations
	 * \return true if the storage is processing some operation
	 */
	virtual bool is_processing();

	/**
	 * Return a buffer to be released 
	 * \param buffer buffer that is not used anymore
	 */ 
	virtual void release(const std::shared_ptr<InternalBuffer>& buffer);
	
};

#endif
