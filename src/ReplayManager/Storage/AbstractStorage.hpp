/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef ABSTRACTSTORAGE_HPP
#define ABSTRACTSTORAGE_HPP

#include <vector>
#include <map>
#include <memory>
#include <mutex>

#include "AbstractComponent.hpp"

class Operation;
class InternalBuffer;
class Config;

/**
 * \brief Component in charge of the storage and the buffers during 
 * the replays.
 *
 * Manages the storage and the buffers.
 *
 * Buffers management is to optimize in memory storage 
 * and avoid unecessary copies.
 */
class AbstractStorage: public AbstractComponent {
protected:
	/// Vector of get_buffer operations
	std::vector<std::shared_ptr<Operation>> get_buffer_operations;
	/// Mutex for get_buffer operations
	std::mutex get_buffer_operations_mutex;
	
	/// Vector of new read_operations
	std::vector<std::shared_ptr<Operation>> read_operations;
	/// Mutex for read operations
	std::mutex read_operations_mutex;
	
	/// Vector of new write operations
	std::vector<std::shared_ptr<Operation>> write_operations;
	/// Mutex for write operations
	std::mutex write_operations_mutex;
	
	/// vector of finished operations
	std::vector<std::shared_ptr<Operation>> finished_operations;
	/// Mutex for finished operations
	std::mutex finished_operations_mutex;
	
	/// Map of relevant objects stored initially on the nodes <objectID,size>
	std::shared_ptr<std::map<ObjectID,ObjectSize>> stored_objects;

public:

	/**
	 * Constructor
	 *
	 * \param conf Configuration of the run
	 */
	AbstractStorage(Config& conf);
	
	/**
	 * Destructor
	 */
	virtual ~AbstractStorage();

	/**
	 * Add write operations
	 * \param to_add Vector of write Operation to write onto the storage
	 */
	void add_to_write(std::vector<std::shared_ptr<Operation>>& to_add);
	
	/**
	 * Add get_buffer operations
	 * \param to_add Vector of get_buffer Operations to get from the storage
	 */
	void add_get_buffer(std::vector<std::shared_ptr<Operation>>& to_add);

	/**
	 * Add read operations
	 * \param to_add Vector of read Operation to read from the storage
	 */
	void add_to_read(std::vector<std::shared_ptr<Operation>>& to_add);

	/**
	 * Return the completed operations
	 * \return the operations that have been completed
	 */
	std::vector<std::shared_ptr<Operation>> get_finished();

	/** 
	 * Set the objects that are initially hosted by the node
	 * \param stored_objects map of objects <ObjectID,ObjectSize>
	 */
	void set_objects_initially_on_node(std::shared_ptr<std::map<ObjectID,ObjectSize>> stored_obj);

	/**
	 * Process the operations (read and write data)
	 *
	 * Note: to have better statistics, each order (inside of 
	 * Operation) has a field start_read, and start_write 
	 * to fill with a MPI_Wtime() when the operation is 
	 * effectively scheduled.
	 */
	virtual void process() = 0;
	
	/**
	 * Indicate whether the storage is processing operations
	 * \return true if the storage is processing some operation
	 */
	virtual bool is_processing() = 0;
	
	/**
	 * Return a buffer to be released 
	 * \param buffer buffer that is not used anymore
	 */ 
	virtual void release(const std::shared_ptr<InternalBuffer>& buffer) = 0;

	/**
	 * Called at the end of the process, to be used to clean up.
	 */
	virtual void terminate(){};
};

#define REGISTER_STORAGE(comp) REGISTER_COMPONENT(Storage,comp)

#endif
