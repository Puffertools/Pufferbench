/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "Replay.hpp"

#include <mpi.h>

#include <string>
#include <memory>

#include <map>

#include "Log.hpp"

#include "Order.hpp"
#include "Config.hpp"

#include "Constants.hpp"
#include "Stats.hpp"

#include "AbstractIODispatcher.hpp"
#include "AbstractStorage.hpp"
#include "AbstractNetwork.hpp"

#include "ComponentRegister.hpp"

using namespace std;

Replay::~Replay(){}

Stats Replay::run(Config& conf, Constants& constants, vector<Order>& orders){

	LogTrace("Starting the replay.");

	shared_ptr<ComponentRegister> reg = ComponentRegister::instance();

	// Create and setup the network
	shared_ptr<AbstractNetwork> network = reg->get_network(conf);
	network->init();
	
	// Extract relevant objects (initially on node, and used in an order)
	// Received objects
	std::map<ObjectID,bool> received_obj;
	std::shared_ptr<std::map<ObjectID,ObjectSize>> init_obj = 
		std::make_shared<std::map<ObjectID,ObjectSize>>();

	for (const Order& order : orders){
		if (order.type == RECV_WRITE || order.type == RECV_SEND){
			received_obj[order.object_id] = true;
			if (init_obj->count(order.object_id) > 0){
				init_obj->erase(order.object_id);	
			}
		} else if (order.type == READ_SEND) {
			if (received_obj.count(order.object_id) > 0){
				(*init_obj)[order.object_id] = order.size;	
			}
		}
	}

	// Create and setup the storage
	shared_ptr<AbstractStorage> storage = reg->get_storage(conf);
	storage->set_objects_initially_on_node(init_obj);
	storage->init();
	
	// Create and setup the IODispatcher	
	shared_ptr<AbstractIODispatcher> io_dispatcher = reg->get_io_dispatcher(conf);
	io_dispatcher->set_components(storage,network,constants);
	io_dispatcher->init();

	io_dispatcher->input_orders(orders);

	LogTrace("Inputing orders");

	Log::instance()->flush();

	MPI_Barrier(MPI_COMM_WORLD);
	LogInfo("Starting the replay");
	double start_time = MPI_Wtime();
	// Replay orders

	// Loop 
	Stats stats = io_dispatcher->run();
	
	// Gather Orders back with timings
	MPI_Barrier(MPI_COMM_WORLD);
	double end_time = MPI_Wtime();

	LogInfo("Replay finished, cleaning up.");

	storage->terminate();
	network->terminate();

	LogTrace("Replay finished.");
	LogTrace("Statistics of the replay: ");
	LogTrace(" - Duration:           "+to_string(stats.time));
	LogTrace(" - Read throughput:    "+Log::to_str(stats.read_throughput)+"/s");
	LogTrace(" - Write throughput:   "+Log::to_str(stats.write_throughput)+"/s");
	LogTrace(" - Receive throughput: "+Log::to_str(stats.recv_throughput)+"/s");
	LogTrace(" - Send throughput:    "+Log::to_str(stats.send_throughput)+"/s");
	
	Log::instance()->flush();

	stats.global_time = end_time-start_time;
	return stats;
}

