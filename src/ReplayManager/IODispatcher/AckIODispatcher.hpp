/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef ACKIODISPATCHER_HPP
#define ACKIODISPATCHER_HPP

#include <vector>
#include <memory>

#include "AbstractIODispatcher.hpp"
#include "Defs.hpp"

class Config;
class InternalBuffer;

/**
 * \brief IODispatcher with basic optimisations. 
 *
 * It merges orders concerning the same object to avoid duplicating
 * operations.
 * It sends an acknowledgment once the file is written on storage 
 * Do not process forwards.
 */
class AckIODispatcher: public AbstractIODispatcher {
private:
	/// Struct to group orders that process the same object
	struct OrdersSet {
		/// Order to receive and write the object
		Order* recv_write;
		/// Orders to read and send the object
		std::vector<Order*> read_send;
		/// Order to forward the object
		std::vector<Order*> recv_send;
	};

	/**
	 * Maximum size of the message sent onto the network
	 *
	 * Can be specified in configuration with 
	 * AckIODispatcher:
	 *   chunk_size: X
	 */
	ObjectSize chunk_size = 32*1024;

	/// Number of operations currently launched
	int operations_to_complete = 0;

	/// Buffer for the ack
	std::shared_ptr<InternalBuffer> ack_buffer;
	
	/// Content of the ack
	char ack_content = 'a';

public:

	/**
	 * Constructor
	 *
	 * \param conf Configuration of the run
	 */
	AckIODispatcher(Config& conf);

	~AckIODispatcher();

	/**
	 * Read the value of chunk_size in the configuration
	 */
	void init(); 

	/**
	 * Test whether all the work has been scheduled
	 * \return true is the process is finished
	 */
	virtual bool is_finished();

	/**
	 * Process orders from the vector orders in Operations added
	 * to the process with the function add_to_process .
	 * The function is executed only once.
	 */
	virtual void process_orders();

	/**
	 * All operations that accomplished their task.
	 * Typically used to release the buffers.
	 * \param finished Vector of Operation that have their task finished
	 */
	virtual void process_completed(const std::vector<std::shared_ptr<Operation>>& finished);
};


#endif
