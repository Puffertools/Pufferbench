/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef SIMPLEINMEMORYIODISPATCHER_HPP
#define SIMPLEINMEMORYIODISPATCHER_HPP

#include <vector>
#include <memory>

#include "AbstractIODispatcher.hpp"
#include "Defs.hpp"

class Config;

/**
 * \brief IODispatcher with basic optimisations. 
 *
 * It merges orders concerning the same object to avoid duplicating
 * operations.
 * Do not process forwards.
 * Do not take into account any limit in terms of memory for the buffers.
 */
class SimpleIODispatcher: public AbstractIODispatcher {
private:
	/// Struct to group orders that process the same object
	struct OrdersSet {
		/// Order to receive and write the object
		Order* recv_write;
		/// Orders to read and send the object
		std::vector<Order*> read_send;
		/// Order to forward the object
		std::vector<Order*> recv_send;
	};

	/**
	 * Maximum size of the message sent onto the network
	 *
	 * Can be specified in configuration with 
	 * SimpleIODispatcher:
	 *   chunk_size: X
	 */
	ObjectSize chunk_size = 32*1024;

	/// Number of operations currently launched
	int operations_to_complete = 0;

public:

	/**
	 * Constructor
	 *
	 * \param conf Configuration of the run
	 */
	SimpleIODispatcher(Config& conf);

	~SimpleIODispatcher();

	/**
	 * Read the value of chunk_size in the configuration
	 */
	void init(); 

	/**
	 * Test whether all the work has been scheduled
	 * \return true is the process is finished
	 */
	virtual bool is_finished();

	/**
	 * Process orders from the vector orders in Operations added
	 * to the process with the function add_to_process .
	 * The function is executed only once.
	 */
	virtual void process_orders();

	/**
	 * All operations that accomplished their task.
	 * Typically used to release the buffers.
	 * \param finished Vector of Operation that have their task finished
	 */
	virtual void process_completed(const std::vector<std::shared_ptr<Operation>>& finished);
};


#endif
