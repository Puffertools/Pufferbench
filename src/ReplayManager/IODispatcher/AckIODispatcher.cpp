/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "AckIODispatcher.hpp"

#include <assert.h>
#include <map>
#include <mpi.h>

#include "Order.hpp"
#include "Constants.hpp"

#include "Operation.hpp"
#include "InternalBuffer.hpp"

#include "AbstractStorage.hpp"
#include "AbstractNetwork.hpp"

#include "Config.hpp"

using namespace std;

REGISTER_IODISPATCHER(AckIODispatcher)

AckIODispatcher::AckIODispatcher(Config& conf):
	AbstractIODispatcher(conf){
}

AckIODispatcher::~AckIODispatcher(){
}

void AckIODispatcher::init(){
	chunk_size = conf.get_size_parameter({"AckIODispatcher","chunk_size"},32*1024);	
	LogInfo("AckIODispatcher: Setting chunk_size to "+Log::to_str(chunk_size));
	ack_buffer = make_shared<InternalBuffer>(&ack_content,1);
}

bool AckIODispatcher::is_finished(){
	return (orders.empty() && operations_to_complete == 0);
}

void AckIODispatcher::process_orders(){
	/// Work finished
	if (orders.empty())
		return ;
	/// Wait order
	if (operations_to_complete > 0)
		return ;

	vector<Order*>::iterator it;

	LogTrace("AckIODispatcher: processing orders");

	if ((*orders.begin())->type == WAIT){
		LogTrace(" - First order is wait: waiting...");
		MPI_Barrier(MPI_COMM_WORLD);
		LogTrace(" - First order is wait: waking up.");
		orders.erase(orders.begin());
	}
	
	LogTrace(" - Grouping orders per object");	
	// Group orders per objectID
	map<ObjectID,OrdersSet> orders_per_obj;
	for (it = orders.begin(); it != orders.end();){
		Order* order = *it;

		if (order->type == WAIT){
			LogTrace(" - - Found WAIT order");
			break;
		}

		LogTrace(" - - Found order for object: "+to_string(order->object_id));
		LogTrace(" - - - Tag: "+to_string(order->tag));
		// Creating entry
		if (orders_per_obj.find(order->object_id) == orders_per_obj.end()){
			LogTrace(" - - - Creating entry");
			orders_per_obj[order->object_id].recv_write = nullptr;
			orders_per_obj[order->object_id].read_send.clear();
			orders_per_obj[order->object_id].recv_send.clear();
		}
		
		LogTrace(" - - - Adding order "+to_string(order->order_id));

		switch (order->type){
			case (RECV_WRITE):
				LogTrace(" - - - - type: RECV_WRITE");
				// It is useless to have multiple writes -> fail
				assert(orders_per_obj[order->object_id].recv_write == nullptr);
				orders_per_obj[order->object_id].recv_write = order;	
				break;
			case (READ_SEND):
				LogTrace(" - - - - type: READ_SEND");
				orders_per_obj[order->object_id].read_send.push_back(order);	
				break;
			case (RECV_SEND):
				LogTrace(" - - - - type: RECV_SEND");
				orders_per_obj[order->object_id].recv_send.push_back(order);	
				break;
			case (WAIT):
				assert(false);
		}
		it = orders.erase(it);
	}

	// From orders_per_obj generate the operation sequence
	//	- One read or one write 
	//	- Multiple sends 
	// 	- Get buffer to write from storage
	// 		- leave it empty for reads
	
	LogTrace(" - Changing orders into operations");

	int send_op = 0;
	int recv_op = 0;
	int read_op = 0;
	int write_op = 0;
	int get_buffer_op = 0;

	map<ObjectID,OrdersSet>::iterator mit;
	for (mit = orders_per_obj.begin(); mit != orders_per_obj.end(); ++mit){
		OrdersSet& set = mit->second; 
		
		if (set.recv_write != nullptr){
			// write then send
			// Treat reads as recv
			
			Order* order_recv = set.recv_write;
			LogTrace(" - - Processing orders for object "+to_string(order_recv->object_id));
		
			// Get a buffer from the storage
			LogTrace(" - - - Adding get_buffer operation.");
            shared_ptr<Operation> get_buffer = make_shared<Operation>(order_recv, GET_BUFFER, order_recv->size, 0, 0);	
			get_buffer_op++;


			// Central operation is write with the whole buffer
			LogTrace(" - - - Adding write operation.");
			shared_ptr<Operation> write = make_shared<Operation>(order_recv, WRITE, order_recv->size, 0, 0);
			write_op++;
			
			// Send ack
			int tag_init = (order_recv->size/chunk_size)+2;
			int tag_s = tag_init*constants.number_of_tags+order_recv->tag;
			shared_ptr<Operation> ack = make_shared<Operation>(order_recv, ACK_SEND, 1, 0, tag_s);
			ack->set_buffer(make_shared<InternalBuffer>(ack_buffer,1,0));
			write->add_next(ack);
		
			
			vector<shared_ptr<Operation>> recv_acks;
			vector<Order*>::iterator voit;
			for (voit = set.read_send.begin(); voit != set.read_send.end(); ++voit){
				int tag = tag_init*constants.number_of_tags+(*voit)->tag;
				shared_ptr<Operation> rcv_ack = make_shared<Operation>(*voit, ACK_RECV, 1, 0, tag);
				rcv_ack->set_buffer(make_shared<InternalBuffer>(ack_buffer,1,0));
				recv_acks.push_back(rcv_ack);
			}
			for (voit = set.recv_send.begin(); voit != set.recv_send.end(); ++voit){
				int tag = tag_init*constants.number_of_tags+(*voit)->tag;
				shared_ptr<Operation> rcv_ack = make_shared<Operation>(*voit, ACK_RECV, 1, 0, tag);
				rcv_ack->set_buffer(make_shared<InternalBuffer>(ack_buffer,1,0));
				recv_acks.push_back(rcv_ack);
			}
			
			// Cutting object in network messages of size chunk_size
			unsigned int offset = 0; 
			
			LogTrace(" - - - Adding recv operation from rank "+to_string(order_recv->src_rank));
			int chunk_nb = 0;
			// Cut the buffer in chunks add a recv for each, and sends for each destination
			while (offset < order_recv->size){
				int to_recv = chunk_size;
				if (offset + to_recv > order_recv->size){
					to_recv = order_recv->size - offset;
				}

				int tag = chunk_nb*constants.number_of_tags+order_recv->tag;
				shared_ptr<Operation> recv = make_shared<Operation>(order_recv, RECV, to_recv, offset, tag);	
				recv_op++;

				recv->add_next(write);
				get_buffer->add_next(recv);

				int ack_cnt = 0;
				vector<Order*>::iterator oit;
				for (oit = set.read_send.begin(); oit != set.read_send.end(); ++oit){
					tag = chunk_nb*constants.number_of_tags+(*oit)->tag;
					shared_ptr<Operation> send = make_shared<Operation>(*oit, SEND, to_recv, offset, tag);
					recv->add_next(send);	
					send->add_next(recv_acks[ack_cnt]);
					send_op++;
					ack_cnt++;
				}
				for (oit = set.recv_send.begin(); oit != set.recv_send.end(); ++oit){
					tag = chunk_nb*constants.number_of_tags+(*oit)->tag;
					shared_ptr<Operation> send = make_shared<Operation>(*oit, SEND, to_recv, offset, tag);
					recv->add_next(send);	
					send->add_next(recv_acks[ack_cnt]);
					send_op++;
					ack_cnt++;
				}
				
				chunk_nb++;
				offset += to_recv;
			}
			add_to_process(get_buffer);
			LogTrace(" - - - Sending to "+to_string(set.recv_send.size()+set.read_send.size())+
						" nodes in "+to_string(chunk_nb)+" chunks.");
		
		} else if (set.read_send.size()>0) {
			// read then send

			Order* order_read = set.read_send.at(0);
			LogTrace(" - - Processing orders for object "+to_string(order_read->object_id));
			shared_ptr<Operation> read = make_shared<Operation>(order_read, READ, order_read->size, 0, 0);
			read_op++;

			LogTrace(" - - - Adding read operation.");
			
			vector<shared_ptr<Operation>> recv_acks;
			vector<Order*>::iterator voit;
			int tag_init = (order_read->size/chunk_size+2);	
			for (voit = set.read_send.begin(); voit != set.read_send.end(); ++voit){
				int tag = tag_init*constants.number_of_tags+(*voit)->tag;
				shared_ptr<Operation> rcv_ack = make_shared<Operation>(*voit, ACK_RECV, 1, 0, tag);
				rcv_ack->set_buffer(make_shared<InternalBuffer>(ack_buffer,1,0));
				recv_acks.push_back(rcv_ack);
			}
			for (voit = set.recv_send.begin(); voit != set.recv_send.end(); ++voit){
				int tag = tag_init*constants.number_of_tags+(*voit)->tag;
				shared_ptr<Operation> rcv_ack = make_shared<Operation>(*voit, ACK_RECV, 1, 0, tag);
				rcv_ack->set_buffer(make_shared<InternalBuffer>(ack_buffer,1,0));
				recv_acks.push_back(rcv_ack);
			}

			int chunk_nb = 0;
			unsigned int offset = 0; 
			// Cut the buffer in chunks and add a send for each destination
			while (offset < order_read->size){
				int to_send = chunk_size;
				if (offset + to_send > order_read->size){
					to_send = order_read->size - offset;
				}

				int ack_cnt = 0;
				vector<Order*>::iterator oit;
				for (oit = set.read_send.begin(); oit != set.read_send.end(); ++oit){
					int tag = chunk_nb*constants.number_of_tags+(*oit)->tag;
					shared_ptr<Operation> send(new Operation(*oit, SEND, to_send, offset, tag));
					read->add_next(send);	
					send->add_next(recv_acks[ack_cnt]);
					ack_cnt++;
					send_op++;
				}
				for (oit = set.recv_send.begin(); oit != set.recv_send.end(); ++oit){
					int tag = chunk_nb*constants.number_of_tags+(*oit)->tag;
					shared_ptr<Operation> send(new Operation(*oit, SEND, to_send, offset, tag));
					read->add_next(send);	
					send_op++;
					send->add_next(recv_acks[ack_cnt]);
					ack_cnt++;
				}

				chunk_nb++;
				offset+=to_send;
			}
			add_to_process(read);
			LogTrace(" - - - Sending to "+to_string(set.recv_send.size()+set.read_send.size())+
						" nodes in "+to_string(chunk_nb)+" chunks.");
		} else {
			// just forwards
			assert(false);
		}
	}
	
	LogTrace(" - Orders processed");
	LogTrace(" - - Scheduled reads: "+to_string(read_op));
	LogTrace(" - - Scheduled writes: "+to_string(write_op));
	LogTrace(" - - Scheduled sends: "+to_string(send_op));
	LogTrace(" - - Scheduled recvs: "+to_string(recv_op));
	LogTrace(" - - Scheduled get_buffer: "+to_string(get_buffer_op));

	operations_to_complete += read_op + write_op + send_op + recv_op + get_buffer_op;
}


void AckIODispatcher::process_completed(const vector<shared_ptr<Operation>>& finished){
	vector<shared_ptr<Operation>>::const_iterator it;
	for (it = finished.begin(); it != finished.end(); ++it){
		// Release buffer if needed
		if ((*it)->get_type() != ACK_SEND && (*it)->get_type() != ACK_RECV){		
			if ((*it)->is_buffer_initialized()){
				(*it)->get_buffer()->release_access();
					storage->release((*it)->get_buffer());
			}
			operations_to_complete--;
		}
	}
    // XXX: the following line was in the code before I changed from a "vector" argument
    // to a "const vector&" argument. Should the argument have been a "vector&" argument?
	// finished.clear();
}

