/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "AbstractIODispatcher.hpp"

#include <mpi.h>
#include <cassert>

#include "Defs.hpp"
#include "Constants.hpp"
#include "Stats.hpp"

#include "AbstractStorage.hpp"
#include "AbstractNetwork.hpp"

#include "Operation.hpp"

#include "Log.hpp"

using namespace std;

AbstractIODispatcher::AbstractIODispatcher(Config& conf):
	AbstractComponent(conf){}

void AbstractIODispatcher::set_components(const shared_ptr<AbstractStorage>& _storage, 
										  const shared_ptr<AbstractNetwork>& _network, 
												Constants& _constants){
	storage = _storage;
	network = _network;
	constants = _constants;
}

void AbstractIODispatcher::input_orders(vector<Order>& order_vect){
	LogTrace("Adding "+to_string(orders.size())+" orders.");
	for (unsigned int i = 0; i < order_vect.size(); ++i){
		orders.push_back(order_vect.data() + i);
	}
	LogTrace("Inputted "+to_string(orders.size())+" orders.");
}


Stats AbstractIODispatcher::run(){
	vector<shared_ptr<Operation>> to_send;
	vector<shared_ptr<Operation>> to_recv;
	vector<shared_ptr<Operation>> to_read;
	vector<shared_ptr<Operation>> to_write;
	vector<shared_ptr<Operation>> get_buffer;
	vector<shared_ptr<Operation>> finished;

	vector<shared_ptr<Operation>>::iterator it;

	bool done = false;

	double start_time = MPI_Wtime();

	LogTrace("Run replay for "+to_string(orders.size())+" orders.");

	do {
		process_orders();
	
		// From messages fill to_send, to_recv, to_read, to_write, get_buffer
		for (it = messages.begin(); it != messages.end(); ++it){
			OperationType t = (*it)->get_type();
			switch (t){
				case READ:
					to_read.push_back(*it);
					break;
				case WRITE:
					to_write.push_back(*it);
					break;
				case RECV:
					to_recv.push_back(*it);
					break;
				case ACK_RECV:
					to_recv.push_back(*it);
					break;
				case SEND:
					to_send.push_back(*it);
					break;
				case ACK_SEND:
					to_send.push_back(*it);
					break;
				case GET_BUFFER:
					get_buffer.push_back(*it);
					break;
			};
		}
		messages.clear();

		storage->add_get_buffer(get_buffer);
		storage->add_to_read(to_read);
		storage->add_to_write(to_write);
		storage->process();

		finished = process_returned_operations(storage->get_finished());
		process_completed(finished);
		finished.clear();

		network->add_to_send(to_send);
		network->add_to_receive(to_recv);
		
		network->process();

		finished = process_returned_operations(network->get_finished());
		process_completed(finished);
		finished.clear();

		done = messages.empty() && get_buffer.empty();
		done = done && to_send.empty() && to_recv.empty() && to_read.empty() && to_write.empty();
		done = done && is_finished() && !storage->is_processing() && !network->is_processing(); 

	} while (!done);

	double end_time = MPI_Wtime();

	double duration = end_time - start_time;

	// Compute stats
	Stats stats;
	stats.time = duration;
	stats.read_throughput = data_read / duration;
	stats.write_throughput = data_writ / duration;
	stats.recv_throughput = data_recv / duration;
	stats.send_throughput = data_sent / duration;

	return stats;
}

vector<shared_ptr<Operation>> AbstractIODispatcher::process_returned_operations(
		const vector<shared_ptr<Operation>>& returned){
	vector<shared_ptr<Operation>> finished;
	if (returned.empty())
		return finished;

	vector<shared_ptr<Operation>> nexts;
	vector<shared_ptr<Operation>>::const_iterator it;
	vector<shared_ptr<Operation>>::const_iterator it2;
	for (it = returned.begin(); it != returned.end(); ++it){
		nexts.clear();
		(*it)->mark_finished();
		// Update statistics
		switch ((*it)->get_type()){
			case READ:
				data_read += (*it)->get_size();
				break;
			case WRITE:
				data_writ += (*it)->get_size();
				break;
			case RECV:
				data_recv += (*it)->get_size();
				break;
			case SEND:
				data_sent += (*it)->get_size();
				break;
			case GET_BUFFER:
				// No stats for that
				break;
			case ACK_RECV:
				break;
			case ACK_SEND:
				break;
		}
		
		// Schedule nexts
		nexts.swap((*it)->get_nexts());
		for (it2 = nexts.begin(); it2 != nexts.end(); ++it2){
			if ((*it2)->can_be_scheduled()){
				messages.push_back(*it2);
			}
		}
		finished.push_back(*it);
	}
	return finished;
}


void AbstractIODispatcher::add_to_process(const shared_ptr<Operation>& im){
	messages.push_back(im);
}

