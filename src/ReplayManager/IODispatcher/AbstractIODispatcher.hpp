/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef ABSTRACTIODISPATCHER_HPP
#define ABSTRACTIODISPATCHER_HPP

#include <vector>
#include <string>
#include <memory>

#include "AbstractComponent.hpp"
#include "Order.hpp"
#include "Constants.hpp"

struct Stats;
class AbstractStorage;
class AbstractNetwork;
class Operation;
class Config;

/**
 * \brief Component in charge of scheduling order during the replay
 *
 * Abstract class for the "brain" of the replay.
 * It changes the orders in operations.
 */
class AbstractIODispatcher: public AbstractComponent{
private:
	/**
	 * Operations to process or in processing.
	 */
	std::vector<std::shared_ptr<Operation>> messages;	
	
	/**
	 * Extract the next operations from the list of operations
	 * that have been processed by an AbstractNetwork or AbstractDevice,
	 * add the next steps (Operation) to be processed by the main loop,
	 * and return the Operations that are completed.
	 *
	 * \param returned vector of processed operations
	 * \return vector of processed operations
	 */
	std::vector<std::shared_ptr<Operation>> process_returned_operations(const std::vector<std::shared_ptr<Operation>>& returned);

	/// Statistics of data sent
	double data_sent = 0;
	/// Statistics of data received
	double data_recv = 0;
	/// Statistics of data written
	double data_writ = 0;
	/// Statistics of data read
	double data_read = 0;	

protected:
	/// Storage component
	std::shared_ptr<AbstractStorage> storage;
	/// Network component
	std::shared_ptr<AbstractNetwork> network;
	/// Constants of this replay
	Constants constants;

	/**
	 * vector ot Order to process. 
	 * Each order matches a transfer of data scheduled by the 
	 * DataTransferScheduler.
	 */
	std::vector<Order*> orders;

	/**
	 * Add an operation to be processed by the replay manager.
	 *
	 * \param op Operation to added
	 */
	void add_to_process(const std::shared_ptr<Operation>& op);

public:

	/**
	 * Constructor
	 * 
	 * \param conf Configuration of the run
	 */
	AbstractIODispatcher(Config& conf);

	/**
	 * Destructor
	 */
	virtual ~AbstractIODispatcher(){};
	
	/**
	 * Initialize the components for the storage, network, and the buffer
	 * allocator.
	 *
	 * \param _storage Storage component
	 * \param _network Network component
	 * \param _constants Constants of the replay
	 */
	void set_components(const std::shared_ptr<AbstractStorage>& _storage, 
						const std::shared_ptr<AbstractNetwork>& _network,
							Constants& _constants);

	/**
	 * Initialize the orders in the scheduler.
	 *
	 * \param orders_vect vector of Order
	 */
	void input_orders(std::vector<Order>& orders_vect);

	/**
	 * Main loop of the replay manager.
	 * 
	 * \return Stats of the replay
	 */ 
	Stats run();

	/**
	 * Test whether all the work has been scheduled
	 *
	 * \return true is the process is finished
	 */
	virtual bool is_finished() = 0;

	/**
	 * Process orders from the vector orders in Operations added
	 * to the process with the function add_to_process .
	 * The function is executed only once.
	 */
	virtual void process_orders() = 0;

	/**
	 * All operations that accomplished their task.
	 * Typically used to release the buffers.
	 *
	 * \param finished Vector of Operation that have their task finished
	 */
	virtual void process_completed(const std::vector<std::shared_ptr<Operation>>& finished) = 0;
};

#define REGISTER_IODISPATCHER(comp) REGISTER_COMPONENT(IODispatcher,comp)

#endif
