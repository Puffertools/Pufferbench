/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef REPLAY_HPP
#define REPLAY_HPP

#include <vector>

struct Order;
class Config;
struct Stats;
struct Constants;

/**
 * \brief Manager of the replay of a set of orders
 *
 * Creates the components needed for the replay of a set of orders
 * and launches them in the proper order.
 */
class Replay {
public:
	~Replay();
	
	/**
	 * Launch a replay
	 *
	 * \param conf Configuration
	 * \param constants Relevant informations about this run
	 * \param orders Array of orders to replay
	 * \return Statistics about the run
	 */
	Stats run(Config& conf, Constants& constants, std::vector<Order>& orders);
};

#endif
