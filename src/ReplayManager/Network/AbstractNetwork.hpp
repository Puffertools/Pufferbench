/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef ABSTRACTNETWORK_HPP
#define ABSTRACTNETWORK_HPP

#include <vector>
#include <string>
#include <memory>
#include <mutex>

#include "AbstractComponent.hpp"

class Operation;
class Config;

/**
 * \brief Component in charge of the network during the replay.
 *
 * Class managing the network
 */
class AbstractNetwork: public AbstractComponent {
protected:
	/**
	 * Vector of operations of type RECV
	 */
	std::vector<std::shared_ptr<Operation>> receive_operations;
	/// Mutex of receive operations
	std::mutex receive_operations_mutex;

	/**
	 * Vector of operations of type SEND
	 */
	std::vector<std::shared_ptr<Operation>> send_operations;
	/// Mutex of receive operations
	std::mutex send_operations_mutex;

	/**
	 * Vector of completed network operations
	 */
	std::vector<std::shared_ptr<Operation>> finished_operations;
	/// Mutex of finished operations
	std::mutex finished_operations_mutex;

public:
	/**
	 * Constructor of the network component.
	 *
	 * \param conf Configuration of the run
	 */
	AbstractNetwork(Config& conf);

	/**
	 * Destructor of the network component.
	 */
	virtual ~AbstractNetwork();
	
	/**
	 * Add receive operations to process: RECV and ACK_RECV
	 *
	 * \param to_add vector of receive operations
	 */
	void add_to_receive(std::vector<std::shared_ptr<Operation>>& to_add);

	/**
	 * Add send operations to process: SEND, and ACK_SEND 
	 *
	 * \param to_add vector of send operations
	 */
	void add_to_send(std::vector<std::shared_ptr<Operation>>& to_add);

	/**
	 *	Return the vector of completed operations.
	 * \return vector of completed operations.
	 */
    std::vector<std::shared_ptr<Operation>> get_finished();
	 
	/**
	 * Process the vectors receive_operations and 
	 * send_operations and complete the operations.
	 * 
	 * Note: to have better statistics, each order (inside of 
	 * Operation) has a field start_send, and start_recv 
	 * to fill with a MPI_Wtime() when the operation is 
	 * effectively scheduled.
	 */
	virtual void process() = 0;

	/**
	 * Indicates if the network component has finished all 
	 * operation that have been given to it.
	 *
	 * \return true if all network operations have been completed.
	 */
	virtual bool is_processing() = 0;

	/**
	 * Called at the end of the process, to be used to clean up.
	 */
	virtual void terminate(){};
};

#define REGISTER_NETWORK(comp) REGISTER_COMPONENT(Network,comp)

#endif
