/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "MPINetwork.hpp"

#include <cassert>
#include <algorithm>
#include <climits>

#include "Log.hpp"

#include "Operation.hpp"
#include "InternalBuffer.hpp"
#include "Order.hpp"

#include "Config.hpp"

using namespace std;

REGISTER_NETWORK(MPINetwork)

MPINetwork::MPINetwork(Config& conf):AbstractNetwork(conf){}

void MPINetwork::terminate(){
	assert(current_com == 0);
}

static bool order_function(shared_ptr<Operation> op1, shared_ptr<Operation> op2){
	return op1->get_tag() < op2->get_tag();
}

void MPINetwork::init(){
	// Getting delay_between_reports
	delay_between_reports = conf.get_parameter<double>({CONFIG_SECTION,
														DELAY_NAME},
														delay_between_reports);	
	LogInfo("MPINetwork: Setting parameter "+string(DELAY_NAME)+" to "+
				to_string(delay_between_reports));
	// Getting max_concurrent_requests
	max_concurrent_requests = conf.get_parameter<int>({CONFIG_SECTION,
														CONCURRENT_REQUESTS_NAME},
														max_concurrent_requests);	
	LogInfo("MPINetwork: Setting parameter "+string(CONCURRENT_REQUESTS_NAME)+" to "+
				to_string(max_concurrent_requests));
	// Getting max_concurrent_priority
	max_concurrent_priority = conf.get_parameter<int>({CONFIG_SECTION,
														CONCURRENT_PRIORITY_NAME},
														max_concurrent_priority);	
	LogInfo("MPINetwork: Setting parameter "+string(CONCURRENT_PRIORITY_NAME)+" to "+
				to_string(max_concurrent_priority));
}


void MPINetwork::process(){
	if (current_requests.empty() 
			&& send_operations.empty() 
			&& receive_operations.empty()
			&& waiting_operations.empty())
		return ;

	LogNetwork("MPINetwork");

	max_current_priority = -1;

	// Check the termination of current requests
	int recv_wait = 0;
	int send_wait = 0;
	int tmp_recv_finished = 0;
	int tmp_send_finished = 0;
	long data_recv_wait = 0;
	long data_send_wait = 0;

	vector<InternalRequest>::iterator req;
	for (req = current_requests.begin(); req != current_requests.end(); ){

		int flag = false;
		int err;
		err = MPI_Test(&(req->request), &flag, MPI_STATUS_IGNORE);
		if (err != MPI_SUCCESS){
			assert(false);
		}

		if (flag){
			// Update waiting operations
			int priority = req->operation->get_order()->priority;
			// Operation is finished
			if (req->operation->get_type() == RECV){
				tmp_recv_finished++;
			} else if (req->operation->get_type() == SEND){
				tmp_send_finished++;
			}
			if (req->operation->get_type() == RECV || req->operation->get_type() == SEND){
				waiting_operations[priority].running--;
				waiting_operations[priority].completed++;
			}
			current_com--;
			
			finished_operations.push_back(req->operation);
			req->operation.reset();
			req = current_requests.erase(req);
		} else {
			// Operation is not finished
			if (req->operation->get_type() == RECV){
				recv_wait++;
				data_recv_wait += req->operation->get_buffer()->get_size();
			} else if (req->operation->get_type() == SEND){
				send_wait++;
				data_send_wait += req->operation->get_buffer()->get_size();
			}
			if (req->operation->get_type() == RECV || req->operation->get_type() == SEND){
				if (req->operation->get_order()->priority > max_current_priority){
					max_current_priority = req->operation->get_order()->priority;
				}
			}
			++req;
		}
	}
	// Update stats
	send_finished += tmp_send_finished;
	recv_finished += tmp_recv_finished;
	concurrent_send_req -= tmp_send_finished;
	concurrent_recv_req -= tmp_recv_finished;

	LogNetwork(" - send finished: "+to_string(send_finished));
	LogNetwork(" - recv finished: "+to_string(recv_finished));	
	LogNetwork(" - send waiting: "+to_string(send_wait)+" ("+to_string(data_send_wait)+")");
	LogNetwork(" - recv waiting: "+to_string(recv_wait)+" ("+to_string(data_recv_wait)+")");
	
	LogNetwork(" - Sending operations");
	LogNetwork(" - - "+to_string(send_operations.size())+" new sends.");

	vector<shared_ptr<Operation>> acks;

	// Process new operations
	vector<shared_ptr<Operation>>::iterator it;
	for (it = send_operations.begin(); it != send_operations.end(); ++it){
		// add new send operations
		shared_ptr<Operation> op = *it;
		// Check that there were no mistakes
		assert(op->is_buffer_initialized());
		assert(op->get_type() == SEND || op->get_type() == ACK_SEND);

		if (op->get_type() == SEND){
			// Add into the waiting map
			int priority = op->get_order()->priority;
			if (waiting_operations.find(priority) == waiting_operations.end()){
				assert(!waiting_operations[priority].sorted);
			}
	
			waiting_operations[priority].operations.push_back(op);
			waiting_operations[priority].added++;
			waiting_operations[priority].sorted = false;
	
			send_to_launch++;
		} else {
			acks.push_back(op);
		}
	}
	send_operations.clear();

	LogNetwork(" - Receive operations");
	LogNetwork(" - - "+to_string(receive_operations.size())+" new receives.");
	for (it = receive_operations.begin(); it != receive_operations.end(); ++it){
		// add new send operations
		shared_ptr<Operation> op = *it;
		// Check that there were no mistakes
		assert(op->is_buffer_initialized());
		assert(op->get_type() == RECV || op->get_type() == ACK_RECV);

		if (op->get_type() == RECV){
			// Add into the waiting map
			int priority = op->get_order()->priority;
			if (waiting_operations.find(priority) == waiting_operations.end()){
				assert(!waiting_operations[priority].sorted);
			}
	
			waiting_operations[priority].operations.push_back(op);
			waiting_operations[priority].added++;
			waiting_operations[priority].sorted = false;
	
			recv_to_launch++;
		} else {
			acks.push_back(op);
		}
	}
	receive_operations.clear();

	// Acks
	for (const shared_ptr<Operation>& op : acks){
		if (op->get_type() == ACK_SEND){
			// Start the asynchronous send
			InternalRequest req;
			req.operation = op;
			int err;
			err = MPI_Isend(op->get_buffer()->get_buffer_space(),
							op->get_buffer()->get_size(),
							MPI_BYTE,
							op->get_order()->src_rank,
							op->get_tag(),
							MPI_COMM_WORLD,
							&(req.request));
			if (err != MPI_SUCCESS){
					assert(false);
			}

			current_requests.push_back(req);
			current_com++;
		} else if (op->get_type() == ACK_RECV) {
			// Start the asynchronous recv
			InternalRequest req;
			req.operation = op;
			int err;
			err = MPI_Irecv(op->get_buffer()->get_buffer_space(),
							op->get_buffer()->get_size(),
							MPI_BYTE,
							op->get_order()->dst_rank,
							op->get_tag(),
							MPI_COMM_WORLD,
							&(req.request));
			if (err != MPI_SUCCESS){
					assert(false);
			}
			current_requests.push_back(req);
			current_com++;
		}
	}

	double current_time = MPI_Wtime();
	map<int, InternalWaiting>::iterator mit;
	// the order in which map iterators go through the map is by 
	// order of increasing keys
	// Here keys are ordered  (the highest priority is the smallest one) 
	for (mit = waiting_operations.begin(); mit != waiting_operations.end(); ){
		// If the limit for tasks of the same priority is reached, do nothing
		if (mit->second.running >= max_concurrent_priority){
			++mit;
			continue;
		}
		
		if (!mit->second.sorted){
			sort(mit->second.operations.begin(),mit->second.operations.end(),order_function);
			mit->second.sorted = true;
		}

		int priority = mit->first;
		if (max_current_priority < 0){
			max_current_priority = priority;
		}


		int can_add = max_concurrent_priority - mit->second.running;
		int can_add_recv = max_concurrent_requests / 4 - concurrent_recv_req;
		int can_add_send = max_concurrent_requests / 4 - concurrent_send_req;
		if (priority > max_current_priority){
			can_add_recv = (concurrent_recv_req+max_concurrent_requests / 2)/2 - concurrent_recv_req;
			can_add_send = (concurrent_send_req+max_concurrent_requests / 2)/2 - concurrent_send_req;
		}

		if (can_add > 0){
	
			vector<shared_ptr<Operation>>::iterator vit;
			for (vit = mit->second.operations.begin(); vit != mit->second.operations.end(); ){
	
				shared_ptr<Operation> op = *vit;
				if (op->get_type() == SEND && can_add_send > 0){
					// Update statistics
					send_started++;
					send_to_launch--;
			
					// Set start time
					op->get_order()->start_send = current_time;
			
					// Start the asynchronous send
					InternalRequest req;
					req.operation = op;
					int err;
					err = MPI_Isend(op->get_buffer()->get_buffer_space(),
									op->get_buffer()->get_size(),
									MPI_BYTE,
									op->get_order()->dst_rank,
									op->get_tag(),
									MPI_COMM_WORLD,
									&(req.request));
					if (err != MPI_SUCCESS){
						assert(false);
					}

					current_requests.push_back(req);
					current_com++;
					LogNetwork(" - - Initialized the transfer of object "
								+to_string(op->get_order()->object_id)+
								" to "+to_string(op->get_order()->dst_rank));	
					concurrent_send_req++;
					can_add--;
					can_add_send--;
					mit->second.running++;
					vit = mit->second.operations.erase(vit);
				} else if (op->get_type() == RECV && can_add_recv > 0) {
					// Update statistics
					recv_started++;
					recv_to_launch--;
			
					// Set start time
					op->get_order()->start_recv = current_time;
			
					// Start the asynchronous recv
					InternalRequest req;
					req.operation = op;
					int err;
					err = MPI_Irecv(op->get_buffer()->get_buffer_space(),
									op->get_buffer()->get_size(),
									MPI_BYTE,
									op->get_order()->src_rank,
									op->get_tag(),
									MPI_COMM_WORLD,
									&(req.request));
					if (err != MPI_SUCCESS){
						assert(false);
					}
			
					current_requests.push_back(req);
					current_com++;
					LogNetwork(" - - Initialized the reception of object "
								+to_string(op->get_order()->object_id)+
								" from "+to_string(op->get_order()->src_rank));
					concurrent_recv_req++;
					can_add--;
					can_add_recv--;
					mit->second.running++;
					vit = mit->second.operations.erase(vit);
				} else {
					++vit;
				}

				if (can_add <= 0){
					break;
				}
				if (can_add_recv <= 0 && can_add_send <= 0){
					break;
				}
			}
		}
		if (mit->second.completed == mit->second.added ){
			mit = waiting_operations.erase(mit);
		} else {
			++mit;
		}

		if (can_add_recv <= 0 && can_add_send <= 0){
			break;
		}
	}

	if (MPI_Wtime() > last_report + delay_between_reports){
		LogTrace("MPINetwork - During the last "+to_string(delay_between_reports)+"s.");
		LogTrace(" - Recv started: "+to_string(recv_started));
		LogTrace(" - Send started: "+to_string(send_started));
		LogTrace(" - Recv finished: "+to_string(recv_finished));
		LogTrace(" - Send finished: "+to_string(send_finished));
		LogTrace(" - Recv currently waiting:  "+to_string(recv_wait)+" ("+to_string(data_recv_wait)+")");
		LogTrace(" - Send currently waiting:  "+to_string(send_wait)+" ("+to_string(data_send_wait)+")");
		LogTrace(" - Recv currently waiting to be launched:  "+to_string(recv_to_launch));
		LogTrace(" - Send currently waiting to be launched:  "+to_string(send_to_launch));
		recv_started = 0;
		send_started = 0;
		recv_finished = 0;
		send_finished = 0;
		last_report = MPI_Wtime();
	}
}

bool MPINetwork::is_processing(){
	return !(receive_operations.empty()
				&& send_operations.empty()
				&& finished_operations.empty()
				&& current_requests.empty()
				&& waiting_operations.empty());
}
