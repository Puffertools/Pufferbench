/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "AbstractNetwork.hpp"

#include <mpi.h>
#include <assert.h>

#include "Config.hpp"
#include "Operation.hpp"
#include "Order.hpp"

#include "Log.hpp"

using namespace std;

AbstractNetwork::AbstractNetwork(Config& conf):
	AbstractComponent(conf){}


AbstractNetwork::~AbstractNetwork(){
}

void AbstractNetwork::add_to_receive(vector<shared_ptr<Operation>>& to_add){
	double current_time = MPI_Wtime();
	vector<shared_ptr<Operation>>::iterator it;
	for (it = to_add.begin(); it != to_add.end(); ++it){
		assert((*it)->get_type() == RECV || (*it)->get_type() == ACK_RECV);
		if ((*it)->get_order()->submission_recv == 0){
			// If the submission time has not been set, then this is the first
			// submission of the order
			(*it)->get_order()->submission_recv = current_time;
		}
	}
	receive_operations_mutex.lock();
	receive_operations.insert(receive_operations.end(),to_add.begin(),to_add.end());
	receive_operations_mutex.unlock();
	to_add.clear();
}

void AbstractNetwork::add_to_send(vector<shared_ptr<Operation>>& to_add){
	double current_time = MPI_Wtime();
	vector<shared_ptr<Operation>>::iterator it;
	for (it = to_add.begin(); it != to_add.end(); ++it){
		assert((*it)->get_type() == SEND || (*it)->get_type() == ACK_SEND);
		if ((*it)->get_order()->submission_send == 0){
			// If the submission time has not been set, then this is the first
			// submission of the order
			(*it)->get_order()->submission_send = current_time;
		}
	}
	send_operations_mutex.lock();
	send_operations.insert(send_operations.end(),to_add.begin(),to_add.end());
	send_operations_mutex.unlock();
	to_add.clear();
}

vector<shared_ptr<Operation>> AbstractNetwork::get_finished(){
	vector<shared_ptr<Operation>> tmp;
	finished_operations_mutex.lock();
	tmp.swap(finished_operations);
	finished_operations_mutex.unlock();
	double current_time = MPI_Wtime();
	vector<shared_ptr<Operation>>::iterator it;
	for (it = tmp.begin(); it != tmp.end(); ++it){
		// Overwrite each time the end_time to get the last
		if ((*it)->get_type() == SEND){
			(*it)->get_order()->end_send = current_time;
		} else if ((*it)->get_type() == RECV){
			(*it)->get_order()->end_recv = current_time;
		}
	}
	return tmp;
}

