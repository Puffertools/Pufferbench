/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef MPINETWORK_HPP
#define MPINETWORK_HPP

#include <vector>
#include <map>
#include <string>
#include <mpi.h>
#include <memory>

#include "AbstractNetwork.hpp"

class Config;

/**
 * \brief Network component implemented with MPI
 */
class MPINetwork : public AbstractNetwork {
private:
	/**
	 * \brief Pair of asynchronous MPI request and matching Operation
	 *
	 * Struct that records an operation and a MPI_Request
	 * to track non-blocking operations.
	 */
	struct InternalRequest {
		/// MPI_Request 
		MPI_Request request;
		/// Operation associated with the MPI_Request
		std::shared_ptr<Operation> operation;
	};

	/** 
	 * \brief Operation of the same priority waiting to be launched
	 * 
	 * Struct to order requests as well as keeping
	 * track of the number of running requests.
	 */
	struct InternalWaiting {
		/// Is the vector operations sorted
		bool sorted = false;  
		/// Total nubmer of completed operations
		int completed = 0;
		/// Total number of operations
		int added = 0;
		/// Number of operations currently running
		int running = 0;
		/// Vector of operations to launch
		std::vector<std::shared_ptr<Operation>> operations;
	};

	/// Vector of laucnhed non-blocking requests.
	std::vector<InternalRequest> current_requests;

	/// Map of Operations to launch sorted in reverse order
	std::map<int, InternalWaiting, std::greater<int>> waiting_operations; 

	/// Time of the last report
	double last_report = 0;

	/// Config section
	const char* CONFIG_SECTION = "MPINetwork";

	/// Delay between reports
	double delay_between_reports = 1;
	/// Configuration parameter corresponding
	const char* DELAY_NAME = "delay_between_reports";

	/// Number of concurrent requests
	int max_concurrent_requests = 500;
	/// Configuration parameter to set max_concurrent_requests
	const char* CONCURRENT_REQUESTS_NAME = "max_concurrent_requests";

	/// Nubmer of concurrent requests from the same priority
	int max_concurrent_priority = 50;
	/// Configuration parameter to set max_concurrent_priority
	const char* CONCURRENT_PRIORITY_NAME = "max_same_priority";

	/// Number of concurrent recv operations
	int concurrent_recv_req = 0;
	/// Number of concurrent send operations
	int concurrent_send_req = 0;

	/// Lowest priority currently launched
	int max_current_priority = 0;

	/// Receive operation finished during the last period
	int recv_finished = 0;
	/// Send operation finished during the last period
	int send_finished = 0;

	/// Receive operation started during the last period
	int recv_started = 0;
	/// Send operation started during the last period
	int send_started = 0;

	/// Receive operations waiting
	int recv_to_launch = 0;
	/// Send operations waiting
	int send_to_launch = 0;

	/// Current running requests
	int current_com = 0;

public:
	/**
	 * Constructor
	 *
	 * \param conf Configuration of the run
	 */
	MPINetwork(Config& conf);

	/**
	 * reads the conf and initialize the parameters to initialise
	 */
	virtual void init();

	/**
	 * Process network operations with MPI.
	 */
	virtual void process();

	/**
	 * Return true if there are any operation 
	 * stored in the component.
	 * \return true if the component is processing operations.
	 */
	virtual bool is_processing();

	virtual void terminate();
};

#endif
