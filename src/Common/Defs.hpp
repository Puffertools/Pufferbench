/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef DEFS_HPP
#define DEFS_HPP

#include <stdint.h>

/// Type of Node ID
typedef int NodeID;

/// Type of OrderID.  Needs to be int for MPI
typedef int OrderID; 

/// Type of ObjectID
typedef int ObjectID;
/// Type of ObjectSize
typedef int64_t ObjectSize;

#endif
