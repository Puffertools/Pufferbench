/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef COMPONENTFACTORY_HPP
#define COMPONENTFACTORY_HPP

#include <memory>
#include <string>

#include "ComponentRegister.hpp"

#include "Log.hpp"

class AbstractComponent;

/**
 * \brief Abstract class for the component factory
 */
class ComponentFactory{
public:
	
	/**
	 * Create a component
	 *
	 * \param conf Configuration of the run
	 * \return Shared_ptr to a component
	 */
	virtual std::shared_ptr<AbstractComponent> create(Config& conf) = 0;
};

#define REGISTER_COMPONENT(type, comp) \
	class comp##Factory: public ComponentFactory { \
	public: \
		comp##Factory(){ \
			ComponentRegister::instance()->registerFactory(#type, #comp, this); \
		} \
		\
		virtual std::shared_ptr<AbstractComponent> create(Config& conf) {\
			LogInfo("Selecting "+string(#type)+": "+string(#comp)); \
			return make_shared<comp>(conf); \
		}\
	};\
	static const comp##Factory global_##comp##Factory;
#endif
