/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef COMPONENTREGISTER_HPP
#define COMPONENTREGISTER_HPP

#include <exception>
#include <map>
#include <string>
#include <memory>

class AbstractMetadataGenerator;
class AbstractDataDistributionGenerator;
class AbstractDataTransferScheduler;

class AbstractIODispatcher;
class AbstractStorage;
class AbstractNetwork;

class ComponentFactory;

class Config;

/**
 * \brief Register of all component that can be selected through the configuration.
 *
 * Register of the different versions of components available to 
 * Pufferbench. 
 * Return the component specified in the configuration file.
 */
class ComponentRegister {
private:
	/// Map of component factories sorted by type and name
	std::map<std::string,std::map<std::string,ComponentFactory*>> factories;

	/**
	 * Gives informations and generate an error if the component factory
	 * is not found.
	 *
	 * \param type Type of the requested factory
	 * \param name Name of the requested component
	 */
	void not_found_error(std::string type, std::string name);

public:
	/**
	 * Single instance of the ComponentRegister
	 */
	static std::shared_ptr<ComponentRegister> p_instance;

	/**
	 * Return the component register 
	 *
	 * \return the single instance of the component register
	 */
	static std::shared_ptr<ComponentRegister> instance();

	/**
	 * Register a new component factory.
	 *
	 * \param type Type of the component (MetadataGenerator, DataDistributionGenerator, 
	 * DataTransferScheduler, IODispatcher, Storage, Network)
	 * \param comp Name of the component
	 * \param factory Factory for the component.
	 */
	void registerFactory(std::string type, std::string comp, ComponentFactory* factory);

	/**
	 * Return the MetadataGenerator associated with the name given
	 * in parameter.
	 * 
	 * \param conf Configuration of the run.
	 * \return MetadataGenerator
	 */
	std::shared_ptr<AbstractMetadataGenerator> get_metadata_generator(Config& conf);
	
	/**
	 * Return the DataDistributionGenerator associated with the name given
	 * in parameter.
	 * 
	 * \param conf Configuration of the run.
	 * \return DataDistributionGenerator
	 */
	std::shared_ptr<AbstractDataDistributionGenerator> get_data_distribution_generator(Config& conf);
	
	/**
	 * Return the DataTransferScheduler associated with the name given
	 * in parameter.
	 * 
	 * \param conf Configuration of the run.
	 * \return DataTransferScheduler
	 */
	std::shared_ptr<AbstractDataTransferScheduler> get_data_transfer_scheduler(Config& conf);
	
	/**
	 * Return the Storage associated with the name given
	 * in parameter.
	 * 
	 * \param conf Configuration of the run.
	 * \return Storage
	 */
	std::shared_ptr<AbstractStorage> get_storage(Config& conf);
	
	/**
	 * Return the Network associated with the name given
	 * in parameter.
	 * 
	 * \param conf Configuration of the run.
	 * \return Network
	 */
	std::shared_ptr<AbstractNetwork> get_network(Config& conf);
	
	/**
	 * Return the IODispatcher associated with the name given
	 * in parameter.
	 * 
	 * \param conf Configuration of the run.
	 * \return IODispatcher
	 */
	std::shared_ptr<AbstractIODispatcher> get_io_dispatcher(Config& conf);

	/**
	 * \brief Exception raised if the component name is not known
	 *
	 * Exception raised if no component matches the name given in parameter.
	 */
	class ComponentNotFoundException: public std::exception {
		/**
		 * \return message describing the exception.
		 */
		virtual const char* what() const noexcept;
	};
};


#endif

