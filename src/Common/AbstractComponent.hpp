/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef ABSTRACTCOMPONENT_HPP
#define ABSTRACTCOMPONENT_HPP

#include <string>

#include "ComponentFactory.hpp"

class Config;

/**
 * \brief Base of all components used in Pufferbench.
 *
 * Base class for all components AbstractDataTransferScheduler
 * AbstractDataDistributionGenerator, AbstractMetadataGenerator, AbstractNetwork,
 * AbstractStorage, and AbstractIODispatcher
 */
class AbstractComponent {
protected:
	/// Configuration of the run
	Config& conf;

public:
	/** 
	 * Constructor
	 *
	 * \param conf Configuration
	 */
	AbstractComponent(Config& conf);

	/**
	 * Destructor
	 */
	 virtual ~AbstractComponent(){};

	/**
	 * Initialization function made to be overwritten.
	 * Called after the configurations have been initialized.
	 */
	virtual void init();
};

#endif
