/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

/**
 * \brief Structure with basic inforamtion needed for the replay.
 *
 * Simple struct used to send informations about the number of orders
 * to send and receive to each slave.
 */
struct Constants {
	/// Total number of objects in the data set.
	int total_number_objects;
	/// Total number of orders that will be processed by the node.
	int total_number_orders;
	/// Maximum tag value + 1;
	int number_of_tags;
};

#endif
