/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "AbstractComponent.hpp"

#include "Log.hpp"

#include "Config.hpp"

using namespace std;

AbstractComponent::AbstractComponent(Config& cf):conf(cf){}

void AbstractComponent::init(){}
