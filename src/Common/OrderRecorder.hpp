/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef ORDERRECORDER_HPP
#define ORDERRECORDER_HPP

#include <vector>
#include <memory>

#include "Defs.hpp"

struct Order;

/**
 * \brief Class that records every order needed to replay the data transfers.
 *
 *
 */
class OrderRecorder {
private:
	/// Orders per node
	std::vector<std::vector<Order>> orders;	
	/// Orders for the ranks that are not used during the replay
	std::vector<Order> orders_for_all;
	/// Rank to node conversion table
	std::vector<int> rank_to_node;

public:
	/**
	 * Constructor
	 *
	 * \param nb_ranks Number of ranks (or number of nodes) to consider.
	 */
	OrderRecorder(int nb_ranks);

	/**
	 * Add an order for a specific node
	 *
	 * \param nodeID NodeID of the node that will replay the order
	 * \param order Order to be replayed
	 */
	void add_order(NodeID nodeID, Order order);

	/**
	 * Add an order for all nodes.
	 *
	 * Warning: only global orders such as Wait are relevant here.
	 *
	 * \param order Order to add on all nodes.
	 */
	void add_order_for_all(Order order);

	/**
	 * Assign nodeID to the ranks that will replay the I/Os
	 * and add information needed for the replay of the orders.
	 */
	void assign_ranks();

	/**
	 * Return the orders that must be replayed by a specific rank.
	 *
	 * \param rnk Rank that will replay the orders.
	 * \return Orders assigned to the specified rank.
	 */
	std::vector<Order>& get_orders_for_rank(int rnk);

	/**
	 * Return the nodeID of a specific rank
	 *
	 * \param rnk Rank
	 * \return Return the nodeID assigned to the specified rank.
	 */
	NodeID nodeID_of_rank(int rnk);

	/**
	 * Duplicate orders to maintain consistency when dividing an
	 * object.
	 *
	 * \param original_obj_id ObjectID of the original object
	 * \param new_size New size of the original object
	 * \param second_part_obj_id ObjectID of the second part of the object
	 * \param second_part_size Size of the second part of the object
	 * \param order_id Current OrderID (used for the new orders)
	 * \param tag Current tag number (used for the new orders)
	 * \return Number of orders added.
	 */
	int duplicate_and_update_orders_for_object(ObjectID original_obj_id,
													ObjectSize new_size,
													ObjectID second_part_obj_id,
													ObjectSize second_part_size,
													int order_id,
													int tag);
};

#endif
