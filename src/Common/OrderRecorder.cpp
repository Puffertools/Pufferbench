/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "OrderRecorder.hpp"

#include <cassert>
#include <mpi.h>
#include <string>
#include <map>

#include "Log.hpp"
#include "Order.hpp"

using namespace std;

OrderRecorder::OrderRecorder(int nb_ranks){
	for (int i = 0; i < nb_ranks; ++i)
		orders.push_back(vector<Order>());
}

void OrderRecorder::add_order(NodeID nodeID, Order order){
	assert(nodeID < (int)orders.size());
	order.given_to = nodeID;
	orders[nodeID].push_back(order);
}

void OrderRecorder::add_order_for_all(Order order){
	for (unsigned int i = 0; i < orders.size(); ++i){
		order.given_to = i;
		orders[i].push_back(order);		
	}
	orders_for_all.push_back(order);
}

void OrderRecorder::assign_ranks(){
	int wsize;
	MPI_Comm_size(MPI_COMM_WORLD,&wsize);	
	
	assert(wsize >= (int)orders.size());
	assert(rank_to_node.size() == 0);

	rank_to_node.resize(wsize,-1);
	vector<int> node_to_rank(orders.size());

	vector<int> unassigned_ranks;
	for (int i = 0; i < wsize; ++i){
		unassigned_ranks.push_back(i);
	}

	LogTrace("Selecting "+to_string(orders.size())+" ranks among "+to_string(wsize));

	for (unsigned int i = 0; i < orders.size(); ++i){
		// randomly selecting unassigned rank
		int pos = rand()%unassigned_ranks.size();
		int rnk = unassigned_ranks[pos];
		// assign node i to that rank
		rank_to_node[rnk] = i;
		node_to_rank[i] = rnk;
		LogTrace(" | - Assigning rank "+to_string(rnk)+" to node ID "+to_string(i));
		
		// Removing from the unused ranks
		unassigned_ranks.erase(unassigned_ranks.begin()+pos);
	}

	// Go through all orders and apply node_to_rank

	for (unsigned int i = 0; i < orders.size(); ++i){
		for (unsigned int j = 0; j < orders[i].size(); ++j){
			orders[i][j].src_rank = node_to_rank[orders[i][j].src_nodeID];
			orders[i][j].dst_rank = node_to_rank[orders[i][j].dst_nodeID];
		}
	}
	for (unsigned int j = 0; j < orders_for_all.size(); ++j){
		orders_for_all[j].src_rank = node_to_rank[orders_for_all[j].src_nodeID];
		orders_for_all[j].dst_rank = node_to_rank[orders_for_all[j].dst_nodeID];
	}
}

vector<Order>& OrderRecorder::get_orders_for_rank(int rnk){
	if (rank_to_node[rnk]>=0){
		return orders[rank_to_node[rnk]];
	}
	return orders_for_all;
}

NodeID OrderRecorder::nodeID_of_rank(int rnk){
	return rank_to_node[rnk];
}

int OrderRecorder::duplicate_and_update_orders_for_object(ObjectID original_obj_id,
													ObjectSize new_size,
													ObjectID second_part_obj_id,
													ObjectSize second_part_size,
													int order_id,
													int tag){
	int orders_updated = 0;
	map<int,int> tag_conversion;
	for (unsigned int i = 0; i < orders.size(); ++i){
		vector<Order>::iterator it;
		for (it = orders[i].begin(); it != orders[i].end(); ){
			if (it->object_id != original_obj_id){
				++it;
				continue;
			}
			// If it is an order for original_obj_id
			// ... update size
			it->size = new_size;
			// ... create a copy for the second part
			Order new_order = (*it);
			new_order.object_id = second_part_obj_id;
			new_order.size = second_part_size;
			new_order.order_id = order_id + orders_updated;
			
			map<int,int>::iterator mit;
			mit = tag_conversion.find(it->tag);
			if (mit == tag_conversion.end()){
				new_order.tag = tag + orders_updated;
				tag_conversion[it->tag] = new_order.tag;
			} else {
				new_order.tag = mit->second;
			}
			it = orders[i].insert(it+1,new_order);
			orders_updated++;
		}
	}
	return orders_updated;
}

