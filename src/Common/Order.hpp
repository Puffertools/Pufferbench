/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef ORDER_HPP
#define ORDER_HPP

#include "Defs.hpp" 

/// Different types of orders
enum OrderType {
	/// Read data and send it.
	READ_SEND, 
	/// Receive data and write it.
	RECV_WRITE, 
	/// Receive data and send it.
	RECV_SEND,
	/// Wait
	WAIT
};

/**
 * \brief Informations about the transfer of an object between nodes.
 *
 * Struct recording all information for an order.
 */
struct Order {
	/// ID of the order, unique.
	OrderID order_id;
	/// Type of the order
	OrderType type;
	/// Priority (lower better)
	int priority;
	/// Tag, common to the send and recv order of a transfer operation
	int tag;

	/// Id of the object transfered
	ObjectID object_id;
	/// Size of the tansfered object
	ObjectSize size;
	/// NodeID of the node that receives this order
	NodeID given_to;
	/// NodeId of the source of the data transfer
	NodeID src_nodeID;
	/// Rank of the source of the data transfer
	int src_rank;
	/// NodeId of the target of the data transfer
	NodeID dst_nodeID;
	/// Rank of the target of the data transfer
	int dst_rank;
	
	/// Time of submission of the read operation
	double submission_read = 0;	
	/// Time of start of the read operation
	double start_read = 0;		
	/// Time of completion of the read operation
	double end_read = 0;		
	/// Time of submission of the send operation
	double submission_send = 0;	
	/// Time of start of the send operation
	double start_send = 0;
	/// Time of completion of the send operation
	double end_send = 0;
	/// Time of submission of the receive operation
	double submission_recv = 0;
	/// Time of start of the receive operation
	double start_recv = 0;
	/// Time of completion of the receive operation
	double end_recv = 0;
	/// Time of submission of the write operation
	double submission_write = 0;
	/// Time of start of the write operation
	double start_write = 0;
	/// Time of completion of the write operation
	double end_write = 0;
};

#endif
