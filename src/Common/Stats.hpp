/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef STATS_HPP
#define STATS_HPP

#include "Defs.hpp"

/**
 * \brief Simple structure used to record various stats.
 *
 * Structure used to record stats about the I/O during the
 * execution of the replay.
 */
struct Stats{
	/// Id of the node
	NodeID node;
	/// Attributed rank
	int rank;
	/// Duration of the replay on the node
	double time;				
	/// Overall duration of the replay
	double global_time;			
	/// Storage read throughput in B
	double read_throughput;		
	/// Storage write throughput in B
	double write_throughput;
	/// Network reception throughput in B
	double recv_throughput;			
	/// Network emission throughput in B
	double send_throughput;	
};

#endif
