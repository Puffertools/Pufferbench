/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "ComponentRegister.hpp"

#include <string>
#include <iostream>

#include "Config.hpp"
#include "GlobalConfiguration.hpp"
#include "ConfigurationKeys.hpp"

#include "AbstractIODispatcher.hpp"
#include "AbstractStorage.hpp"
#include "AbstractNetwork.hpp"

#include "AbstractMetadataGenerator.hpp"
#include "AbstractDataDistributionGenerator.hpp"
#include "AbstractDataTransferScheduler.hpp"

#include "Log.hpp"

using namespace std;

shared_ptr<ComponentRegister> ComponentRegister::p_instance;

shared_ptr<ComponentRegister> ComponentRegister::instance(){
	if (!p_instance)
		p_instance = make_shared<ComponentRegister>();
	return p_instance;
}

void ComponentRegister::registerFactory(string type, string comp, ComponentFactory* factory){	
	factories[type][comp] = factory;
}

void ComponentRegister::not_found_error(string type, string name){
	LogError("No "+type+" registered with the name: "+name);
	LogError("Available "+type+" are:");
	map<string,ComponentFactory*>::iterator it;
	for (it = factories[type].begin(); it != factories[type].end(); ++it){
		LogError(" - "+it->first);
	}
}


shared_ptr<AbstractMetadataGenerator> ComponentRegister::get_metadata_generator(Config& conf){
	string name = conf.get_parameter<string>({ConfigurationKeys::COMPONENTS_SECTION,
												ConfigurationKeys::GENERATION_KEY},
												ConfigurationKeys::GENERATION_DEFAULT);
	map<string,ComponentFactory*>::iterator it;
	it = factories["MetadataGenerator"].find(name);
	if (it != factories["MetadataGenerator"].end()){
		return dynamic_pointer_cast<AbstractMetadataGenerator>(it->second->create(conf));
	}
	not_found_error("MetadataGenerator",name);
	throw ComponentNotFoundException();
}

shared_ptr<AbstractDataDistributionGenerator> ComponentRegister::get_data_distribution_generator(Config& conf){
	string name = conf.get_parameter<string>({ConfigurationKeys::COMPONENTS_SECTION,
												ConfigurationKeys::ALLOCATION_KEY},
												ConfigurationKeys::ALLOCATION_DEFAULT);
	map<string,ComponentFactory*>::iterator it;
	it = factories["DataDistributionGenerator"].find(name);
	if (it != factories["DataDistributionGenerator"].end()){
		return dynamic_pointer_cast<AbstractDataDistributionGenerator>(it->second->create(conf));
	}
	not_found_error("DataDistributionGenerator",name);
	throw ComponentNotFoundException();
}

shared_ptr<AbstractDataTransferScheduler> ComponentRegister::get_data_transfer_scheduler(Config& conf){
	string name = conf.get_parameter<string>({ConfigurationKeys::COMPONENTS_SECTION,
												ConfigurationKeys::DATA_TRANSFER_KEY},
												ConfigurationKeys::DATA_TRANSFER_DEFAULT);
	map<string,ComponentFactory*>::iterator it;
	it = factories["DataTransferScheduler"].find(name);
	if (it != factories["DataTransferScheduler"].end()){
		return dynamic_pointer_cast<AbstractDataTransferScheduler>(it->second->create(conf));
	} 
	not_found_error("DataTransferScheduler",name);
	throw ComponentNotFoundException();
}

shared_ptr<AbstractIODispatcher> ComponentRegister::get_io_dispatcher(Config& conf){
	string name = conf.get_parameter<string>({ConfigurationKeys::COMPONENTS_SECTION,
												ConfigurationKeys::IODISPATCHER_KEY},
												ConfigurationKeys::IODISPATCHER_DEFAULT);
	map<string,ComponentFactory*>::iterator it;
	it = factories["IODispatcher"].find(name);
	if (it != factories["IODispatcher"].end()){
		return dynamic_pointer_cast<AbstractIODispatcher>(it->second->create(conf));
	} 
	not_found_error("IODispatcher",name);
	throw ComponentNotFoundException();
}

shared_ptr<AbstractNetwork> ComponentRegister::get_network(Config& conf){
	string name = conf.get_parameter<string>({ConfigurationKeys::COMPONENTS_SECTION,
												ConfigurationKeys::NETWORK_KEY},
												ConfigurationKeys::NETWORK_DEFAULT);
	map<string,ComponentFactory*>::iterator it;
	it = factories["Network"].find(name);
	if (it != factories["Network"].end()){
		return dynamic_pointer_cast<AbstractNetwork>(it->second->create(conf));
	} 
	not_found_error("Network",name);
	throw ComponentNotFoundException();
}

shared_ptr<AbstractStorage> ComponentRegister::get_storage(Config& conf){
	string name = conf.get_parameter<string>({ConfigurationKeys::COMPONENTS_SECTION,
												ConfigurationKeys::STORAGE_KEY},
												ConfigurationKeys::STORAGE_DEFAULT);
	map<string,ComponentFactory*>::iterator it;
	it = factories["Storage"].find(name);
	if (it != factories["Storage"].end()){
		return dynamic_pointer_cast<AbstractStorage>(it->second->create(conf));
	} 
	not_found_error("Storage",name);
	throw ComponentNotFoundException();
}

const char* ComponentRegister::ComponentNotFoundException::what() const noexcept {
	return "ComponentNotFoundException: The component that was requested does not exist.";
}
