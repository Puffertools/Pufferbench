/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "Slave.hpp"

#include <mpi.h>
#include <assert.h>
#include <string>

#include "Config.hpp"

#include "Stats.hpp"
#include "Constants.hpp"
#include "Order.hpp"
#include "Replay.hpp"

#include "Defs.hpp"

using namespace std;

void Slave::run(Config& conf){
	MPI_Barrier(MPI_COMM_WORLD);
	LogTrace("Running the slave routine.");

	// Retreive rank 
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	// Define data holder
	Constants constants;
	vector<Order> orders;
	
	// Receive constants
	int count = sizeof(Constants);
	MPI_Recv(&constants, count, MPI_BYTE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	
	// Create buffer to receive orders
	orders.resize(constants.total_number_orders);

	// Receive orders
	int to_receive = constants.total_number_orders;
	count = to_receive*sizeof(Order);	
	MPI_Recv(orders.data(), count, MPI_BYTE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

	Replay replay;

	// Run the replay
	Stats stats = replay.run(conf,constants,orders);
	
	MPI_Send(orders.data(), count, MPI_BYTE, 0, 0, MPI_COMM_WORLD);
	MPI_Send(&stats, sizeof(Stats), MPI_BYTE, 0, 1, MPI_COMM_WORLD);
	
	LogTrace("Slave routine finished.");
}


