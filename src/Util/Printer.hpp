/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef PRINTER_HPP
#define PRINTER_HPP

#include <vector>
#include <string>
#include <memory>

struct Stats;
struct Order;
class ClusterInfo;
class DataDistribution;
class DataSet;
class OrderRecorder;

/**
 * \brief Printer of recorded data.
 *
 * Class in charge of the output of the orders and the stats.
 */
class Printer {
public:

	/**
	 * Output the orders in a file named "orders.csv".
	 * 
	 * \param orders vector of orders
	 */
	void log_orders(const std::vector<Order>& orders);

	/**
	 * Output the stats in a file named "stats.csv"
	 *
	 * \param stats vector of stats
	 */
	void log_stats(const std::vector<Stats>& stats);

	/**
	 * Output the informations about the cluster in a file.
	 *
	 * \param filename Name of the file
	 * \param clusterInfo Informations to export
	 */
	void log_cluster_info(const std::string& filename, ClusterInfo& clusterInfo);
	
	/**
	 * Output the about of data stored by each node.
	 *
	 * \param filename Name of the file
	 * \param data_distribution Source of the informations
	 */
	void log_data_per_node(const std::string& filename, 
            const std::shared_ptr<DataDistribution>& data_distribution);

	/**
	 * Output the informations about the data set.
	 *
	 * \param filename Name of the file
	 * \param dataset Informations to export
	 */
	void log_dataset(const std::string& filename, const std::shared_ptr<DataSet>& dataset);

	/**
	 * Output the informations about the orders
	 *
	 * \param filename Name of the file
	 * \param order_recorder Source of the orders
	 * \param nb_ranks Number of ranks used for the replay
	 */
	void log_order_recorder(const std::string& filename, const std::shared_ptr<OrderRecorder>& order_recorder, int nb_ranks);
};

#endif
