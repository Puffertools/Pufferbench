/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "ConfigurationKeys.hpp"

const char* ConfigurationKeys::COMMON_FIELD = "Common";
const char* ConfigurationKeys::OPTION_FIELD = "Options";

const char* ConfigurationKeys::RUN_SECTION = "tests";
const char* ConfigurationKeys::COMPONENTS_SECTION = "components";
const char* ConfigurationKeys::LOG_SECTION = "logs";

const char* ConfigurationKeys::RUN_REPLAYS_KEY = "replay";
const char* ConfigurationKeys::REPLICATION_KEY = "replication_factor";
const char* ConfigurationKeys::SEEDS_KEY = "seeds";
const char* ConfigurationKeys::INITIAL_NUMBER_KEY = "initial_number_of_nodes";
const char* ConfigurationKeys::DELTA_NODES_KEY = "delta_nodes";
const char* ConfigurationKeys::GENERATION_KEY = "metadata_generator";
const char* ConfigurationKeys::ALLOCATION_KEY = "data_distribution_generator"; 
const char* ConfigurationKeys::DATA_TRANSFER_KEY = "data_transfer_scheduler";
const char* ConfigurationKeys::IODISPATCHER_KEY = "io_dispatcher";
const char* ConfigurationKeys::STORAGE_KEY = "storage";
const char* ConfigurationKeys::NETWORK_KEY = "network";
const char* ConfigurationKeys::OUTPUT_DIR_KEY = "output_folder";
const char* ConfigurationKeys::LOG_FILES_KEY = "files";
const char* ConfigurationKeys::LOG_MASTER_KEY = "master";
const char* ConfigurationKeys::LOG_SLAVES_KEY = "slaves";
const char* ConfigurationKeys::LOG_FORCED_SYNC_KEY = "forced_sync";
const char* ConfigurationKeys::LOG_COLOR_KEY = "color";

const bool ConfigurationKeys::RUN_REPLAYS_DEFAULT = true;
const int ConfigurationKeys::REPLICATION_DEFAULT = 3;
const int ConfigurationKeys::SEEDS_DEFAULT = 0;
const int ConfigurationKeys::INITIAL_NUMBER_DEFAULT = 0;
const int ConfigurationKeys::DELTA_NODES_DEFAULT = 0;
const std::string ConfigurationKeys::GENERATION_DEFAULT = "none";
const std::string ConfigurationKeys::ALLOCATION_DEFAULT = "none";
const std::string ConfigurationKeys::DATA_TRANSFER_DEFAULT = "none";
const std::string ConfigurationKeys::IODISPATCHER_DEFAULT = "none";
const std::string ConfigurationKeys::NETWORK_DEFAULT = "none";
const std::string ConfigurationKeys::STORAGE_DEFAULT = "none";
const std::string ConfigurationKeys::OUTPUT_DIR_DEFAULT = "Results";
const std::string ConfigurationKeys::LOG_FILES_DEFAULT = "INFO IMPORTANT WARN ERROR CONFIG";
const std::string ConfigurationKeys::LOG_MASTER_DEFAULT = "INFO IMPORTANT WARN ERROR";
const std::string ConfigurationKeys::LOG_SLAVES_DEFAULT = "ERROR WARN";
const bool ConfigurationKeys::LOG_FORCED_SYNC_DEFAULT = false;
const bool ConfigurationKeys::LOG_COLOR_DEFAULT = true;

