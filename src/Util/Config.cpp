/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "Config.hpp"

#include <cassert>

#include "Log.hpp"

using namespace std;

Config::Config(YAML::Node node, const string& header,
        const shared_ptr<map<string,vector<string>>>& conf_fields){
	config = node;
	config_header = header;
	config_fields = conf_fields;
}


void Config::set_experiment_number(int exp){
	exp_number = exp;
}

int Config::get_experiment_number(){
	return exp_number;
}

void Config::log(){
	YAML::Emitter out;
	out << config;
	LogConf("Configuration of this run: \n"+string(out.c_str()));
}

string Config::get_yaml(){
	YAML::Emitter out;
	out << config;
	string str = string(out.c_str());
	str.resize(out.size());
	return str+"\n";
}

string Config::get_csv(){	
	if (config_csv != "")
		return config_csv;
	
	config_csv = to_string(exp_number);	
	map<string,vector<string>>::iterator it;
	vector<const char*> cv;
	for (it = config_fields->begin(); it != config_fields->end(); ++it){
		cv.clear();
		for (string& s : it->second){
			cv.push_back(s.data());
		}
		bool found = true;
		config_csv += ","+get_parameter<string>(config,cv,string("NA"),&found);
	}	
	return config_csv;
}

string Config::get_csv_header(){
	return config_header;
}

ObjectSize Config::get_size_parameter(initializer_list<const char*> fields, ObjectSize default_value){
	string content = get_parameter<string>(fields,to_string(default_value));
	if (content == to_string(default_value)){
		return default_value;
	}
	
	int number_end = -1;
	const char space = ' ';
	const char zero = '0';
	const char nine = '9';
	const char point = '.';

	int pos = 0;
	string::iterator it;
	for (it = content.begin(); it != content.end(); ){
		if (*it == space){
			it = content.erase(it);
			continue;
		}
		if (number_end < 0 && (*it) != point && ( (*it) < zero || (*it) > nine)){
			number_end = pos;
		}
		pos++;
		++it;
	}
	double nb;
	if (number_end == -1){
		nb = stol(content);		
		return (ObjectSize) nb;
	} else {
		nb = stod(content.substr(0,number_end));
		string unit = content.substr(number_end,content.npos);
		if (unit == "B") 
			return (ObjectSize) nb;
		if (unit == "kB") 
			return (ObjectSize)(1000L * nb);
		if (unit == "MB") 
			return (ObjectSize) (1000000L * nb);
		if (unit == "GB") 
			return (ObjectSize) (1000000000L * nb);
		if (unit == "KiB") 
			return (ObjectSize) (1024L*nb);
		if (unit == "MiB") 
			return (ObjectSize) (1024L*1024*nb);
		if (unit == "GiB") 
			return (ObjectSize) (1024L*1024*1024*nb);
		LogTrace(" - Unit unknown return default value");
	}
	return default_value;	
}

