/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "FileManager.hpp"


#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>

#include <mpi.h>
#include <assert.h>
#include <memory>

#include "Log.hpp"
#include "Config.hpp"
#include "ConfigurationKeys.hpp"

using namespace std;

shared_ptr<FileManager> FileManager::p_instance;

shared_ptr<FileManager> FileManager::instance(){
	if (!p_instance)
		p_instance = make_shared<FileManager>();
	return p_instance;
}

bool FileManager::is_directory(const char* path){
	// C style
	// Can do better with C++17
	struct stat statbuf;
	if (stat(path, &statbuf) != -1) {
   		if (S_ISDIR(statbuf.st_mode)) {
         	return true;
		}
	} else {
		if (errno == ENOENT){
			LogTrace("Path "+string(path)+" does not exist.");
		} else if (errno == EACCES){ 
			LogError("Cannot access path due to access rights");
			throw OutputFolderException();
		} else if (errno == ENOTDIR){ 
			LogError("Path is not a path.");
			throw OutputFolderException();
		}
	}
	return false;
}

void FileManager::create_directory(const char* path){
	// C Style
	// Can do better with C++17
	int mkdir_err = mkdir(path,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);	
	if (mkdir_err != 0){
		if (errno == EEXIST){
			LogTrace("Folder "+string(path)+" already exists.");	
		} else {
			LogError("Error ("+to_string(errno)+") when creating folder "+string(path));
			throw OutputFolderException();
		}
	}
	LogTrace("Created directory "+string(path));
}

void FileManager::create_output_dir(const string& output_dir){
	LogTrace("Creating the output folder.");
	
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int year;
	int month;
	int day;
	int hour;
	int min;
	int sec;

	if (rank == 0){
		time_t now = time(0);
		tm* ltm = localtime(&now);
		year = 1900+ltm->tm_year;
		month = 1+ltm->tm_mon;
		day = ltm->tm_mday;
		hour = ltm->tm_hour;
		min = ltm->tm_min;
		sec = ltm->tm_sec;
	
	
		LogInfo("Date is "+to_string(year)+"-"+to_string(month)+"-"
					+to_string(day)+" "+to_string(hour)+":"+to_string(min)
					+":"+to_string(sec));
	
	} 

	MPI_Bcast(&year, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&month, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&day, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&hour, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&min, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&sec, 1, MPI_INT, 0, MPI_COMM_WORLD);

	LogTrace("Time of the master broadcasted");

	LogTrace("Creating folder.");

	string parent_directory = output_dir;
	
	if (!is_directory(parent_directory.c_str())){
		LogImportant("Output directory "+parent_directory+" does not exist, creating it.");
		create_directory(parent_directory.c_str());
	}

	string sday = to_string(day);
	if (sday.length() == 1)
		sday = "0"+sday;
	string smonth = to_string(month);
	if (smonth.length() == 1)
		smonth = "0"+smonth;
	string shour = to_string(hour);
	if (shour.length() == 1)
		shour = "0"+shour;
	string smin = to_string(min);
	if (smin.length() == 1)
		smin = "0"+smin;
	string ssec = to_string(sec);
	if (ssec.length() == 1)
		ssec = "0"+ssec;

	main_folder = parent_directory+dir_sep+to_string(year)+"-"+smonth
					+"-"+sday+"_"+shour+smin+ssec;

	create_directory(main_folder.c_str());
	LogInfo("Output folder "+main_folder+" has been created.");

	main_log_folder = main_folder + dir_sep + "logs";

	local_folder = main_folder + dir_sep + "local";
	create_directory(local_folder.c_str());
	LogInfo("Output folder "+local_folder+" for local logs has been created.");
}

string FileManager::get_log_name(const string& proc_name, int rank){
	return "log-"+proc_name+"-"+to_string(rank)+".log";
}

void FileManager::add_local_log(int log_level){

	assert(local_folder.length()>0);

	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	
	char proc_name[MPI_MAX_PROCESSOR_NAME];
	int proc_name_len;
	MPI_Get_processor_name(proc_name,&proc_name_len);

	string local_log_file_name = local_folder+dir_sep+get_log_name(string(proc_name),rank);
	local_log_file = make_shared<ofstream>();
	local_log_file_init = true;
	local_log_file->open(local_log_file_name.c_str());
	
	Log::instance()->addLog(local_log_file.get(),log_level, false);

	LogTrace("Started to log into "+local_log_file_name);
	LogTrace("Rank "+to_string(rank)+" processor is named "+string(proc_name));
}


void FileManager::close_local_log(){
	if (local_log_file_init)
		local_log_file->close();	
}

void FileManager::add_run_log(int log_level){
	if (!current_dir_open){
		LogError("No current directory opened, failing.");
		throw OutputFolderException();
	}
	if (is_run_log_open){
		LogError("Run log is already open");
		throw OutputFolderException();
	}
	string log_name = current_dir + dir_sep + "log.log";
	run_log_file = make_shared<ofstream>();
	run_log_file->open(log_name.c_str());

	Log::instance()->addLog(run_log_file.get(),log_level, false);
	is_run_log_open = true;

	LogInfo("Logging run into "+log_name);
}

void FileManager::close_run_log(){
	if (!is_run_log_open){
		LogError("Run log is not open");
		throw OutputFolderException();
	}
	LogInfo("Closing run log.");
	Log::instance()->removeLog(run_log_file.get());
	run_log_file->close();
	is_run_log_open = false;
	run_log_file.reset();
}

void FileManager::gather_logs(ostringstream* log_stream){
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	char proc_name[MPI_MAX_PROCESSOR_NAME];
	int proc_name_len;
	MPI_Get_processor_name(proc_name,&proc_name_len);

	LogTrace("Sending logs to master");

	if (rank == 0){
		create_directory(main_log_folder.c_str());

		int wsize;
		MPI_Comm_size(MPI_COMM_WORLD,&wsize);

		string file_name;

		for (int i = 1; i < wsize; i++){
			MPI_Recv(proc_name, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, i,0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			int size;
			MPI_Recv(&size, 1, MPI_INT, i,1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			vector<char> content(size);
			
			MPI_Recv(content.data(), size, MPI_CHAR, i, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			file_name = main_log_folder + dir_sep + get_log_name(string(proc_name),i);
			unique_ptr<ofstream> file = unique_ptr<ofstream>(new ofstream());
			file->open(file_name);
			*file << content.data();
			file->close();
			
			LogTrace("Log from rank "+to_string(i)+" received and stored into "+file_name);
		}
		
		LogInfo("All logs received.");
		
		// Write current local log into folder
		file_name = main_log_folder + dir_sep + get_log_name(string(proc_name),0);
		unique_ptr<ofstream> file = unique_ptr<ofstream>(new ofstream());
		file->open(file_name);
		*file << log_stream->str();
		file->close();
	} else {
		string content = log_stream->str();
		MPI_Send(proc_name, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
		int size = content.length();
		MPI_Send(&size, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
		MPI_Send(content.c_str(),content.length(), MPI_CHAR, 0, 2, MPI_COMM_WORLD);
	}
}



void FileManager::open_current_dir(const string& name){
	current_dir_open = false;
	current_dir = main_folder + dir_sep + name;
	LogTrace("Opening "+current_dir+" as current directory.");
	if (is_directory(current_dir.c_str())){
		LogError("Folder "+name+" exists, aborting.");
		throw OutputFolderException();
	}
	create_directory(current_dir.c_str());
	current_dir_open = true;
}

void FileManager::print_file(const string& file_name, const string& content){
	if (!current_dir_open){
		LogError("No current directory opened, failing.");
		throw OutputFolderException();
	}
	
	string name = current_dir + dir_sep + file_name ;
	ofstream file;
	file.open(name);
	file << content;
	file.close();
	
	LogTrace("Data written into file "+name);

}

void FileManager::print_global_file(const string& file_name, const string& content){
	string name = main_folder + dir_sep + file_name ;
	ofstream file;
	file.open(name, std::ofstream::out | std::ofstream::app);
	file << content << endl;
	file.close();
	
	LogTrace("Data written into file "+name);
}

