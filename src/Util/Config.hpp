/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <vector>
#include <string>
#include <map>
#include <sstream>
#include <cstring>
#include <exception>
#include <initializer_list>
#include <memory>

#include "yaml-cpp/yaml.h"

#include "ConfigurationKeys.hpp"

#include "Log.hpp"
#include "Defs.hpp"

/**
 * \brief Configuration of the tests.
 *
 * Configuration class that gives access to the content of the configuration file
 * hosted on master.
 */
class Config{
private:
	/// YAML base node
	YAML::Node config;

	/**
	 * Return the requested configuration if it exist, the default value if it cannot be found.
	 * 
	 * \param node YAML::Node to look into 
	 * \param fields sequence of keys used to find the requested parameter
	 * \param default_value Value to be returned if the keys cannot be found in the configuration
	 * \param found boolean indicating whether the value has been found
	 * \return Requested parameter or default_value
	 */
	template<typename A> 
	A get_parameter(YAML::Node& node, std::vector<const char*>& fields, 
						const A& default_value, bool* found);
	
	/// number of the run 
	int exp_number = -1;

	/// Map of the fields of the configuration that must be outputed into the csv
	std::shared_ptr<std::map<std::string,std::vector<std::string>>> config_fields;

	/// Header of the csv used to output the configuration values
	std::string config_header;

	/// Content of the configuration in a csv format when generated
	std::string config_csv;

public:
	/**
	 * Initialize a config with a YAML:Node
	 *
	 * \param node YAML::Node used to configure the config, 
	 * it should not contain any sequence.
	 * \param header Header of the csv for the output of the 
	 * configuration to file.
	 * \param conf_fields Map of the fields to output in the csv
	 * file.
	 */
	Config(YAML::Node node, const std::string& header,
		const std::shared_ptr<std::map<std::string,std::vector<std::string>>>& conf_fields);
	
	/**
	 * Set the experiment number of the run
	 *
	 * \param exp run number
	 */
	void set_experiment_number(int exp);

	/**
	 * Indicates the number of the current run.
	 *
	 * \return the number of the current run.
	 */
	int get_experiment_number();

	/**
	 * Output the configuration into the logs.
	 */
	void log();

	/**
	 * Return the yaml of the configuration.
	 *
	 * \return yaml formatted string 
	 */
	std::string get_yaml();

	/**
	 * Return the values of the configuration in a csv format
	 *
	 * \return value of the parameters in a csv format.
	 */
	std::string get_csv();
	
	/**
	 * Return the keys of the configuration in a csv format
	 *
	 * \return header of the parameters in a csv format.
	 */
	std::string get_csv_header();

	/**
	 * Overload of the function get_parameter to read size values with B.
	 * kB, MB, GB, KiB, MiB, and GiB.
	 *
	 * \param fields Path in the conf to find the parameter
	 * \param default_value Default value in case the parameter is not found
	 * \return the parameter or the default value
	 */
	ObjectSize get_size_parameter(std::initializer_list<const char*> fields, 
									ObjectSize default_value);

	/**
	 * Return a parameter or the default value if it can not be read.
	 *
	 * \param fields Sequence of keys to follow to find the requested parameter
	 * \param default_value value that is returned if the configuration cannot 
	 * be read
	 * \return requested parameter from the configuration or default_value 
	 */
	template<typename A> 
    A get_parameter(std::initializer_list<const char*> fields, const A& default_value){
		std::vector<const char*> vfields;
		vfields.push_back(ConfigurationKeys::OPTION_FIELD);
		std::initializer_list<const char*>::iterator it;
		std::string str;
		for (it = fields.begin(); it != fields.end(); ++it){
			vfields.push_back(*it);
			str += std::string(*it) + ": ";
		}
		std::vector<const char*> vfields2 = vfields;
		LogTrace("Reading configuration for "+str);
		bool found = false;
		A value = get_parameter<A>(config,vfields, default_value,&found);
		if (found){
			return value;
		}
		vfields2[0] = ConfigurationKeys::COMMON_FIELD;
		return get_parameter<A>(config,vfields2,default_value,&found);
	}
};

template<typename A> 
A Config::get_parameter(YAML::Node& node, std::vector<const char*>& fields, const A& default_value, bool* found){
	*found = false;
	if (fields.empty()){
		if (node.IsScalar()){
			*found = true;
			return node.as<A>();
		}
		LogTrace(" - Returning default value.");
		return default_value;
	}
	if (!node.IsDefined()){
		LogTrace(" - Not defined, returning default value.");
		return default_value;
	}
	if (!node.IsMap()){
		LogTrace(" - Returning default value.");
		return default_value;
	}
	const char* f = *(fields.begin());
	fields.erase(fields.begin());
	YAML::Node next_node = node[f];
	return get_parameter<A>(next_node,fields,default_value,found);
}

#endif
