/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "Printer.hpp"

#include <fstream>
#include <sstream>

#include <assert.h>

#include "Defs.hpp"
#include "Log.hpp"
#include "Stats.hpp"
#include "Order.hpp"

#include "ClusterInfo.hpp"
#include "Node.hpp"
#include "DataSet.hpp"
#include "Object.hpp"
#include "DataDistribution.hpp"

#include "OrderRecorder.hpp"

#include "FileManager.hpp"

using namespace std;

void Printer::log_orders(const vector<Order>& orders){
	LogInfo("Logging orders");	

	ostringstream stream;

	stream << "OrderID,Type,Priority,tag,ObjectID,ObjectSize,givento,src_nodeID,src_rank,";
	stream << "dst_nodeID,dst_rank,sub_read,start_read,end_read,sub_send,start_send," ;
	stream << "end_send,sub_recv,start_recv,end_recv,sub_write,start_write,end_write";
	stream << endl;

	for (const Order& o : orders){
		stream << o.order_id << ",";
		if (o.type == READ_SEND){
			stream << "READ_SEND,";
		} else if (o.type == RECV_WRITE){
			stream << "RECV_WRITE,";
		} else if (o.type == RECV_SEND){
			stream << "RECV_SEND,";
		} else if (o.type == WAIT){
			stream << "WAIT,";
		} else {
			assert(false);
		}
		stream << o.priority << ",";
		stream << o.tag << ",";
		stream << o.object_id << ",";
		stream << o.size << ",";
		stream << o.given_to << ",";
		stream << o.src_nodeID << ",";
		stream << o.src_rank << ",";
		stream << o.dst_nodeID << ",";
		stream << o.dst_rank << ",";
		stream << fixed << o.submission_read << ",";
		stream << fixed << o.start_read << ",";
		stream << fixed << o.end_read << ",";
		stream << fixed << o.submission_send << ",";
		stream << fixed << o.start_send << ",";
		stream << fixed << o.end_send << ",";
		stream << fixed << o.submission_recv << ",";
		stream << fixed << o.start_recv << ",";
		stream << fixed << o.end_recv << ",";
		stream << fixed << o.submission_write << ",";
		stream << fixed << o.start_write << ",";
		stream << fixed << o.end_write ;
		stream << endl;

	}
	FileManager::instance()->print_file("orders.csv",stream.str());
}

void Printer::log_stats(const vector<Stats>& stats){
	LogInfo("Logging stats");	

	ostringstream stream;

	stream << "NodeID,rank,time(s),read(B/s),write(B/s),recv(B/s),send(B/s)" << endl;

	for (const Stats& s : stats){
		stream << s.node << ",";
		stream << s.rank << ",";
		stream << s.time << ",";
		stream << fixed << s.read_throughput << ",";
		stream << fixed << s.write_throughput << ",";
		stream << fixed << s.recv_throughput << ",";
		stream << fixed << s.send_throughput << ",";
		stream << endl;
	}

	FileManager::instance()->print_file("stats.csv",stream.str());
}

void Printer::log_cluster_info(const string& filename, ClusterInfo& cluster_info){
	LogTrace("Logging ClusterInfo into "+filename);
	ostringstream stream;
	stream << "NodeID,in_decommission,in_commission" << endl;
	const vector<shared_ptr<Node>>& nodes = cluster_info.get_nodes();
	for (const shared_ptr<Node>& node : nodes){
		stream << node->get_nodeID() << ",";
		if (node->is_in_decommission()){
			stream << "true,";
		} else {
			stream << "false,";
		}
		if (node->is_in_commission()){
			stream << "true" << endl;
		} else {
			stream << "false" << endl;
		}
	}
	FileManager::instance()->print_file(filename,stream.str());
}

void Printer::log_dataset(const string& filename, const shared_ptr<DataSet>& dataset){
	LogTrace("Logging DataSet into "+filename);
	ostringstream stream;
	stream << "object,size" << endl;
    const vector<shared_ptr<Object>>& objects = dataset->get_objects();
	for (const shared_ptr<Object>& obj : objects){
		stream << obj->get_objectID() << ",";
		stream << obj->get_size() << endl;
	}
	FileManager::instance()->print_file(filename,stream.str());
}

void Printer::log_data_per_node(const string& filename, 
        const shared_ptr<DataDistribution>& data_distribution){
	LogTrace("Logging DataDistribution into "+filename);
	ostringstream stream;
	stream << "NodeID,data" << endl;
	const vector<shared_ptr<Node>>& nodes = data_distribution->get_cluster_info().get_nodes();
	for (const shared_ptr<Node>& node : nodes){
		stream << node->get_nodeID() << ",";
		stream << data_distribution->get_amount_data_hosted(node) << endl;
	}
	FileManager::instance()->print_file(filename,stream.str());
}

void Printer::log_order_recorder(const string& filename,
        const shared_ptr<OrderRecorder>& order_recorder, int nb_ranks){
	LogTrace("Logging node to rank distribution into "+filename);
	ostringstream stream;
	stream << "rank,nodeID" << endl;
	for (int i = 0; i < nb_ranks; ++i){
		stream << i << ",";
		stream << order_recorder->nodeID_of_rank(i) << endl;
	}
	FileManager::instance()->print_file(filename,stream.str());
}


