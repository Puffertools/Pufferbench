/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "Log.hpp"

#include <sys/time.h>
#include <iomanip>
#include <sstream>
#include <vector>
#include <unistd.h>
#include <assert.h>

#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>

using namespace std;

shared_ptr<Log> Log::p_instance = shared_ptr<Log>();

shared_ptr<Log> Log::instance(){
	if (!p_instance)
		p_instance = shared_ptr<Log>(new Log());
	return p_instance;
}

string Log::to_str(ObjectSize size){
	if (size < 1000){
		return to_string(size)+" B";
	}
	long ment;
	long dec;
	string unit;
	if (size < 1000000L){
		ment = size / 1000;
		dec = (size - ment*1000)/10;
		unit = " kB";
	} else if (size < 1000000000L){
		ment = size / 1000000L;
		dec = (size - ment*1000000L)/10000L;
		unit = " MB";
	} else {
		ment = size / 1000000000L;
		dec = (size - ment*1000000000L)/10000000L;
		unit = " GB";
	}
	if (dec >= 10)
		return to_string(ment)+"."+to_string(dec)+unit;
	return to_string(ment)+".0"+to_string(dec)+unit;
}

Log::Log(){
	tag = "["+to_string(::getpid())+"]";

	tag_level_console[TRACE] 		= "\033[30;1mTRACE\033[0m  ";
	tag_level_console[INFO]  		= "\033[1mINFO\033[0m   ";
	tag_level_console[IMPORTANT]	= "\033[33;1mIMP.\033[0m   ";
	tag_level_console[WARN]			= "\033[31;1mWARN\033[0m   ";
	tag_level_console[ERROR]		= "\033[33;41;1mERROR\033[0m  ";
	tag_level_console[TRANSFER] 	= "\033[30;1mTRAN.\033[0m   ";
	tag_level_console[DISTRIBUTIONGENERATOR] 	= "\033[30;1mDIST.\033[0m   ";
	tag_level_console[METADATAGENERATOR] 		= "\033[30;1mMETA.\033[0m   ";
	tag_level_console[DISPATCHER] 				= "\033[30;1mDISP.\033[0m   ";
	tag_level_console[STORAGE] 		= "\033[30;1mSTOR.\033[0m   ";
	tag_level_console[NETWORK] 		= "\033[30;1mNET.\033[0m    ";
	tag_level_console[CONFIG] 		= "\033[30;1mCONF.\033[0m   ";

	tag_level[ERROR] 				= "ERROR  ";
	tag_level[WARN] 				= "WARN   ";
	tag_level[IMPORTANT] 			= "IMP.   ";
	tag_level[INFO] 				= "INFO   ";
	tag_level[TRACE] 				= "TRACE  ";
	tag_level[TRANSFER] 			= "TRAN.  ";
	tag_level[DISTRIBUTIONGENERATOR] 			= "DIST.  ";
	tag_level[METADATAGENERATOR] 			= "META.  ";
	tag_level[DISPATCHER] 				= "DISP.  ";
	tag_level[STORAGE] 				= "STOR.  ";
	tag_level[NETWORK] 				= "NET.   ";
	tag_level[CONFIG] 				= "CONF.  ";
}

void Log::addLog(ostream* out, int mask, bool sync)
{
	shared_ptr<LogOutput> log(new LogOutput);
	log->out = out;
	log->mask = mask;
	log->sync = sync;
	log->initialized = messages.size();
	logs.push_back(log);
	if (!initialized){
		for (LogMessage lm : messages){
			if ((lm.level & mask) == lm.level) {
				print_message(out, lm.level, lm.msg, lm.file, lm.line,lm.time);
			}
		}
	}
	relevant_masks |= mask;
}

void Log::removeLog(ostream* out)
{
	flush();
	vector<shared_ptr<LogOutput>>::iterator it;
	relevant_masks = 0;
	for (it = logs.begin(); it != logs.end();){
		if ((*it)->out == out){
			it = logs.erase(it);
		} else {
			relevant_masks |= (*it)->mask;
			++it;
		}
	}
}

void Log::closeLogs()
{
	if (initialized)	
		flush();
	logs.clear();
}

void Log::initialize(bool color){
	assert(!initialized);
	use_color = color;
	for (shared_ptr<LogOutput> log_output : logs){
		if (!log_output->sync){
			int log_mask = log_output->mask;
			for (unsigned int i = log_output->initialized; i < messages.size(); ++i){
				LogMessage lm = messages[i];
				if ((lm.level & log_mask) == lm.level) {
					print_message(log_output->out, lm.level, lm.msg, lm.file, lm.line, lm.time);
				}
			}
		}
	}
	messages.clear();
	initialized = true;
}

void Log::flush(){
	assert(initialized);
	if (force_sync)
		return ;
	for (shared_ptr<LogOutput> log_output : logs){
		if (!log_output->sync){
			int log_mask = log_output->mask;
			for (LogMessage lm : messages){
				if ((lm.level & log_mask) == lm.level) {
					print_message(log_output->out, lm.level, lm.msg, lm.file, lm.line, lm.time);
				}
			}
		}
	}
	messages.clear();
}

void Log::set_force_sync(bool sync){
	if (sync)
		LogInfo("Forcing the synchronisation of the logs.");
	force_sync = sync;
}

void Log::print_message(std::ostream* out, int level, const string& msg, const string& file, int line, const string& time){

	bool console_output = (out == &cout) & use_color;

	if (console_output) {
		*out << "\033[37m";
	}

	*out << time;
	*out << tag << " ";

	if (console_output) {
		*out << "\033[0m";
	}

	if (console_output) {
		*out << tag_level_console[level];
	}
	else {
		*out << tag_level[level];
	}

	*out << fixed << setw(45) << setfill(' ') << left << msg;
	if (console_output) {
		*out << "\033[30;1m";
	}

	size_t last_slash = file.rfind('/')+1;
	string short_file = file.substr(last_slash);

	*out << " (" << short_file << ":" << line << ")";
	if (console_output) {
		*out << "\033[0m";
	}
	*out << endl;
	out->flush();
}


void Log::log(int level, const string& msg, const string& file, int line){
	// Check if the message should be kept
	if (!initialized || (level & relevant_masks) != 0){
		
		// Compute time of the message
		bool synced = true;
		struct timeval time;
		gettimeofday(&time, NULL);
		
		long seconds = time.tv_sec % 60;
		long min = (time.tv_sec / 60) % 60;
		long hours = (time.tv_sec / 3600) % 24;
	
		long us = time.tv_usec % 1000;
		long ms = time.tv_usec / 1000;
	
		ostringstream time_str;
		
		time_str << fixed << setw(2) << right << setfill('0') << hours << ":";
		time_str << fixed << setw(2) << right << setfill('0') << min << ":";
		time_str << fixed << setw(2) << right << setfill('0') << seconds << " ";
		time_str << fixed << setw(3) << right << setfill('0') << ms << " ";
		time_str << fixed << setw(3) << right << setfill('0') << us << " ";
		
		// Print message for synched outputs
		for (shared_ptr<LogOutput> log : logs) {
			int log_mask = log->mask;
			ostream* out = log->out;
			if (force_sync || log->sync){
				if ((level & log_mask) == level) {
					print_message(out, level, msg, file, line, time_str.str());
				}
			} else {
				synced = false;
			}
		}
		// record message
		if (!initialized || !synced){
			LogMessage log_msg;
			log_msg.level = level;
			log_msg.msg = msg;
			log_msg.file = file;
			log_msg.line = line;
			log_msg.time = time_str.str();
			messages.push_back(log_msg);
		}
	}
}

int Log::to_loglevel(const string& str){
	int level = ERROR;
	istringstream tokens_str(str);
	vector<string> tokens;
	copy(istream_iterator<string>(tokens_str),
    		istream_iterator<string>(),
	      	back_inserter(tokens));
	for (string token : tokens){
		// Generic 
		if (token == "WARN")
			level |= WARN;
		if (token == "IMPORTANT")
			level |= IMPORTANT;
		if (token == "INFO")
			level |= INFO;
		if (token == "TRACE")
			level |= TRACE;
		if (token == "ALL")
			level |= TRACE | INFO | IMPORTANT | WARN;
		// Sections
		if (token == "TRANSFER")
			level |= TRANSFER;
		if (token == "METADATAGENERATOR")
			level |= METADATAGENERATOR;
		if (token == "DISTRIBUTIONGENERATOR")
			level |= DISTRIBUTIONGENERATOR;
		if (token == "DISPATCHER")
			level |= DISPATCHER;
		if (token == "STORAGE")
			level |= STORAGE;
		if (token == "NETWORK")
			level |= NETWORK;
		if (token == "CONFIG")
			level |= CONFIG;
	}
	LogInfo("Log level set to "+to_string(level));
	return level;
}
