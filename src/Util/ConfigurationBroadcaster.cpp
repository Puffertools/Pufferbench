/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "ConfigurationBroadcaster.hpp"

#include <iostream>
#include <mpi.h>
#include <stdlib.h>
#include <exception>
#include <vector>

#include "Config.hpp"
#include "GlobalConfiguration.hpp"
#include "ConfigurationKeys.hpp"
#include "Log.hpp"
#include "FileManager.hpp"

using namespace std;

shared_ptr<ConfigurationBroadcaster> ConfigurationBroadcaster::p_instance;


shared_ptr<ConfigurationBroadcaster> ConfigurationBroadcaster::instance(){
	if (!p_instance)
		p_instance = make_shared<ConfigurationBroadcaster>();
	return p_instance;
}



GlobalConfiguration ConfigurationBroadcaster::bcast_config(const string& conf_file){
	
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	
	LogInfo("MPIRank: "+to_string(rank));

	GlobalConfiguration gconf;

	int config_size;
	vector<char> config_content;

	if (rank == 0) {	
		string config_content_str = gconf.read(conf_file);
		config_size = config_content_str.length();
	
		config_content.resize(config_size);

		strcpy(config_content.data(),config_content_str.c_str());
		LogTrace("Config has size "+to_string(config_size));
	}		
	
	MPI_Bcast(&config_size, 1, MPI_INT, 0, MPI_COMM_WORLD);
	
	if (rank != 0) {
		config_content.resize(config_size);
	}

	MPI_Bcast(config_content.data(), config_size, MPI_CHAR, 0, MPI_COMM_WORLD);

	LogInfo("Configuration broadcasted.");

	gconf.load(config_content.data());

	LogTrace("Config loaded.");

	return gconf;
}

