/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef GLOBALCONFIGURATION_HPP
#define GLOBALCONFIGURATION_HPP

#include <vector>
#include <string>
#include <map>
#include <sstream>
#include <cstring>
#include <exception>
#include <memory>

#include "yaml-cpp/yaml.h"

#include "Log.hpp"

class Config;

/**
 * \brief Global configuration.
 *
 * Configuration class that provides Config according to the configuration
 * file given in parameter.
 */
class GlobalConfiguration{
private:
	/// YAML base node
	YAML::Node config;

	/// Indicates if the YAML::Node config is loaded
	bool loaded = false;

	/// Various types of TreePosition nodes
	enum node_type {
		/// If the YAML::Node is a Map
		MAP,
		/// If the YAML::Node is a Sequence
		SEQUENCE,
		/// If the YAML::Node is a Scalar
		SCALAR,
		/// If the YAML::Node is Null
		NUL
	};

	/// Tree struct for the next configuration to provide
	/// We only consider the sequences in the YAML file
	struct TreePosition{
		/// Type of the TreePosition, must match the YAML::Node
		node_type type; 
		/// Current position for that sequence
		unsigned int current = 0;
		/// maximum position for that sequence
		unsigned int max = 0;
		/// positions in the sub sequences
		std::vector<TreePosition> children;	
		/// Map TreePosition if the node is a map
		std::map<std::string,TreePosition> map_children;
	};
	
	/**
	 * Return the next configuration of parameters.
	 *
	 * \param pos number of sequences encountered when going through
	 * the YAML configuration (in depth tree)
	 * \param increase True if it should return the following item in the next sequence
	 * \param to_visit YAML::Node being explored
	 */
	YAML::Node get_next(TreePosition& pos, bool* increase, YAML::Node& to_visit);

	/**
	 * Initialize the global configuration
	 *
	 * \param to_visit YAML:Node to initialize
	 * \return TreePosition matching the YAML::Node
	 */
	TreePosition initialize(YAML::Node& to_visit);

	/**
	 * Initialize the header and map of fields for the csv ouput of the configuration
	 *
	 * \param to_visit YAML::Node to initialize
	 * \param path Sequence of configuration fields followed to reach to_visit
	 */
	void initialize_output(YAML::Node& to_visit, std::vector<std::string> path);

	/**
	 * Count the total number of possible configurations
	 *
	 * \param pos TreePosition
	 * \return the number of configurations in the configuration.
	 */
	int count_combinaisons(TreePosition& pos);

	/// Position of the configuration in the various sequences 
	/// of the yaml file
	TreePosition next_positions;

	/// Number of possible combinaisons
	int combinaisons = 1;

	/// True if the current positions are the last possible ones
	bool last = false;

	/// Indicates if the position vector has been initialized
	bool initialized = false;

	/// Map of the fields of the configuration that must be outputed into the csv
	std::shared_ptr<std::map<std::string, std::vector<std::string>>> config_fields;

	/// Header of the csv used to output the configuration values
	std::string config_header;

public:
	/**
	 * Initialize a config but dot not do anything
	 */
	GlobalConfiguration();

	/**
	 * Load a configuration from a char*
	 *
	 * Note: This is used to broadcast the configuration through MPI.
	 *
	 * \param content Content of the configuration file
	 */
	void load(const std::string& content);

	/** 
	 * Read a configuration file and return the content as a string
	 *
	 * Note: This is used to broadcast the configuration from the master
	 * node to all the nodes with MPI.
	 *
	 * \param file path to the file
	 * \return the content of the file
	 */
	std::string read(const std::string& file); 

	/**
	 * Return the next set of parameters.
	 *
	 * \return the next configuration obtained from the configuration file.
	 */
	Config get_next_configuration();

	/**
	 * Indicates if there is another configuration possible.
	 *
	 * \return true if another configuration is possible.
	 */
	bool has_next();

	/**
	 * Return the number of configurations 
	 *
	 * \return total number of configurations
	 */
	int get_number_configurations();

	/**
	 * Restart the generation of the various configurations.
	 */
	void reset();
	
	/**
	 * \brief exception raised if the configuration file is not found.
	 */
	class FileNotFoundException: public std::exception {};
	
	
	/**
	 * \brief exception raised if the configuration file is not found.
	 */	
	class ConfigurationNotLoadedException: public std::exception {};
	
	/**
	 * \brief exception raised if the configuration file is not properly formatted.
	 *
	 * There should be no sequences except of Scalars
	 */
	class ConfigurationNotSupportedException: public std::exception {
		/**
		 * \return message describing the exception.
		 */
		virtual const char* what() const noexcept;	
	};
};

#endif
