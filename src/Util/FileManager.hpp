/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef FILEMANAGER_HPP
#define FILEMANAGER_HPP

#include <exception>
#include <string>
#include <fstream> 
#include <sstream> 
#include <memory>

class Config;

/**
 * \brief Manages output folder.
 *
 * Singleton managing the files and directories
 */
class FileManager {
private:
	/**
	 * Test if a path link to a directory
	 * 
	 * \param path path to test
	 * \return true if path is a directory
	 */
	bool is_directory(const char* path);
	
	/**
	 * Create a directory
	 *
	 * \param path path to the directory
	 */
	void create_directory(const char* path);

	/**
	 * Create a standardized name for the logs
	 *
	 * \param proc_name MPI_NAME of the processor
	 * \param rank rank of the process
	 * \return standardized log name
	 */
	std::string get_log_name(const std::string& proc_name, int rank);
	
	/// Main folder in which all results can be found
	std::string main_folder;
	/// Folder containing the logs of the current node and process
	std::string local_folder;
	/// Folder that will receive all logs
	std::string main_log_folder;
	/// Separator in the paths
	std::string dir_sep = "/";

	/// Local file receiving logs
	std::shared_ptr<std::ofstream> local_log_file;
	/// Boolean indicating if local_log_file is initialized
	bool local_log_file_init = false;

	/// For one run of the benchmark
	std::shared_ptr<std::ofstream> run_log_file;
	/// Boolean indicating if run_log_file is open
	bool is_run_log_open = false;

	/// Boolean indicating if current_dir is created
	bool current_dir_open = false;
	/// Directory in which the results of an experiment can be found
	std::string current_dir;

public:
	/// Single instance of FileManager 
	static std::shared_ptr<FileManager> p_instance;
	
	/**
	 * Return the singleton
	 *
	 * \return the single instance of FileManager
	 */
	static std::shared_ptr<FileManager> instance();

	/**
	 * Create the output folder for all results
	 *
	 * \param output_dir Name of the folder for all results.
	 */
	void create_output_dir(const std::string& output_dir); 

	/**
	 * Add a written log on the node
	 *
	 * \param log_level level of the log
	 */
	void add_local_log(int log_level);

	/**
	 * Close the local log.
	 */
	void close_local_log();
	
	/** 
	 * Add a log for one run.
	 * To be only used on the master node.
	 *
	 * \param log_level level of detail of the log
	 */
	void add_run_log(int log_level);

	/**
	 * Close the log for the run.
	 */
	void close_run_log();

	/**
	 * Retreive the logs from all slaves and write them on the master
	 *
	 * \param log_stream log of the master
	 */
	void gather_logs(std::ostringstream* log_stream);

	/**
	 * Create a directory for a run.
	 * To be used only on the master.
	 *
	 * \param name name of the directory.
	 */
	void open_current_dir(const std::string& name);

	/**
	 * Write a file in the current directory.
	 * open_current_dir must have been called before using this method.
	 * To be used only on the master.
	 *
	 * \param filename name of the file to write
	 * \param content content of the file
	 */
    void print_file(const std::string& filename, const std::string& content);
	
	/**
	 * Write a file in the output folder of the configuration.
	 * To be used only on the master.
	 *
	 * \param file_name name of the file to write
	 * \param content content of the file
	 */
	void print_global_file(const std::string& file_name, const std::string& content);

	/**
	 * \brief Raised when there is a problem with the output.
	 *
	 * Exception that is raised when there is a problem with the output of 
	 * Pufferbench
	 */
	class OutputFolderException: public std::exception{};
};

#endif
