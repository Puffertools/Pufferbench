/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef CONFIGURATIONBROADCASTER_HPP
#define CONFIGURATIONBROADCASTER_HPP

#include <memory>
#include <string>

class GlobalConfiguration;

/**
 * \brief Sends the configuration to each node
 *
 * Singleton with the role of broadcasting the configuration
 * file.
 */
class ConfigurationBroadcaster {
public:
	/// Singleton
	static std::shared_ptr<ConfigurationBroadcaster> p_instance;

	/**
	 * Static method to access the singleton
	 *
	 * \return The instance of the ConfigurationBroadcaster
	 */
	static std::shared_ptr<ConfigurationBroadcaster> instance();

	/**
	 * Broadcast a configuration to all nodes
	 *
	 * \param conf_file path of the configuration file
	 * \return Global configuration ready to be used.
	 */
	GlobalConfiguration bcast_config(const std::string& conf_file);

};

#endif
