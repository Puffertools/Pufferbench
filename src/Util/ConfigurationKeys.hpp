/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef CONFIGURATIONKEYS_HPP
#define CONFIGURATIONKEYS_HPP

#include <string>

/**
 * \brief Set of configuration keys and default values for Pufferbench.
 */
class ConfigurationKeys {
public:	
	/// Name of the common field
	static const char* COMMON_FIELD;
	/// Name of the option field
	static const char* OPTION_FIELD;

	// Static values of the configuration
	/// Name of the section with the configuration of the run.
	static const char* RUN_SECTION;
	/// Name of the section with the configuration of the components.
	static const char* COMPONENTS_SECTION;
	/// Name of the section with the configuration of the logs.
	static const char* LOG_SECTION;
	
	/// Name of the parameter indicating whether replays have to be run.
	static const char* RUN_REPLAYS_KEY;
	/// Name of the parameter giving the replication factor
	static const char* REPLICATION_KEY;
	/// Name of the parameter giving the seed of the random number generator.
	static const char* SEEDS_KEY;
	/// Name of the parameter giving the number of nodes initially deployed.
	static const char* INITIAL_NUMBER_KEY;
	/// Name of the parameter indicating the number of nodes to commission.
	static const char* DELTA_NODES_KEY;
	/// Name of the parameter indicating which MetadataGenerator to use.
	static const char* GENERATION_KEY;
	/// Name of the parameter indicating which DataDistributionGenerator to use.
	static const char* ALLOCATION_KEY; 
	/// Name of the parameter indicating which DataTransferScheduler to use.
	static const char* DATA_TRANSFER_KEY;
	/// Name of the parameter indicating which IODispatcher to use.
	static const char* IODISPATCHER_KEY;
	/// Name of the parameter indicating which Network Component to use.
	static const char* NETWORK_KEY;
	/// Name of the parameter indicating which Storage Components to use.
	static const char* STORAGE_KEY;
	/// Name of the parameter indicating the output directory
	static const char* OUTPUT_DIR_KEY;
	/// Name of the parameter for the log level of files
	static const char* LOG_FILES_KEY;
	/// Name of the parameter for the log level of the master cout
	static const char* LOG_MASTER_KEY;
	/// Name of the parameter for the log level of the slave cout
	static const char* LOG_SLAVES_KEY;
	/// Name of the parameter for the forced synchronization of logs
	static const char* LOG_FORCED_SYNC_KEY;
	/// Name of the parameter for the coloring of logs
	static const char* LOG_COLOR_KEY;


	/// Default value for RUN_REPLAYS_KEY
	static const bool RUN_REPLAYS_DEFAULT;
	/// Default value for REPLICATION_KEY 
	static const int REPLICATION_DEFAULT;
	/// Default value for SEEDS_KEY
	static const int SEEDS_DEFAULT;
	/// Default value for INITIAL_NUMBER_KEY
	static const int INITIAL_NUMBER_DEFAULT;
	/// Default value for COMMISSION_NODES_KEY
	static const int DELTA_NODES_DEFAULT;
	/// Default value for GENERATION_KEY
	static const std::string GENERATION_DEFAULT;
	/// Default value for ALLOCATION_KEY
	static const std::string ALLOCATION_DEFAULT;
	/// Default value for DATA_TRANSFER_KEY
	static const std::string DATA_TRANSFER_DEFAULT;
	/// Default value for IODISPATCHER_KEY 
	static const std::string IODISPATCHER_DEFAULT;
	/// Default value for STORAGE_KEY 
	static const std::string STORAGE_DEFAULT;
	/// Default value for NETWORK_KEY 
	static const std::string NETWORK_DEFAULT;
	/// Default value for OUTPUT_DIR_KEY
	static const std::string OUTPUT_DIR_DEFAULT;
	/// Default value for LOG_FILES
	static const std::string LOG_FILES_DEFAULT;
	/// Default value for LOG_MASTER
	static const std::string LOG_MASTER_DEFAULT;
	/// Default value for LOG_SLAVES
	static const std::string LOG_SLAVES_DEFAULT;
	/// Default value for LOG_FORCED_SYNC_KEY
	static const bool LOG_FORCED_SYNC_DEFAULT;
	/// Default value for LOG_COLOR_KEY
	static const bool LOG_COLOR_DEFAULT;
};

#endif
