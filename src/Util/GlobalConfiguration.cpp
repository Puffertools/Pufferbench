/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "GlobalConfiguration.hpp"

#include <fstream>
#include <assert.h>

#include "Config.hpp"
#include "Log.hpp"

using namespace std;
using namespace YAML;

GlobalConfiguration::GlobalConfiguration(){
    config_fields = make_shared<map<string,vector<string>>>();
}

void GlobalConfiguration::load(const string& l){
	try {
		config = YAML::Load(l.c_str());
		loaded = true;
	} catch (YAML::BadFile& e) {
		LogError("Incorrect configuration file.");	
		loaded = false;
	}
}

Config GlobalConfiguration::get_next_configuration(){
	if (!loaded)
		throw ConfigurationNotLoadedException();
	if (!initialized){
		next_positions = initialize(config);
		combinaisons = count_combinaisons(next_positions);
		LogConf("There are "+to_string(combinaisons)+" configuration(s).");
		config_fields->clear();
		LogConf("Configuration fields are:");
		initialize_output(config,vector<string>());		
		initialized = true;
		// Create header
		config_header = "exp_number";
		map<string,vector<string>>::iterator it;
		for (it = config_fields->begin(); it != config_fields->end(); ++it){
			config_header += ","+it->first;
		}
	}
	bool increase = true;	
	Node nd = get_next(next_positions,&increase,config);
	Config conf(nd,config_header,config_fields);
	// if increase is still true, we have the last configuration
	if (increase){
		last = true;
	}
	return conf;
}

int GlobalConfiguration::count_combinaisons(TreePosition& pos){
	if (pos.type == SCALAR || pos.type == NUL)
		return 1;
	if (pos.type == MAP){
		int mult = 1;
		map<string,TreePosition>::iterator it;
		for (it = pos.map_children.begin(); it != pos.map_children.end(); ++it){
			mult *= count_combinaisons(it->second);
		}
		return mult;
	} else {
		// if there is no children, count as 1 option
		if (pos.children.empty())
			return 1;
		int mult = 0;
		for (unsigned int i = 0; i < pos.children.size(); ++i){
			mult += count_combinaisons(pos.children[i]);
		}
		return mult;
	}
}

int GlobalConfiguration::get_number_configurations(){
	return combinaisons;
}

GlobalConfiguration::TreePosition GlobalConfiguration::initialize(YAML::Node& to_visit){
	if (!to_visit.IsDefined()){
		LogError("Undefined node");
		throw ConfigurationNotSupportedException();
	} else if (to_visit.IsMap()){
		YAML::iterator it;
		TreePosition current;
		current.type = MAP;
		for (it = to_visit.begin(); it != to_visit.end(); ++it){
			string key = it->first.as<string>(); 
			map<string,TreePosition>::iterator found;
			found = current.map_children.find(key);
			if (found != current.map_children.end()){
				LogError("The configuration file provided contains two keys intitled "+key);
				throw ConfigurationNotSupportedException();
			}
			current.map_children[it->first.as<string>()] = initialize(it->second);
		}
		assert(current.type == MAP);
		assert(to_visit.IsMap());
		return current;
	} else if (to_visit.IsScalar()){
		TreePosition tp;
		tp.type = SCALAR;
		return tp;
	} else if (to_visit.IsNull()){
		TreePosition tp;
		tp.type = NUL;
		return tp;
	} else if (to_visit.IsSequence()){
		if (to_visit.size()>0) {
			TreePosition tp;
			tp.current = 0;
			tp.type = SEQUENCE;
			tp.max = to_visit.size();
			// fill TreePosition
			for (unsigned int i = 0; i < to_visit.size(); ++i){
				Node r = to_visit[i];
				tp.children.push_back(initialize(r));
			}
			// configuration
			if (to_visit.size()>1)
				last = false;
			assert(to_visit.IsSequence());
			return tp;
		} else {
			TreePosition tp;
			tp.type = SEQUENCE;
			return tp;
		}
	}
	LogError("Untyped node");
	throw ConfigurationNotSupportedException();
}

void GlobalConfiguration::initialize_output(YAML::Node& to_visit, vector<string> path){
	if (to_visit.IsMap()){
		YAML::iterator it;
		for (it = to_visit.begin(); it != to_visit.end(); ++it){
			string key = it->first.as<string>();
			path.push_back(key);
			initialize_output(it->second, path);
			path.pop_back();
		}
	} else if (to_visit.IsScalar()){
		// Create field name and add to map
		if (path.size() > 0){
			string key = "";
			for (unsigned int i = 1; i < path.size(); ++i){
				key += path[i];
				if (i != path.size()-1)
					key += "-";
			}
			if ((*config_fields)[key].size() == 0){
				(*config_fields)[key] = path;
				LogConf("| - "+key);
			}
		}
	} else if (to_visit.IsSequence()){
		for (unsigned int i = 0; i < to_visit.size(); ++i){
			Node r = to_visit[i];
			initialize_output(r, path);
		}
	}
}


YAML::Node GlobalConfiguration::get_next(TreePosition& pos,bool* increase, YAML::Node& to_visit){
	assert(initialized);
	if (!to_visit.IsDefined()){
		throw ConfigurationNotSupportedException();
	} else if (to_visit.IsMap()){
		YAML::iterator it;
		YAML::Node r(NodeType::Map);
		assert(pos.type == MAP);
		for (it = to_visit.begin(); it != to_visit.end(); ++it){
			string key = it->first.as<string>();
			TreePosition& next = pos.map_children[key];
			r[key] = get_next(next, increase, it->second);
		}
		assert(pos.type == MAP);
		assert(r.size() == to_visit.size());
		assert(r.IsMap());
		return r;
	} else if (to_visit.IsNull()){
		assert(pos.type == NUL);
		YAML::Node r(NodeType::Null);
		return r;
	} else if (to_visit.IsScalar()){
		assert(pos.type == SCALAR);
		YAML::Node r(NodeType::Scalar);
		r = to_visit.as<string>();
		return r;
	} else if (to_visit.IsSequence()){
		if (to_visit.size()>0){
			assert(pos.type == SEQUENCE);

			assert(pos.current < pos.max);
			Node r = to_visit[pos.current];
			Node p = get_next(pos.children[pos.current], increase, r);

			// Update position for the next config
			if (*increase){
				// all combinaisons in the child have been used
				// Going to next child
				if (pos.current == pos.max - 1){
					pos.current = 0;
				} else {
					pos.current++;
					*increase = false;
				}				
			}
			// Return the current configuration
			return p;
		} else {
			YAML::Node r(NodeType::Null);
			return r;
		}
	}
	throw ConfigurationNotSupportedException();
	return YAML::Node(Null);
}

bool GlobalConfiguration::has_next(){
	return !last;
}

string GlobalConfiguration::read(const string& file){
	ifstream myfile (file.c_str());
	if (myfile.is_open()){
		myfile.seekg (0, myfile.end);
		int length = myfile.tellg();
		myfile.seekg (0, myfile.beg);

		vector<char> buffer(length);
		myfile.read(buffer.data(),length);
		myfile.close();
		
		string content(buffer.data());
		content.resize(length);
		return content;
	}
	throw FileNotFoundException();
}

void GlobalConfiguration::reset(){
	last = false;
	next_positions = TreePosition();
	initialized=false;
}

const char* GlobalConfiguration::ConfigurationNotSupportedException::what() const noexcept {
	return "ConfigurationNotSupportedException: Problem with the configuration file.";
}
