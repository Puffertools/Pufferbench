/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef __LOG_H
#define __LOG_H

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <memory> 

#include "Defs.hpp"


#define ERROR 2
#define WARN 4
#define IMPORTANT 8
#define INFO 16
#define TRACE 32
#define TRANSFER 64
#define METADATAGENERATOR 128
#define DISTRIBUTIONGENERATOR 256
#define DISPATCHER 512
#define STORAGE 1024
#define NETWORK 2048
#define CONFIG 4096

/**
 * \brief Log system for Pufferbench
 *
 * The configuration include the informations log for 
 * the master (standard output), the slaves (standard output),
 * and in the files.
 *
 * By default, the file logging is not synchronized but the option
 * can be set in the configuration.
 *
 * Possible configurations are: \n
 * TRACE INFO IMPORTANT WARN ERROR for the different levels of logging \n
 * TRANSFER METADATAGENERATOR DISTRIBUTIONGENERATOR REPLAY STORAGE NETWORK CONFIG for the details 
 * of each components
 *
 * \verbatim
logs:
  master: TRACE INFO IMPORTANT WARN ERROR
  slaves: TRACE INFO IMPORTANT WARN ERROR
  files: TRACE INFO IMPORTANT WARN ERROR
  forced_sync: false
\endverbatim
 */
class Log{
private:
	/// \brief Simple struct used to record information about a log output
	struct LogOutput {
		/// Ofstream of a log output
		std::ostream* out;
		/// Mask associated to the log
		int mask;
		/// Sync
		bool sync;
		/// initialized
		int initialized;
	};

	/// vector of logs
	std::vector<std::shared_ptr<LogOutput>> logs;
	
	/// pid of the process
	std::string tag;
	
	/// \brief Log message
	struct LogMessage {
		/// Level of the log
		int level;
		/// Message to log
		std::string msg;
		/// File from which it has been emitted
		std::string file;
		/// Line from which it has been emitted 
		int line;
		/// Time of the log
		std::string time;	
	};

	/// vector of messages
	std::vector<LogMessage> messages;

	/// force the sync of the log
	bool force_sync = false;

	/// is it initialized
	bool initialized = false;

	/// Whether to use color on the console output
	bool use_color = false;

	/**
	 * Print a message in the logs
	 *
	 * \param out Ostream of the output
	 * \param level Level of the message
	 * \param msg Message to log
	 * \param file File logging messages
	 * \param line Line logging messages
	 * \param time Time at with the log was received
	 */
	void print_message(std::ostream* out, int level, const std::string& msg, 
		const std::string& file, int line, const std::string& time);

	/// mask with all the recorder levels
	int relevant_masks = 0;

	/// tags for levels in console
	std::map<int,std::string> tag_level_console;

	/// tags for levels in file
	std::map<int,std::string> tag_level;

public:
	/// Single instance of Log
	static std::shared_ptr<Log> p_instance;

	/**
	 * Return the singleton
	 *
	 * \return the single instance of Log
	 */
	static std::shared_ptr<Log> instance();

	/**
	 * Change a number in a size (B, kB, MB, GB)
	 *
	 * \param size Number to convert.
	 * \return A string representing the size.
	 */
	static std::string to_str(ObjectSize size);

	/**
	 * Constructor
	 */
	Log();

	/**
	 * Add a log output
	 *
	 * \param out Output ostream
	 * \param mask Mask that filters the informations to log
	 * \param sync Whether the output should be synchronized
	 */
	void addLog(std::ostream* out, int mask, bool sync);
	
	/**
	 * Remove a specific log
	 *
	 * \param out log to remove
	 */
	void removeLog(std::ostream* out);
	
	/**
	 * log a message
	 *
	 * \param level level of the message
	 * \param msg message to add in the log
	 * \param file file from which the log was written
	 * \param line line of the code that called the macro to log infos
	 */
	void log(int level, const std::string& msg, const std::string& file, int line);
	
	/**
	 * Intialize the logs, stop recording all messages by default.
	 *
	 * \param color True to use colored output in the console
	 */
	void initialize(bool color = true);

	/**
	 * Flush all the messages onto the non synchronized outputs
	 */
	void flush();

	/**
	 * Force the synchronization of all outputs.
	 *
	 * \param sync Whether all outputs should be synchronized
	 */
	void set_force_sync(bool sync);

	/**
	 * Close all log streams
	 */
	void closeLogs();

	/**
	 * Change a string into a Loglevel
	 *
	 * \param str String to change into a log level
	 * \return a log level
	 */
	int to_loglevel(const std::string& str);
};

// Macros to log with various levels
#define LogTrace(msg) Log::instance()->log(TRACE, msg, __FILE__, __LINE__)
#define LogInfo(msg) Log::instance()->log(INFO, msg, __FILE__, __LINE__)
#define LogImportant(msg) Log::instance()->log(IMPORTANT, msg, __FILE__, __LINE__)
#define LogWarn(msg) Log::instance()->log(WARN, msg, __FILE__, __LINE__)
#define LogError(msg) Log::instance()->log(ERROR, msg, __FILE__, __LINE__)

#define LogNetwork(msg) Log::instance()->log(NETWORK, msg, __FILE__, __LINE__)
#define LogStorage(msg) Log::instance()->log(STORAGE, msg, __FILE__, __LINE__)
#define LogDispatcher(msg) Log::instance()->log(DISPATCHER, msg, __FILE__, __LINE__)
#define LogDataTransfer(msg) Log::instance()->log(TRANSFER, msg, __FILE__, __LINE__)
#define LogDistribution(msg) Log::instance()->log(DISTRIBUTIONGENERATOR, msg, __FILE__, __LINE__)
#define LogGenerator(msg) Log::instance()->log(METADATAGENERATOR, msg, __FILE__, __LINE__)
#define LogConf(msg) Log::instance()->log(CONFIG, msg, __FILE__, __LINE__)

#endif
