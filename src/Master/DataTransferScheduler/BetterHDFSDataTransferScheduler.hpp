/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef BETTERHDFSDATATRANSFERSCHEDULER_HPP
#define BETTERHDFSDATATRANSFERSCHEDULER_HPP

#include "AbstractDataTransferScheduler.hpp" 

class Config;

/**
 * \brief DataTransferScheduler designed to be better than the one of HDFS
 *
 * Implementation of algorithms better for the HDFS model.
 *
 * Commission:\n
 * The object are allocated randomly.
 * The transfers are balanced according to the amount of data
 * received first, then by the amount of data sent by node.
 *
 * Decommission:\n
 * The object are allocated randomly.
 * The transfers are balanced by reception in the case of a network
 * bottleneck, and by storage activity in the case of a storage bottleneck.
 * Then further balancing is done with the reads and send operations.
 *
 * The configuration define whether the storage is the bottleneck:
 * \verbatim
BetterHDFSDataTransferScheduler:
  storage_bottleneck: true
   \endverbatim
 */
class BetterHDFSDataTransferScheduler: public AbstractDataTransferScheduler {
private:

	/// Name of the section of the configuration for this scheduler
	const char* BETTERHDFS_SECTION = "BetterHDFSDataTransferScheduler";
	/// Key indicating if the storage is the bottlenck
	const char* STORAGE_BOTTLENECK_KEY = "storage_bottleneck";
	/// Default value for STORAGE_BOTTLENECK_KEY
	const bool STORAGE_BOTTLENECK_DEFAULT = false;

public:

	/**
	 * Constructor
	 */
	BetterHDFSDataTransferScheduler(Config& conf);

	virtual void schedule_commission();

	virtual void schedule_decommission();
};

#endif
