/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "AbstractDataTransferScheduler.hpp"

#include <assert.h>

#include "Log.hpp"

#include "Order.hpp"
#include "OrderRecorder.hpp"

#include "Node.hpp"
#include "Object.hpp"
#include "DataDistribution.hpp" 

#include "DataSet.hpp"
#include "ClusterInfo.hpp"

using namespace std;

AbstractDataTransferScheduler::AbstractDataTransferScheduler(Config& conf):
								AbstractComponent(conf){}

void AbstractDataTransferScheduler::set_data_distribution(const shared_ptr<DataDistribution>& _data_distribution){
	data_distribution = _data_distribution;	
}

void AbstractDataTransferScheduler::copy(const shared_ptr<Object>& obj, const shared_ptr<Node>& from, 
										const shared_ptr<Node>& to, int priority){
	assert(data_distribution);
	assert(obj);
	assert(from);
	assert(to);

	data_distribution->set(obj,to);
	// sender orders
	Order sender_orders;// = order_recorder->get_new_order_for_node(from->get_nodeID());
	sender_orders.order_id = order_counter;
	sender_orders.type = READ_SEND;
	sender_orders.priority = priority;
	sender_orders.src_nodeID = from->get_nodeID();
	sender_orders.dst_nodeID = to->get_nodeID();
	sender_orders.tag = tag_counter;
	sender_orders.object_id = obj->get_objectID();
	sender_orders.size = obj->get_size();
	order_counter++;
	order_recorder->add_order(from->get_nodeID(),sender_orders);

	// receiver orders
	Order recv_orders;// = order_recorder->get_new_order_for_node(to->get_nodeID());
	recv_orders.order_id = order_counter;
	recv_orders.type = RECV_WRITE;
	recv_orders.priority = priority;
	recv_orders.src_nodeID = from->get_nodeID();
	recv_orders.dst_nodeID = to->get_nodeID();
	recv_orders.tag = tag_counter;
	recv_orders.object_id = obj->get_objectID();
	recv_orders.size = obj->get_size();
	order_recorder->add_order(to->get_nodeID(),recv_orders);
	tag_counter++;
	order_counter++;
}

void AbstractDataTransferScheduler::forward(const shared_ptr<Object>& obj, const shared_ptr<Node>& from, 
											const shared_ptr<Node>& to, int priority){
	assert(data_distribution);
	assert(obj);
	assert(from);
	assert(to);

	data_distribution->set(obj,to);
	// sender orders
	Order sender_orders;// = order_recorder->get_new_order_for_node(from->get_nodeID());
	sender_orders.order_id = order_counter;
	sender_orders.type = RECV_SEND;
	sender_orders.priority = priority;
	sender_orders.src_nodeID = from->get_nodeID();
	sender_orders.dst_nodeID = to->get_nodeID();
	sender_orders.tag = tag_counter;
	sender_orders.object_id = obj->get_objectID();
	sender_orders.size = obj->get_size();
	order_recorder->add_order(from->get_nodeID(),sender_orders);
	order_counter++;
	
	// receiver orders
	Order recv_orders;// = order_recorder->get_new_order_for_node(to->get_nodeID());
	recv_orders.order_id = order_counter;
	recv_orders.type = RECV_WRITE;
	recv_orders.priority = priority;
	recv_orders.src_nodeID = from->get_nodeID();
	recv_orders.dst_nodeID = to->get_nodeID();
	recv_orders.tag = tag_counter;
	recv_orders.object_id = obj->get_objectID();
	recv_orders.size = obj->get_size();
	order_recorder->add_order(to->get_nodeID(),recv_orders);
	tag_counter++;
	order_counter++;
}

void AbstractDataTransferScheduler::add_wait(){
	Order wait;
	wait.order_id = order_counter;
	wait.type = WAIT;
	wait.tag = -1;
	wait.priority = -1;
	wait.object_id = -1;
	wait.src_nodeID = -1;
	wait.dst_nodeID = -1;
	wait.size = -1;
	order_recorder->add_order_for_all(wait);
	order_counter++;
}

void AbstractDataTransferScheduler::decommission_nodes(unsigned int number){
	LogTrace("Selecting "+to_string(number)+" nodes to decommission.");
	vector<shared_ptr<Node>> nodes = data_distribution->get_cluster_info().get_nodes(); 
	assert(number <= nodes.size());
	
	vector<int> tmp;
	for (unsigned int i = 0; i < nodes.size(); ++i){
		tmp.push_back(i);
	}
	for (unsigned int i = 0; i < number; i++){
		int index = rand()%tmp.size();
		int pos = tmp[index];
		LogTrace(" | - Node with ID "+to_string(nodes[pos]->get_nodeID())+" is decommissioned.");
		data_distribution->get_cluster_info().decommission_node(nodes[pos]);
		tmp.erase(tmp.begin()+index);
	}
}

void AbstractDataTransferScheduler::commission_nodes(unsigned int number){
	LogTrace("Commissionning "+to_string(number)+" nodes.");
	data_distribution->get_cluster_info().commission(number);
}

void AbstractDataTransferScheduler::remove(const shared_ptr<Object>& obj, const shared_ptr<Node>& from){
	data_distribution->remove(obj,from);
}

const vector<shared_ptr<Object>>& AbstractDataTransferScheduler::get_objects_on_node(const shared_ptr<Node>& node){
	return data_distribution->get_objects_on_node(node);
}

const vector<shared_ptr<Node>>& AbstractDataTransferScheduler::get_nodes_hosting_object(const shared_ptr<Object>& obj){
	return data_distribution->get_nodes_hosting_object(obj);
}

ClusterInfo& AbstractDataTransferScheduler::get_cluster_info(){
	return data_distribution->get_cluster_info();
}

DataSet& AbstractDataTransferScheduler::get_dataset(){
	return data_distribution->get_dataset();
}

bool AbstractDataTransferScheduler::host(const shared_ptr<Node>& node, const shared_ptr<Object>& obj){
	return data_distribution->host(node,obj);
}

ObjectSize AbstractDataTransferScheduler::get_amount_data_hosted(const shared_ptr<Node>& node){
	return data_distribution->get_amount_data_hosted(node);
}

int AbstractDataTransferScheduler::get_number_of_tags(){
	return tag_counter;
}

void AbstractDataTransferScheduler::set_order_recorder(const shared_ptr<OrderRecorder>& order_rec){
	order_recorder = order_rec;
}

std::shared_ptr<Object> AbstractDataTransferScheduler::divide_object(const std::shared_ptr<Object>& obj, 
																	ObjectSize new_size){
	shared_ptr<Object> new_obj = data_distribution->get_dataset().divide_object(obj, new_size);
	vector<shared_ptr<Node>> nodes = data_distribution->get_nodes_hosting_object(obj);
	for (shared_ptr<Node> node : nodes){
		data_distribution->set(new_obj,node);
	}
		
	int new_orders = order_recorder->duplicate_and_update_orders_for_object(obj->get_objectID(),
						new_size, new_obj->get_objectID(), new_obj->get_size(), order_counter,
						tag_counter);
	tag_counter +=new_orders;
	order_counter += new_orders;

	return new_obj;
}

