/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "BetterHDFSDataTransferScheduler.hpp"

#include <assert.h>
#include <algorithm>

#include "Log.hpp"
#include "Config.hpp"
#include "GlobalConfiguration.hpp"
#include "ConfigurationKeys.hpp"

#include "Order.hpp"
#include "Node.hpp"
#include "Object.hpp"
#include "DataDistribution.hpp" 

#include "DataSet.hpp"
#include "ClusterInfo.hpp"

using namespace std;

REGISTER_DATATRANSFERSCHEDULER(BetterHDFSDataTransferScheduler)

BetterHDFSDataTransferScheduler::BetterHDFSDataTransferScheduler(Config& conf):
	AbstractDataTransferScheduler(conf){}

void BetterHDFSDataTransferScheduler::schedule_commission(){
	LogInfo("BetterHDFSScheduler: Commission");

	// Assign Data randomly 
	// 	- Get average data to host
	// 	- Cluster into overloaded and underloaded
	//	- assign randomly to new nodes

	// Compute the ideal load per node
	ObjectSize total = get_dataset().get_total_size();
	int r = conf.get_parameter<int>({ConfigurationKeys::REPLICATION_KEY},
										ConfigurationKeys::REPLICATION_DEFAULT);
	total *= r;
	int nb_nodes = get_cluster_info().get_size();
	ObjectSize avg = total/nb_nodes;

	LogDataTransfer(" - Each node should host "+to_string(avg)+" of data.");

	int max_tries = 2 * r;

	// Vector of all nodes
	vector<shared_ptr<Node>> vnodes = get_cluster_info().get_nodes();
	vector<shared_ptr<Node>>::const_iterator it;

	vector<shared_ptr<Node>> overloaded;
	vector<shared_ptr<Node>> underloaded;

	vector<ObjectSize> load(vnodes.size());

	for (unsigned int i = 0; i < vnodes.size(); ++i){
		ObjectSize ld = get_amount_data_hosted(vnodes[i]);
		if (ld>avg){
			LogDataTransfer("Node "+to_string(vnodes[i]->get_nodeID())+" (pos: "+to_string(i)+") is overloaded.");
			overloaded.push_back(vnodes[i]);
		} else {
			LogDataTransfer("Node "+to_string(vnodes[i]->get_nodeID())+" (pos: "+to_string(i)+") is underloaded.");
			underloaded.push_back(vnodes[i]);
		}
		load[i] = ld;
	}

	LogDataTransfer(" - Overloaded nodes: "+to_string(overloaded.size()));
	LogDataTransfer(" - Underloaded nodes: "+to_string(underloaded.size()));

	map<shared_ptr<Object>, vector<shared_ptr<Node>>> objects_to_move;
	map<shared_ptr<Object>, vector<shared_ptr<Node>>> objects_to_remove;
	int count_new_replicas = 0;
	int count_removed_replicas = 0;
	// For all overloaded nodes
	vector<shared_ptr<Node>>::iterator nit;
	for (nit = overloaded.begin(); nit != overloaded.end(); ++nit){
		LogDataTransfer(" - Emptying "+to_string((*nit)->get_nodeID()));
		// copy the objects they host 

		
		vector<shared_ptr<Object>> hosted = get_objects_on_node(*nit);

		bool done = false;	

		while (!done){ // still overloaded
			// randomly pick objects
			// remove from the list
			
			int pos = rand()%hosted.size();
			shared_ptr<Object> obj = hosted[pos];

			if (load[(*nit)->get_nodeID()]-obj->get_size() < avg){
				done = true;
				LogDataTransfer(" - - Scheduled to host "+to_string(load[(*nit)->get_nodeID()]));
				break;
			}

			bool moved = false;

			shared_ptr<Node> new_host = nullptr;

			int tries = 0;

			while (!moved && tries < max_tries){
			
				tries++;

				// pick an underloaded node 
				int npos = rand()%underloaded.size();
				new_host = underloaded[npos];

				// check if data is on it
				if (host(new_host,obj)){
					//LogDataTransfer(" - - - Already hosted");
					continue;
				}
				
				// Check is the node is already scheduled to receive the data
				vector<shared_ptr<Node>>::iterator already_hosting;
				already_hosting = find(objects_to_move[obj].begin(),
								objects_to_move[obj].end(),
								new_host);
				if (already_hosting != objects_to_move[obj].end()){
					//LogDataTransfer(" - - - Already scheduled");
					continue;
				}

				// if not add it
				objects_to_move[obj].push_back(new_host);
				// Update the data hosted per node
				load[new_host->get_nodeID()] += obj->get_size();
				load[(*nit)->get_nodeID()] -= obj->get_size();
				// update stats
				count_new_replicas++;
				// if the node is not underloaded anymore remove it
				if (load[new_host->get_nodeID()]>avg){
					underloaded.erase(underloaded.begin()+npos);
				}
				
				// Recording the removal
				objects_to_remove[obj].push_back(*nit);
				count_removed_replicas++;
				moved = true;
			}
			
			hosted.erase(hosted.begin()+pos);
		}
	}

	LogInfo(" |- "+to_string(objects_to_move.size())+" objects are being moved.");
	LogDataTransfer(" | |- "+to_string(count_new_replicas)+" new replicas are created.");	
	LogDataTransfer(" |- "+to_string(objects_to_remove.size())+" objects are being removed.");
	LogDataTransfer(" | |- "+to_string(count_removed_replicas)+" replicas are removed.");	


	// Schedule transfers
	// Unified for both Network and Storage bottleneck
	//	* - Force forwarding (Tnew->new < Trecv)	
	//	1 - Balance recv / writes 
	//	2 - Balance sends

	// Priorities
	int priority_step = get_dataset().get_size();
	int order_counter = 0;

	// Vectors for send load for all nodes
	vector<ObjectSize> send_load(vnodes.size()); 

	map<shared_ptr<Object>, vector<shared_ptr<Node>>>::iterator omit;
	for (omit = objects_to_move.begin(); omit != objects_to_move.end(); ++omit){
		shared_ptr<Object> obj = omit->first;
		vector<shared_ptr<Node>> dests = omit->second;
		
		// Select src 
	
		LogDataTransfer(" - Transfer of object "+to_string(obj->get_objectID()));

		vector<shared_ptr<Node>> srcs = get_nodes_hosting_object(obj);
		assert(!srcs.empty());

		shared_ptr<Node> src;
		ObjectSize min_load = 0;
		vector<shared_ptr<Node>>::const_iterator nit;
		for (nit = srcs.begin(); nit != srcs.end(); ++nit){
			if (!src || min_load > send_load[(*nit)->get_nodeID()]){
				src = *nit;
				min_load = send_load[(*nit)->get_nodeID()];
			}
		}
		assert(src);

		LogDataTransfer(" - - From node "+to_string(src->get_nodeID()));

		// Select last

		shared_ptr<Node> last;
		ObjectSize max_load = 0;
		vector<shared_ptr<Node>>::iterator pos;
		vector<shared_ptr<Node>>::iterator nit2;
		for (nit2 = dests.begin(); nit2 != dests.end(); ++nit2){
			if (!last || max_load < send_load[(*nit2)->get_nodeID()]){
				pos = nit2;
				max_load = send_load[(*nit2)->get_nodeID()];
				last = *nit2;
			}
		}
		assert(last);
		dests.erase(pos);
		dests.push_back(last);

		int priority = dests.size()*priority_step+order_counter;

		shared_ptr<Node> previous = src;
		for (unsigned int i = 0; i < dests.size(); ++i){
			LogDataTransfer(" - - to node "+to_string(dests[i]->get_nodeID()));
			
			// Update load
			send_load[previous->get_nodeID()]+=obj->get_size();
			
			if (i == 0){
				// Read_send 
				copy(obj,previous,dests[i],priority);
			} else {
				// Recv_send
				forward(obj,previous,dests[i],priority);
			}
			priority -= priority_step;
			previous = dests[i];
		}
		order_counter++;
	}
	

	// For all objects to move
	// 	- Need a src from old nodes (least send loaded)
	// 	- Select end of forwarding (most send loaded)

	LogDataTransfer(" - Removing data.");


	// Remove the extra copies
	for (omit = objects_to_remove.begin(); omit != objects_to_remove.end(); ++omit){
		vector<shared_ptr<Node>>::iterator nit;
		for (nit = omit->second.begin(); nit != omit->second.end(); ++ nit){
			LogDataTransfer(" - - Removing object "+to_string(omit->first->get_objectID())+" from node "+to_string((*nit)->get_nodeID()));
			
			remove(omit->first,*nit);
		}
	}
}

void BetterHDFSDataTransferScheduler::schedule_decommission(){
	LogInfo("BetterHDFSScheduler: Decommission");

	bool storage_bottleneck = conf.get_parameter<bool>({BETTERHDFS_SECTION,
								STORAGE_BOTTLENECK_KEY},STORAGE_BOTTLENECK_DEFAULT);
	if (storage_bottleneck){
		LogInfo(" |- Assuming a storage bottleneck");
	} else {
		LogInfo(" |- Assuming a network bottleneck");
	}

	// Assign Data randomly 
	// 	- Get average data to host
	// 	- Cluster into overloaded and underloaded
	//	- assign randomly to new nodes

	// Compute the ideal load per node
	ObjectSize total = get_dataset().get_total_size();
	int r = conf.get_parameter<int>({ConfigurationKeys::REPLICATION_KEY},
										ConfigurationKeys::REPLICATION_DEFAULT);
	total *= r;
	int nb_nodes = get_cluster_info().get_size()-get_cluster_info().number_of_decommissioned_nodes();
	ObjectSize avg = total/nb_nodes;

	LogDataTransfer(" - Each node should host "+to_string(avg)+" of data.");
	LogDataTransfer(" - "+to_string(get_cluster_info().number_of_decommissioned_nodes())+" decommissioned nodes.");
	LogDataTransfer(" - "+to_string(get_cluster_info().get_size())+" nodes.");

	int max_tries = 2 * r;

	// Vector of all nodes
	const vector<shared_ptr<Node>> vnodes = get_cluster_info().get_nodes();
	vector<shared_ptr<Node>>::const_iterator it;

	vector<shared_ptr<Node>> overloaded;
	vector<shared_ptr<Node>> underloaded;

	vector<shared_ptr<Node>> in_decommission;

	vector<ObjectSize> load(vnodes.size());

	for (unsigned int i = 0; i < vnodes.size(); ++i){
		ObjectSize ld = get_amount_data_hosted(vnodes[i]);
		if (vnodes[i]->is_in_decommission()){
			LogDataTransfer("Node "+to_string(vnodes[i]->get_nodeID())+" (pos: "+to_string(i)+") is in-decommission.");
			in_decommission.push_back(vnodes[i]);
			continue;
		}
		if (ld>avg){
			LogDataTransfer("Node "+to_string(vnodes[i]->get_nodeID())+" (pos: "+to_string(i)+") is overloaded.");
			overloaded.push_back(vnodes[i]);
		} else {
			LogDataTransfer("Node "+to_string(vnodes[i]->get_nodeID())+" (pos: "+to_string(i)+") is underloaded.");
			underloaded.push_back(vnodes[i]);
		}
		load[i] = ld;
	}

	LogDataTransfer(" - In-decommission nodes: "+to_string(in_decommission.size()));
	LogDataTransfer(" - Overloaded nodes: "+to_string(overloaded.size()));
	LogDataTransfer(" - Underloaded nodes: "+to_string(underloaded.size()));


	// Object -> targets
	map<shared_ptr<Object>, vector<shared_ptr<Node>>> objects_to_move;

	int count_new_replicas = 0;
	int count_removed_replicas = 0;
	
	// For all overloaded nodes
	vector<shared_ptr<Node>>::iterator nit;
	for (nit = in_decommission.begin(); nit != in_decommission.end(); ++nit){
		LogDataTransfer(" - Emptying "+to_string((*nit)->get_nodeID()));
		// copy the objects they host 
		vector<shared_ptr<Object>> hosted = get_objects_on_node(*nit);
		count_removed_replicas += hosted.size();
		
		LogDataTransfer(" - - "+to_string(hosted.size())+" objects to move.");

		for (unsigned int pos = 0; pos < hosted.size(); ++pos){
			// Not yet empty
			shared_ptr<Object> obj = hosted[pos];

			bool moved = false;
			shared_ptr<Node> new_host;
			
			int tries = 0;
			
			while (!moved){
			
				tries++;
				
				bool from_underloaded = true;
				// pick an underloaded node 
				int npos; 
				if (underloaded.empty() || (!overloaded.empty() && tries > max_tries)){
					npos = rand()%overloaded.size();
					new_host = overloaded[npos];
					from_underloaded = false;
				} else {
					npos = rand()%underloaded.size();
					new_host = underloaded[npos];
				}

				// check if data is on it
				if (host(new_host,obj)){
					continue;
				}
				
				// Check is the node is already scheduled to receive the data
				vector<shared_ptr<Node>>::iterator already_hosting;
				already_hosting = find(objects_to_move[obj].begin(),
								objects_to_move[obj].end(),
								new_host);
				if (already_hosting != objects_to_move[obj].end()){
					continue;
				}

				// if not add it
				objects_to_move[obj].push_back(new_host);
				// Update the data hosted for the new node
				load[new_host->get_nodeID()] += obj->get_size();
				
				// update stats
				count_new_replicas++;
				// if the node is not underloaded anymore remove it
				if (load[new_host->get_nodeID()]>avg && from_underloaded){
					underloaded.erase(underloaded.begin()+npos);
					overloaded.push_back(new_host);
				}
				
				// Recording the removal
				moved = true;
			}
		}
	}

	LogInfo(" |- "+to_string(objects_to_move.size())+" objects are being moved.");
	LogDataTransfer(" | |- "+to_string(count_new_replicas)+" new replicas are created.");	
	LogDataTransfer(" |- "+to_string(count_removed_replicas)+" old replicas are removed.");	

	// Schedule transfers
	// network bottleneck
	//	1 - balance recv
	//	2 - limit/balance sends
	// storage bottlenck
	//	2 - balance writes
	//	3 - limit reads 
	// 	4 - balance sends

	// Need to differenciate between storage and network bottlenecks
	// - Storage bottleneck : balance reads + writes between nodes
	
	// First move data out of the in-decommission nodes
	// and assign it to the other nodes while balancing their total amount of data.

	// Initialize load vectors

	// Second: Assign transfers
	// For each data transfer
	// 1 - choose which node reads with the read load vector
	// 2 - Choose which node does not forward (max send_load)
	

	// Priorities
	int priority_step = get_dataset().get_size();
	int order_counter = 0;


	// Load vector	
	vector<ObjectSize> read_load(vnodes.size()); 
	// if storage bottleneck add writes already scheduled

	LogDataTransfer("Initializing read_load");

	map<shared_ptr<Object>, vector<shared_ptr<Node>>>::iterator omit;
	if (storage_bottleneck){
		for (omit = objects_to_move.begin(); omit != objects_to_move.end(); ++omit){
			shared_ptr<Object> obj = omit->first;
			vector<shared_ptr<Node>> dests = omit->second;
			vector<shared_ptr<Node>>::iterator nit;
			for (nit = dests.begin(); nit != dests.end(); ++nit){
				read_load[(*nit)->get_nodeID()]+=obj->get_size();
			}
		}	
	}
	
	vector<ObjectSize> send_load(vnodes.size()); 

	// Move data
	for (omit = objects_to_move.begin(); omit != objects_to_move.end(); ++omit){

		shared_ptr<Object> obj = omit->first;
		vector<shared_ptr<Node>> dests = omit->second;
		
		// Select src in order to read from the storage only once 
		LogDataTransfer(" - Transfer of object "+to_string(obj->get_objectID()));

		vector<shared_ptr<Node>> hosts = get_nodes_hosting_object(obj);
		assert(!hosts.empty());

		shared_ptr<Node> src;
		ObjectSize min_load = 0;
		vector<shared_ptr<Node>>::const_iterator nit;
		for (nit = hosts.begin(); nit != hosts.end();++nit){
			if (storage_bottleneck){
				if (!src || min_load > read_load[(*nit)->get_nodeID()]){
					src = *nit;
					min_load = read_load[(*nit)->get_nodeID()];
				}
			} else {
				if (!src || min_load > send_load[(*nit)->get_nodeID()]){
					src = *nit;
					min_load = send_load[(*nit)->get_nodeID()];
				}
			}
		}
		assert(src);

		vector<shared_ptr<Node>> srcs;
		srcs.push_back(src);
		
		int priority = dests.size()*priority_step+order_counter;

		if (storage_bottleneck){
			read_load[src->get_nodeID()] += obj->get_size();
		}

		// Aside from the first sender all only forwards to ensure
		// that the least amount of data is read from storage
		while (!dests.empty()){
			// Sender
			shared_ptr<Node> from;
			ObjectSize min_load = 0;
			vector<shared_ptr<Node>>::iterator nit2;
			for (nit2 = srcs.begin(); nit2 != srcs.end(); ++nit2){
				if (!from || min_load > send_load[(*nit2)->get_nodeID()]){
					min_load = send_load[(*nit2)->get_nodeID()];
					from = *nit2;
				}
			}
			assert(from);

			// Target
			shared_ptr<Node> to;
			min_load = 0;
			vector<shared_ptr<Node>>::iterator pos;
			for (nit2 = dests.begin(); nit2 != dests.end(); ++nit2){
				if (!to || min_load > send_load[(*nit2)->get_nodeID()]){
					pos = nit2;
					min_load = send_load[(*nit2)->get_nodeID()];
					to = *nit2;
				}
			}
			assert(to);
			dests.erase(pos);
			srcs.push_back(to);
			
			LogDataTransfer(" - - from node "+to_string(from->get_nodeID())+" to node "+to_string(to->get_nodeID()));
			LogDataTransfer(" - - - Priority: "+to_string(priority));
			// update send_load
			send_load[from->get_nodeID()] += obj->get_size();
			
			if ( srcs.size() == 2){
				// If it is the first transfer, it has to be read
				copy(obj,from,to,priority);
			} else {
				forward(obj,from,to,priority);
			}

			priority -= priority_step;
		}
		order_counter++;
	}
	
	LogDataTransfer(" - Removing data.");

	// Remove the extra copies
	for (nit = in_decommission.begin(); nit != in_decommission.end(); ++nit){
		LogDataTransfer(" - - Removing objects from node "+to_string((*nit)->get_nodeID()));
		vector<shared_ptr<Object>> obj_to_remove = get_objects_on_node(*nit);
		vector<shared_ptr<Object>>::const_iterator oit;
		for (oit = obj_to_remove.begin(); oit != obj_to_remove.end();++oit){
			remove(*oit,*nit);	
		}
	}
}
