/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef ABSTRACTDATATRANSFERSCHEDULER_HPP
#define ABSTRACTDATATRANSFERSCHEDULER_HPP

#include <vector>
#include <memory>

#include "AbstractComponent.hpp"

#include "Defs.hpp"

struct Order;
class Node;
class Object;
class DataDistribution;
class ClusterInfo;
class DataSet;
class Config;
class OrderRecorder;

/** 
 * \brief AbstractDataTransferScheduler used to implement a DataTransferScheduler
 *
 * Abstract class describing how a DataTransferScheduler should be implemented.
 */
class AbstractDataTransferScheduler: public AbstractComponent {
private:
	/// DataDistribution
	std::shared_ptr<DataDistribution> data_distribution;

	/// Counter of the number of orders given
	OrderID order_counter = 0;

	/// Counter of the number of tags given
	int tag_counter = 0;

	/// Records all orders given for the data transfers
	std::shared_ptr<OrderRecorder> order_recorder;

public:
	/**
	 * Constructor
	 *
	 * \param conf Configuration of the run
	 */
	AbstractDataTransferScheduler(Config& conf);
	
	/**
	 * Destructor
	 */
	virtual ~AbstractDataTransferScheduler(){};

	/**
	 * Set the initial DataDistribution.
	 *
	 * \param data_distribution Initial DataDistribution
	 */
	void set_data_distribution(const std::shared_ptr<DataDistribution>& data_distribution);

	/**
	 * Method to implement to realize a commission.
	 *
	 * Use copy, remove, forward to move objects.
	 * All the protected method give informations about the situation.
	 */
	virtual void schedule_commission() = 0;
	
	/**
	 * Method to implement to realize a decommission.
	 *
	 * Use copy, remove, forward to move objects.
	 * All the protected method give informations about the situation.
	 */
	virtual void schedule_decommission() = 0;

	/**
	 * Method that can be reimplemented to choose specific nodes
	 * to decommission.
	 * By defauly, nodes are randomly chosen.
	 *
	 * \param number Number of nodes to decommission. 
	 */
	virtual void decommission_nodes(unsigned int number);
	
	/**
	 * Method that can be reimplemented to choose specific nodes
	 * to commission.
	 * By defauly, nodes are simply added.
	 *
	 * \param number Number of nodes to commission. 
	 */
	virtual void commission_nodes(unsigned int number);

	/**
	 * Return the number of tags used for the orders.
	 *
	 * \return the number of tags that have been given to order.
	 */
	int get_number_of_tags();

	/**
	 * Initialize the component in charge of recording all
	 * orders to execute on the cluster.
	 *
	 * \param order_recorder OrderRecorder to use.
	 */
	void set_order_recorder(const std::shared_ptr<OrderRecorder>& order_recorder);

protected:
	/**
	 * Copy an object from a source to a destination.
	 * The operations are Read, send and recv write.
	 *
	 * \param obj Object to move
	 * \param from Node that sends the object.
	 * \param to Node that receives the data.
	 * \param priority Priority of the order.
	 */
	void copy(const std::shared_ptr<Object>& obj,
            const std::shared_ptr<Node>& from, 
			const std::shared_ptr<Node>& to, int priority);
	
	/**
	 * Remove an object from a Node.
	 *
	 * \param obj Object to remove
	 * \param from Node that hosts the object.
	 */
	void remove(const std::shared_ptr<Object>& obj,
            const std::shared_ptr<Node>& from);

	/**
	 * Forward an object from a source to a destination.
	 * The operations are Recv, send and recv write.
	 *
	 * Indicates that the Node sending the data will receive 
	 * the object before (due to another forward or copy) and then forward it.
	 *
	 * \param obj Object to move
	 * \param from Node that sends the object.
	 * \param to Node that receives the data.
	 * \param priority Priority of the order.
	 */
	void forward(const std::shared_ptr<Object>& obj,
            const std::shared_ptr<Node>& from, 
			const std::shared_ptr<Node>& to, int priority);

	/**
	 * Add a wait operation for all nodes.
	 * All nodes will wait for all other orders to finish before
	 * continuing.
	 */
	void add_wait();

	/**
	 * Returns the objects placed on a node.
	 *
	 * \param node Node
	 * \return vector of objects present on the node.
	 */
    const std::vector<std::shared_ptr<Object>>& get_objects_on_node(const std::shared_ptr<Node>& node);
	
	/**
	 * Returns the list of nodes hosting an object
	 *
	 * \param obj Object
	 * \return vector of the nodes hosting said object.
	 */
    const std::vector<std::shared_ptr<Node>>& get_nodes_hosting_object(const std::shared_ptr<Object>& obj);

	/** 
	 * Returns the informations about the cluster.
	 *
	 * \return Informations about the cluster.
	 */
	ClusterInfo& get_cluster_info();
	
	/**
	 * Returns the DataSet distributed on the nodes.
	 *
	 * \return DataSet distributed on the nodes
	 */
	DataSet& get_dataset();
	
	/**
	 * Indicates if a node hosts some object.
	 *
	 * \param node Node to consider
	 * \param obj Object to consided
	 * \return true if the node host the object, false if not.
	 */
	bool host(const std::shared_ptr<Node>& node, const std::shared_ptr<Object>& obj);
	
	/**
	 * Returns the amount of data hosted on a specific node.
	 *
	 * \param node Node.
	 * \return amount of data stored on the node.
	 */
	ObjectSize get_amount_data_hosted(const std::shared_ptr<Node>& node);

	/**
	 * Divide an object in two parts, the first part keeps the objectID
	 * of the original but get resized to the new size.
	 * The second object is the remaing part of the first.
	 *
	 * Maintain the consistency with emitted orders (each order existing 
	 * before for the original object is duplicated for the second object)
	 * and for the data distribution (the nodes hosting the orginal object 
	 * now host both parts)
	 *
	 * \param obj Object to divide.
	 * \param new_size new size of the object.
	 * \return The second part of the object.
	 */
	std::shared_ptr<Object> divide_object(const std::shared_ptr<Object>& obj, ObjectSize new_size);
};

#define REGISTER_DATATRANSFERSCHEDULER(comp) REGISTER_COMPONENT(DataTransferScheduler, comp)

#endif
