/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "Master.hpp"

#include <mpi.h>
#include <assert.h>
#include <memory>

#include "Log.hpp"
#include "Stats.hpp"

#include "AbstractMetadataGenerator.hpp"
#include "AbstractDataDistributionGenerator.hpp"

#include "DataDistributionValidator.hpp" 

#include "FileManager.hpp"

#include "Replay.hpp"
#include "ComponentRegister.hpp"
#include "Printer.hpp"

#include "AbstractDataTransferScheduler.hpp"

#include "Config.hpp"
#include "GlobalConfiguration.hpp"
#include "ConfigurationKeys.hpp"

#include "Constants.hpp"
#include "Order.hpp"
#include "OrderRecorder.hpp"

#include "DataSet.hpp"
#include "DataDistribution.hpp"
#include "ClusterInfo.hpp"

#include "Defs.hpp"
#include "Node.hpp"

using namespace std;

Master::~Master(){
}

void Master::master_metadata_management(Config& conf, Constants& constants, const shared_ptr<OrderRecorder>& order_recorder){
	LogTrace("Starting the simulation of the data.");

	shared_ptr<ComponentRegister> reg = ComponentRegister::instance();

	shared_ptr<AbstractMetadataGenerator> metadata_generator = reg->get_metadata_generator(conf);
	metadata_generator->init();
	
	shared_ptr<AbstractDataDistributionGenerator> data_distribution_generator = reg->get_data_distribution_generator(conf); 
	data_distribution_generator->init();
	
	shared_ptr<AbstractDataTransferScheduler> data_transfer_scheduler = reg->get_data_transfer_scheduler(conf);
	data_transfer_scheduler->set_order_recorder(order_recorder);
	data_transfer_scheduler->init();

	LogImportant(" ## Generating and allocating data.");

	LogTrace("Generate DataSet");
	// Generate a new data set
	shared_ptr<DataSet> dataset = metadata_generator->generate_data();
	constants.total_number_objects = dataset->get_size();	

	Printer printer;
	printer.log_dataset("dataset_generated.csv",dataset);

	// Create the informations about the cluster
	int nb_initial = conf.get_parameter<int>({ConfigurationKeys::RUN_SECTION,
												ConfigurationKeys::INITIAL_NUMBER_KEY},
												ConfigurationKeys::INITIAL_NUMBER_DEFAULT);
	ClusterInfo cluster_info(nb_initial);

	LogTrace("Allocate data");
	// Allocate data to nodes
	data_distribution_generator->create(cluster_info,*dataset);
	data_distribution_generator->allocate();
	
	shared_ptr<DataDistribution> data_distribution = data_distribution_generator->get_data_distribution();	

	//printer.log_cluster_info("cluster_info_allocated.csv",cluster_info);
	printer.log_dataset("dataset_allocated.csv",dataset);
	printer.log_data_per_node("data_per_node_allocated.csv",data_distribution);

	// Evaluate the data distribution before any transfer of data
	DataDistributionValidator data_distribution_validator(*data_distribution);
	data_distribution_validator.evaluate("evaluation_before.txt");	

	data_transfer_scheduler->set_data_distribution(data_distribution);

	// Read parameters of commission and decommission
	int nb_commission = conf.get_parameter<int>({ConfigurationKeys::RUN_SECTION,
												ConfigurationKeys::DELTA_NODES_KEY},
												ConfigurationKeys::DELTA_NODES_DEFAULT);	
	LogImportant(" ## Scheduling data transfers.");
	if (nb_commission > 0){
		LogInfo("Scheduling the commission of "+to_string(nb_commission)+" node(s).");
		data_transfer_scheduler->commission_nodes(nb_commission);
		data_transfer_scheduler->schedule_commission();
	} else if (nb_commission < 0){
		int nb_decommission = -nb_commission;
		LogInfo("Scheduling the decommission of "+to_string(nb_decommission)+" node(s).");
		data_transfer_scheduler->decommission_nodes(nb_decommission);
		data_transfer_scheduler->schedule_decommission();
	}

	constants.number_of_tags = data_transfer_scheduler->get_number_of_tags();

	printer.log_cluster_info("cluster_info_final.csv",cluster_info);
	printer.log_dataset("dataset_final.csv",dataset);
	printer.log_data_per_node("data_per_node_final.csv",data_distribution);

	// Report on the distribution
	data_distribution_validator.evaluate("evaluation_after.txt");

	LogTrace("Simulation of the data finished.");
	Log::instance()->flush();
}

double Master::run(Config& conf, bool run_replays){
	LogTrace("This node is the master.");
	
	int wsize = 1;
	MPI_Comm_size(MPI_COMM_WORLD,&wsize);	
	
	// Maximum number of nodes
	int nb_delta = conf.get_parameter<int>({ConfigurationKeys::RUN_SECTION,
												ConfigurationKeys::DELTA_NODES_KEY},
												ConfigurationKeys::DELTA_NODES_DEFAULT);
	int nb_initial = conf.get_parameter<int>({ConfigurationKeys::RUN_SECTION,
												ConfigurationKeys::INITIAL_NUMBER_KEY},
												ConfigurationKeys::INITIAL_NUMBER_DEFAULT);
	int max_nodes = nb_initial;
	if (nb_delta > 0){
		max_nodes += nb_delta;
	}	

	// Generate orders
	Constants constants;

	// Order recorder to store orders
	int nb_orders_stacks = wsize;
	if (wsize < max_nodes)
		nb_orders_stacks = max_nodes;
	shared_ptr<OrderRecorder> orders_to_replay(new OrderRecorder(nb_orders_stacks)); 

	// Schedule all work
	master_metadata_management(conf,constants,orders_to_replay);	

	if (!run_replays){
		LogTrace("Skipping replays as configured.");	
		return 0;
	}	

	LogImportant(" ## Starting the replay.");
	
	assert(wsize>=max_nodes);

	// Assign orders to ranks
	orders_to_replay->assign_ranks();
	Printer printer;
	printer.log_order_recorder("rank_to_nodeID.csv",orders_to_replay,wsize);
	MPI_Barrier(MPI_COMM_WORLD);

	int number_orders = 0;

	for (int i = 1; i < wsize; ++i){
		// Get orders for rank i
		vector<Order>& orders = orders_to_replay->get_orders_for_rank(i);
		// update constants
		constants.total_number_orders = orders.size();
		number_orders += orders.size();

		LogTrace("Sending "+to_string(orders.size())+" order(s) to rank "+to_string(i));

		int count = sizeof(Constants);
		MPI_Send(&constants, count, MPI_BYTE, i, 0, MPI_COMM_WORLD);

		count = orders.size()*sizeof(Order);
		MPI_Send(orders.data(), count, MPI_BYTE, i, 0, MPI_COMM_WORLD);
	}

	// Get own orders
	vector<Order>& orders = orders_to_replay->get_orders_for_rank(0);
	constants.total_number_orders = orders.size();
	number_orders += orders.size();

	// Replay orders
	Replay replay;
	Stats local_stats = replay.run(conf,constants,orders);
	double duration = local_stats.global_time;	
	
	LogImportant("The replay lasted for "+to_string(duration)+"s.");

	LogTrace("There are "+to_string(number_orders)+" to retreive");	
	// Gather Orders back with timings
	vector<Order> results;
	results.resize(number_orders);
	int obtained = 0;
	
	// To store stats
	vector<Stats> stats;
	stats.resize(wsize);
	
	memcpy(results.data(),orders.data(),orders.size()*sizeof(Order));	
	stats[0] = local_stats;
	stats[0].rank = 0;
	stats[0].node = orders_to_replay->nodeID_of_rank(0);
	obtained += orders.size();
	
	int stat_index = 1;

	for (int i = 1; i < wsize; ++i){
		// Number of orders dispatched to that rank
		int to_retreive = orders_to_replay->get_orders_for_rank(i).size();
	
		Order* buf_pos = results.data()+obtained;

		LogTrace("Retreiving "+to_string(to_retreive)+" from rank "+to_string(i));
		
		int count = to_retreive*sizeof(Order);	
		MPI_Recv(buf_pos, count, MPI_BYTE, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		
		MPI_Recv(&stats[stat_index], sizeof(Stats), MPI_BYTE, i, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		
		// Add info to stats
		stats[stat_index].rank = i;
		stats[stat_index].node = orders_to_replay->nodeID_of_rank(i);
		
		obtained += to_retreive;
		stat_index++;
	}
	assert(obtained == number_orders);

	LogTrace(to_string(obtained)+" orders were retreived from the replay.");

	printer.log_orders(results);
	printer.log_stats(stats);
	
	LogTrace("Master routine finished.");
	Log::instance()->flush();

	results.clear();
	stats.clear();

	return duration;
}



