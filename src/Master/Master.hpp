/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef MASTER_HPP
#define MASTER_HPP

#include <memory>

class Config;
class OrderRecorder;
struct Constants;

/** 
 * \brief Class overseeing the behaviour of the master node.
 *
 * Manage the master (generate data, create an initial configuration,
 * commission or decommission nodes, plan data transfers. 
 */
class Master {
private:

	/**
	 * Brain of the master.
	 * Manage the various components to generate a dataset,
	 * Distribute the data among the nodes, select nodes to commission and decommission,
	 * and plan the transfers of data.
	 * 
	 * \param conf Configuration
	 * \param constants Constants needed for the replay
	 * \param order_recorder OrderRecorder used to record the orders 
	 */
	void master_metadata_management(Config& conf, Constants& constants, 
					const std::shared_ptr<OrderRecorder>& order_recorder);

public:
	~Master();

	/**
	 * Manager the master for a whole run.
	 * Generating data transfers and then replay them.
	 *
	 * \param conf Configuration
	 * \param run_replays Whether the replays are launched or not.
	 * \return the duration of the run. 
	 */
	double run(Config& conf, bool run_replays);
};

#endif
