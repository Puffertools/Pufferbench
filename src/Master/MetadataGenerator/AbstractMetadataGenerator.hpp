/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef ABSTRACTMETADATAGENERATOR_HPP
#define ABSTRACTMETADATAGENERATOR_HPP

#include <string>
#include <memory>

#include "AbstractComponent.hpp"
#include "ComponentFactory.hpp"

class DataSet;
class Config;

/**
 * \brief Abstract class of the MetadataGenerator component
 * 
 * Class used to generate the data set to distribute
 * on the nodes.
 */
class AbstractMetadataGenerator : public AbstractComponent{
public:
	/**
	 * Constructor
	 *
	 * \param conf Configuration of the run
	 */
	AbstractMetadataGenerator(Config& conf);

	/**
	 * Destructor
	 */
	virtual ~AbstractMetadataGenerator(){};

	/**
	 * Main function to generate data
	 * \return A DataSet containing the newly generated data
	 */
	virtual std::unique_ptr<DataSet> generate_data() = 0;
};

#define REGISTER_METADATAGENERATOR(comp) REGISTER_COMPONENT(MetadataGenerator, comp)

#endif
