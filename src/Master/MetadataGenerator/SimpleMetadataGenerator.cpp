/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "SimpleMetadataGenerator.hpp"

#include <string>

#include "Defs.hpp"
#include "Config.hpp"
#include "GlobalConfiguration.hpp"
#include "ConfigurationKeys.hpp"

#include "DataSet.hpp"
#include "Object.hpp"

using namespace std;

//REGISTER_METADATAGENERATOR(SimpleMetadataGenerator)

SimpleMetadataGenerator::SimpleMetadataGenerator(Config& conf):
	AbstractMetadataGenerator(conf){}

unique_ptr<DataSet> SimpleMetadataGenerator::generate_data(){
	
	LogInfo("SimpleMetadataGenerator: Start");

	int nb_nodes = conf.get_parameter<int>({ConfigurationKeys::RUN_SECTION,
												ConfigurationKeys::INITIAL_NUMBER_KEY},
												ConfigurationKeys::INITIAL_NUMBER_DEFAULT);
	int r = conf.get_parameter<int>({ConfigurationKeys::REPLICATION_KEY},
										ConfigurationKeys::REPLICATION_DEFAULT);

	ObjectSize per_node = conf.get_size_parameter({"SimpleMetadataGenerator","per_node"},10*1024);
	ObjectSize fsize = conf.get_size_parameter({"SimpleMetadataGenerator","size"},128);

	unique_ptr<DataSet> to_fill(new DataSet());

	ObjectSize total_data = per_node*nb_nodes/r;

	ObjectSize generated = 0;
	while (generated < total_data){
		long nsize = fsize;
		if (generated + nsize > total_data){
			nsize = total_data-generated;
		}
		to_fill->add_object(nsize);
		generated += nsize;
	}
	
	LogInfo("SimpleMetadataGenerator: Finished");
	return to_fill;
}
