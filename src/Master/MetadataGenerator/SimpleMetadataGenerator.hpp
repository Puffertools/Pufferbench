/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef SIMPLEMETADATAGENERATOR_HPP
#define SIMPLEMETADATAGENERATOR_HPP

#include "AbstractMetadataGenerator.hpp"
#include <memory>

class Config;

/**
 * \brief Simple MetadataGenerator that generates objects of fixed size
 *
 * Create a fixed number of objects with the same size.
 */
class SimpleMetadataGenerator: public AbstractMetadataGenerator {
public:
	/**
	 * Constructor
	 *
	 * \param conf Configuration of the run
	 */
	SimpleMetadataGenerator(Config& conf);

	/**
	 * Generate enough objects to fill each node with some amount
	 * of data.
	 * Values of the parameters can be set in the configuration file.
	 */
	virtual std::unique_ptr<DataSet> generate_data();
};

#include <string>

using namespace std;

REGISTER_METADATAGENERATOR(SimpleMetadataGenerator)

#endif
