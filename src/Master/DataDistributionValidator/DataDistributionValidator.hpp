/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef DATADISTRIBUTIONVALIDATOR_HPP
#define DATADISTRIBUTIONVALIDATOR_HPP

#include <string>
#include <map>
#include <vector>

#include "Defs.hpp"

class DataSet;
class DataDistribution;

/**
 * \brief Class that evaluates the quality of a data distribution
 *
 * This class looks at the load balancing, the data replication,
 * and the uniformity of a data distribution.
 */
class DataDistributionValidator {
private:
	/// DataDistribution to evaluate
	DataDistribution& data_distribution;

	/**
	 * Evaluation of the load balancing
	 *
	 * \return the results of the evaluation in the csv format
	 */
	std::string EvaluateLoadBalancing();
	
	/**
	 * Evaluation of the data replication
	 *
	 * \return the results of the evaluation in the csv format
	 */
	std::string EvaluateDataReplication();
	
	/**
	 * Evaluation of the uniformity of the data replication
	 *
	 * \return the results of the evaluation in the csv format
	 */
	std::string EvaluateUniformity();

	/**
	 * Function that populates a map with keys obtained from sets 
	 * of r nodes.
	 * Recursively generates the keys.
	 *
	 * \param map Map of keys of sets of r nodes
	 * \param nodes Vector of nodes that are not being decommissioned
	 * \param pos Whitch of the r nodes is currently added to the key
	 * \param r Number of nodes in each set
	 * \param node Current node selected
	 * \param nb_nodes Total number of nodes in the cluster
	 * \param acc Key being built
	 *
	 * \return number of generated sets
	 */
	int initialize_sets(std::map<long,ObjectSize>& map, 
							const std::vector<NodeID>& nodes, 
							int pos, 
							int r, 
							int node,
							int nb_nodes,
							long acc);

public:
	/**
	 * Constructor.
	 *
	 * \param dd DataDistribution to evaluate
	 */
	DataDistributionValidator(DataDistribution& dd);	

	/**
	 * Run the evaluation of the data distribution
	 *
	 * \param filename Name of the file in which the results are written.
	 */
	void evaluate(const std::string& filename);
};

#endif
