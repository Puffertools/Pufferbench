/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "DataDistributionValidator.hpp"

#include <memory>
#include <assert.h>
#include <sstream>
#include <math.h>
#include <algorithm>

#include "FileManager.hpp"
#include "Log.hpp"

#include "DataDistribution.hpp"
#include "DataSet.hpp"

#include "ClusterInfo.hpp"
#include "Node.hpp"
#include "Object.hpp"

using namespace std;

string DataDistributionValidator::EvaluateLoadBalancing(){
	
	LogInfo("|- Evaluating the load balancing");
	
	bool init = false;
	ObjectSize min_size = 0;
	ObjectSize max_size = 0;

	ObjectSize total = 0;
	ObjectSize sum_sq = 0;

	int active_nodes = 0;
	double MB = 1000000;

	vector<shared_ptr<Node>> nodes = data_distribution.get_cluster_info().get_nodes();
	vector<shared_ptr<Node>>::iterator it;

	// for each node in the cluster we compute the amount of data stored on it
	for (it = nodes.begin(); it != nodes.end(); ++it){
		// Ignore in decommission nodes
		if ((*it)->is_in_decommission())
			continue;
		active_nodes++;	
		
		ObjectSize current = 0;
			
		// Look if node host data
		current = data_distribution.get_amount_data_hosted(*it);

		total += current;
		sum_sq += (double)(current)/MB*((double)(current)/MB);

		if (!init){
			min_size = current;
			max_size = current;
			init = true;
		} 
		if (current < min_size)
			min_size = current;
		if (current > max_size)
			max_size = current;
	}
	double avg = (double)(total) / active_nodes;
	sum_sq /= active_nodes;
	double std_dev = sum_sq-(avg/MB)*(avg/MB);
	if (std_dev < 0)
		std_dev = 0;
	std_dev = sqrt(std_dev)*MB;
	
	LogInfo("| |- The cluster host "+Log::to_str(total)+".");   
	LogInfo("| |- The average load is "+Log::to_str(avg)+".");
	LogInfo("| |- The std. dev. is approx. "+Log::to_str(std_dev)+".");
	LogInfo("| |- The least loaded node hosts "+Log::to_str(min_size)+"."); 
	LogInfo("| |- The most loaded node hosts "+Log::to_str(max_size)+"."); 
	
	string str = to_string(total)+","+to_string(avg)+",";
	str += to_string(std_dev)+","+to_string(min_size)+",";
	str += to_string(max_size);

	return str;
}

string DataDistributionValidator::EvaluateDataReplication(){
	LogInfo("|- Checking the data replication");
	int r = data_distribution.get_replication_factor();
	// min observed replication factor
	int min_r = r;
	// max observed replication factor
	int max_r = r;

	int over_replicated = 0;
	int under_replicated = 0;
	int correctly_replicated = 0;
	int total = 0;
	int missing = 0;
	
	int total_replicas = 0;

	const vector<shared_ptr<Object>> objects = data_distribution.get_dataset().get_objects();
	vector<shared_ptr<Object>>::const_iterator it;

	// for each node in the cluster we compute the amount of data stored on it
	for (it = objects.cbegin(); it != objects.end(); ++it){
		int s = data_distribution.get_nodes_hosting_object(*it).size();
		// Compute the total amount of replicas hosted
		total_replicas += s;
		// Check the replication
		if (s < min_r)
			min_r = s;
		if (s > max_r)
			max_r = s;
		if (s > r)
			over_replicated++;
		if (s < r && s > 0)
			under_replicated++;
		if (s == 0)
			missing++;
		total++;
	}
	correctly_replicated = total - over_replicated - under_replicated - missing;

	// r
	int replicas_hosted = 0;
	vector<shared_ptr<Node>> nodes = data_distribution.get_cluster_info().get_nodes();

	for (shared_ptr<Node> node : nodes){
		replicas_hosted += data_distribution.get_objects_on_node(node).size();
	}

	// Log 
	if (min_r == max_r && replicas_hosted == total_replicas){
		LogInfo("| |- Data replication is correct.");
		LogTrace("| |- Replication factor checked"); 
		LogTrace("| |- All objects have "+to_string(r)+" replicas."); 
	} else if (min_r == max_r) {
		LogInfo("| |- All objects have "+to_string(r)+" replicas.");
		int diff = replicas_hosted - total_replicas;
		if (diff > 0){
			// Should never happen
			assert(false);
			LogInfo("| |- "+to_string(diff)+" replicas are missing."); 
		} else {
			LogInfo("| |- There are "+to_string(-diff)+" replicas too many."); 
		}
	} else {
		int diff = replicas_hosted - total_replicas;
		if (diff > 0){
			LogInfo("| |- "+to_string(diff)+" replicas are missing."); 
		} else {
			LogInfo("| |- There are "+to_string(-diff)+" replicas too many."); 
		}
		LogInfo("| |- The number of replicas per object varies between "+to_string(min_r)+
					" and "+to_string(max_r)+".");	
	}

	// expected rep factor, min, max
	// total objects, correctly replicated, under replicated, over replicated, 
	// missing, total replicas to host, total replicas hosted
	string str = to_string(r)+","+to_string(min_r)+","+to_string(max_r)+",";
	str += to_string(total)+","+to_string(correctly_replicated)+",";
	str += to_string(under_replicated)+","+to_string(over_replicated)+",";
	str += to_string(missing)+","+to_string(total_replicas)+",";
	str += to_string(replicas_hosted);
	return str;
}

static bool sort_function(shared_ptr<Node> n1, shared_ptr<Node> n2){
	return n1->get_nodeID() < n2->get_nodeID();
}

int DataDistributionValidator::initialize_sets(
			map<long,ObjectSize>& map, 
			const vector<NodeID>& nodes, 
			int pos, 
			int r,
			int node,
			int nb_nodes,
			long acc){
	if (pos == r){
		map[acc] = 0;
		return 1;
	}
	int sum = 0;
	for (unsigned int i = node+1; i < nodes.size(); ++i){
		sum += initialize_sets(map, nodes, pos+1, r, i, nb_nodes, acc*nb_nodes + nodes[i]);
	}
	return sum;
}

string DataDistributionValidator::EvaluateUniformity(){
	LogInfo("|- Checking the uniformity");
	int r = data_distribution.get_replication_factor();
	
	int nb_nodes = data_distribution.get_cluster_info().get_size();

	vector<shared_ptr<Object>> objects = data_distribution.get_dataset().get_objects();
	
	vector<shared_ptr<Node>> nodes = data_distribution.get_cluster_info().get_nodes();
	vector<NodeID> nodes_to_consider;

	for (const shared_ptr<Node> node : nodes){
		if (!node->is_in_decommission()){
			nodes_to_consider.push_back(node->get_nodeID());
		}
	}

	map<long,ObjectSize> hosted_per_set;
	
	unsigned int nb_sets = initialize_sets(hosted_per_set, nodes_to_consider, 0, r, -1, nb_nodes, 0);
	assert(hosted_per_set.size() == nb_sets);

	bool data_present_on_decommissioned_nodes = false;

	for (shared_ptr<Object> obj : objects){
		vector<shared_ptr<Node>> hosts = data_distribution.get_nodes_hosting_object(obj);
		sort(hosts.begin(),hosts.end(),sort_function);
	
		long key = 0;
		for (const shared_ptr<Node>& node : hosts){
			key = key*nb_nodes+node->get_nodeID();
		}
		if (hosted_per_set.count(key) ==  0){
			data_present_on_decommissioned_nodes = true;
		} else {
			hosted_per_set[key] += obj->get_size();
		}
	}
	assert(hosted_per_set.size() == nb_sets);

	bool init = false;
	double MB = 1000000;
	double sum_sq = 0;
	ObjectSize min = 0;
	ObjectSize max = 0;
	ObjectSize total = 0;

	map<long,ObjectSize>::iterator it;
	for (it = hosted_per_set.begin(); it != hosted_per_set.end(); ++it){
		ObjectSize cur = it->second;
		if (!init){
			min = cur;
			max= cur;
			init = true;
		}
		if (cur > max)
			max = cur;
		if (cur < min)
			min = cur;
		total += cur;
		sum_sq += ((double)(cur)/MB)*((double)(cur)/MB);
	}
	
	double avg = (double)(total) / nb_sets;
	sum_sq /= nb_sets;
	double std_dev = sum_sq-(avg/MB)*(avg/MB);
	if (std_dev < 0)
		std_dev = 0;
	std_dev = sqrt(std_dev)*MB;
	
	LogInfo("| |- The sets of r="+to_string(r)+" nodes");
	LogInfo("| | |- On avg. host "+Log::to_str(avg)+" of unique data.");
	LogInfo("| | |- The std. dev. is approx. "+Log::to_str(std_dev)+".");
	LogInfo("| | |- The least loaded set hosts "+Log::to_str(min)+"."); 
	LogInfo("| | |- The most loaded set hosts "+Log::to_str(max)+"."); 

	if (data_present_on_decommissioned_nodes){
		LogWarn("| | |- Warning: There are data on decommissioned nodes."); 
	}

	string str = to_string(avg)+",";
	str += to_string(std_dev)+","+to_string(min)+",";
	str += to_string(max)+","+((data_present_on_decommissioned_nodes)?"true,":"false,");

	return str;
}


DataDistributionValidator::DataDistributionValidator(DataDistribution& dd):
	data_distribution(dd){
}	

void DataDistributionValidator::evaluate(const string& filename){
	LogInfo("Evaluating the data distribution.");

	ostringstream stream;

	stream << "replication_factor,min_replication_factor_observed,max_replication_factor_observed,";
	stream << "total_nb_objects,objects_under_replicated,objects_over_replicated,";
	stream << "missing_objects,number_replicas_expected,number_replicas_hosted,,";
	stream << "total_data_hosted,average_load_per_node,std_dev_load_per_node,";
	stream << "min_load_per_node,max_load_per_node,,";
	stream << "average_unique_load_per_set,std_dev_unique_load_per_set,";
	stream << "min_unique_load_per_set,max_unique_load_per_set,data_on_decom_nodes"<< endl;
	stream << EvaluateDataReplication() + ",,";
	stream << EvaluateLoadBalancing() + ",," ;
	stream << EvaluateUniformity() << endl;

	FileManager::instance()->print_file(filename,stream.str());
}


