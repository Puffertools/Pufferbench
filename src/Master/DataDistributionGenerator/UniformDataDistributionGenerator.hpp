/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef UNIFORMDATADISTRIBUTIONGENERATOR_HPP
#define UNIFORMDATADISTRIBUTIONGENERATOR_HPP

#include "AbstractDataDistributionGenerator.hpp" 

/**
 * \brief DataDistributionGenerator that uniformly place data on the nodes.
 *
 * Implementation of AbstractDataDistributionGenerator that distributes 
 * data uniformly on the nodes.
 */
class UniformDataDistributionGenerator : public AbstractDataDistributionGenerator {
private:
	/**
	 * Return the number of possible combinations of k element among n.
	 * 
	 * \param k Number of elements
	 * \param n Total number of elements
	 * \return number of different combinaisons
	 */
	int k_among_n(int k,int n);

public:
	/**
	 * Constructor
	 *
	 * \param conf Configuration of the run
	 */
	UniformDataDistributionGenerator(Config& conf);

	/**
	 * Allocates data uniformly among the nodes in the cluster.
	 * Ensure the load balancing (almost).
	 * Ensure the replication factor.
	 * Ensure that no node has two replicas of the same object.
	 * Ensure that the amount of data unique to each set of r nodes is the same.
	 */
	virtual void allocate();
};

#endif
