/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "AbstractDataDistributionGenerator.hpp"

#include "Log.hpp"
#include "Node.hpp"
#include "Object.hpp"
#include "DataSet.hpp"
#include "ClusterInfo.hpp"
#include "DataDistribution.hpp"
#include "Config.hpp"
#include "GlobalConfiguration.hpp"

#include "ConfigurationKeys.hpp"

using namespace std;

AbstractDataDistributionGenerator::AbstractDataDistributionGenerator(Config& conf):
	AbstractComponent(conf){}

AbstractDataDistributionGenerator::~AbstractDataDistributionGenerator(){}

void AbstractDataDistributionGenerator::create(ClusterInfo& cluster_info, DataSet& dataset){
	int replication = conf.get_parameter<int>({ConfigurationKeys::REPLICATION_KEY},
											ConfigurationKeys::REPLICATION_DEFAULT);
	data_distribution = make_shared<DataDistribution>(cluster_info,dataset,replication);
}

shared_ptr<DataDistribution> AbstractDataDistributionGenerator::get_data_distribution(){
	return data_distribution;
}

void AbstractDataDistributionGenerator::set_data(
        const shared_ptr<Object>& obj, const shared_ptr<Node>& node){
	data_distribution->set(obj,node);
}

void AbstractDataDistributionGenerator::remove_data(
        const shared_ptr<Object>& obj,
        const shared_ptr<Node>& node){
	data_distribution->remove(obj,node);
}

DataSet& AbstractDataDistributionGenerator::get_dataset(){
	return data_distribution->get_dataset();
}

ClusterInfo& AbstractDataDistributionGenerator::get_cluster_info(){
	return data_distribution->get_cluster_info();
}

int AbstractDataDistributionGenerator::get_replication_factor(){
	return data_distribution->get_replication_factor();
}

bool AbstractDataDistributionGenerator::host(
        const shared_ptr<Node>& node,
        const shared_ptr<Object>& obj){
	return data_distribution->host(node,obj);
}

shared_ptr<Object> AbstractDataDistributionGenerator::divide_object_keep_alloc(
        const shared_ptr<Object>& obj, 
        ObjectSize new_size){
	shared_ptr<Object> new_obj = data_distribution->get_dataset().divide_object(obj, new_size);
	vector<shared_ptr<Node>> nodes = data_distribution->get_nodes_hosting_object(obj);
	for (const shared_ptr<Node>& node : nodes){
		data_distribution->set(new_obj,node);
	}
	return new_obj;
}

