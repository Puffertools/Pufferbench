/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef ABSTRACTDATADISTRIBUTIONGENERATOR_HPP
#define ABSTRACTDATADISTRIBUTIONGENERATOR_HPP

#include <string>
#include <memory>

#include "AbstractComponent.hpp"

class Node;
class Object;
class DataSet;
class ClusterInfo;
class DataDistribution;

/**
 * \brief Component in charge of the initial data allocation on the nodes.
 *
 * Abstract class encompassing the component that must distribute data on the
 * nodes before the commission or decommission.
 */
class AbstractDataDistributionGenerator : public AbstractComponent{
private:
	/// Generated DataDistribution
	std::shared_ptr<DataDistribution> data_distribution;

protected:
	/**
	 * Set the specified object on the specified node.
	 *
	 * \param obj Object to place.
	 * \param node Node that will receive the data.
	 */
	void set_data(const std::shared_ptr<Object>& obj,
            const std::shared_ptr<Node>& node);
	
	/**
	 * Remove an object from a node.
	 *
	 * \param obj Object to remove.
	 * \param node Node from which to remove the object.
	 */
	void remove_data(const std::shared_ptr<Object>& obj, 
            const std::shared_ptr<Node>& node);
	
	/** 
	 * Gives the dataset
	 *
	 * \return the dataset that contains the data to allocate.
	 */
	DataSet& get_dataset();

	/** 
	 * Gives the informations about the cluster
	 *
	 * \return the information about the cluster.
	 */
	ClusterInfo& get_cluster_info();

	/** 
	 * Indicates if a node already host an object.
	 *
	 * \param node Specified node
	 * \param obj Specified object
	 * \return true if the object is hosted on the node, 
	 * false in any other case.
	 */
	bool host(const std::shared_ptr<Node>& node, 
            const std::shared_ptr<Object>& obj);

	/**
	 * Return the replication factor requested.
	 *
	 * \return desired replication factor.
	 */
	int get_replication_factor();

public:
	/**
	 * Constructor
	 *
	 * \param conf Configuration of the run
	 */
	AbstractDataDistributionGenerator(Config& conf);

	/**
	 * Destructor
	 */
	virtual ~AbstractDataDistributionGenerator();

	/**
	 * Allocate, function to implement that will distribute the data 
	 * from the dataset to the nodes.
	 */
	virtual void allocate() = 0;

	/**
	 * Create the internal DataDistribution to fill.
	 *
	 * \param cluster_info Information about the cluster.
	 * \param dataset DataSet with all the objects to allocate onto the nodes.
	 */
	void create(ClusterInfo& cluster_info, DataSet& dataset);

	/**
	 * Return the data distribution after the allocation.
	 *
	 * It should not be used by any class inheriting from AbstractDataDistributionGenerator
	 *
	 * \return the data distribution created.
	 */
	std::shared_ptr<DataDistribution> get_data_distribution();

	/**
	 * Divide an object in two parts according the provided size,
	 * and place the new object on the nodes that were hosting the
	 * original object.
	 *
	 * \param obj Original object to divide, will be resized to new_size
	 * \param new_size Size of the first part of the object
	 * \return the second part of the object as an independant one.
	 */
	std::shared_ptr<Object> divide_object_keep_alloc(
            const std::shared_ptr<Object>& obj, 
            ObjectSize new_size);
};

#define REGISTER_DATADISTRIBUTIONGENERATOR(comp) REGISTER_COMPONENT(DataDistributionGenerator, comp)

#endif
