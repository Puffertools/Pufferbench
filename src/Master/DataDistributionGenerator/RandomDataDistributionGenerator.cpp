/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "RandomDataDistributionGenerator.hpp"

#include <map>
#include <cstdlib>

#include "Log.hpp"
#include "Node.hpp"
#include "Object.hpp"
#include "DataSet.hpp"
#include "ClusterInfo.hpp"
#include "DataDistribution.hpp"
#include "Config.hpp"

using namespace std;

REGISTER_DATADISTRIBUTIONGENERATOR(RandomDataDistributionGenerator)

RandomDataDistributionGenerator::RandomDataDistributionGenerator(Config& conf):AbstractDataDistributionGenerator(conf){}

void RandomDataDistributionGenerator::allocate(){
	int r = get_replication_factor();	

	vector<shared_ptr<Node>> nodes = get_cluster_info().get_nodes();
	vector<shared_ptr<Object>> objects = get_dataset().get_objects();
	vector<shared_ptr<Object>>::const_iterator it;
	
	for (it = objects.cbegin(); it != objects.cend(); ++it){
		int replicas = 0;
		while (replicas < r){
			int pos = rand()%nodes.size();
			if (!host(nodes.at(pos),*it)){
				set_data(*it,nodes.at(pos));
				replicas++;
			}
		}
	}
}

