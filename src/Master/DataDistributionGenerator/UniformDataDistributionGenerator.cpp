/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "UniformDataDistributionGenerator.hpp"

#include <cmath>
#include <assert.h>
#include <memory>

#include "Log.hpp"
#include "Node.hpp"
#include "Object.hpp"
#include "DataSet.hpp"
#include "ClusterInfo.hpp"
#include "DataDistribution.hpp"
#include "Config.hpp"
#include "GlobalConfiguration.hpp"
#include "ConfigurationKeys.hpp"

using namespace std;

REGISTER_DATADISTRIBUTIONGENERATOR(UniformDataDistributionGenerator)

UniformDataDistributionGenerator::UniformDataDistributionGenerator(Config& conf):
	AbstractDataDistributionGenerator(conf){}

int UniformDataDistributionGenerator::k_among_n(int k, int n){
	long num = 1;
	long denum = 1;

	for (int i = 1; i <= k; i++){
		num *= (n+1-i);
		denum *= i;
	}
	return (int)(num/denum);
}

void UniformDataDistributionGenerator::allocate(){
	// Get total size of data to distribute
	ObjectSize total_data = get_dataset().get_total_size();
	ObjectSize to_distribute = total_data;

	// get number of possible combinaisons of r nodes
	//int r = get_replication_factor();
	int r = conf.get_parameter<int>({ConfigurationKeys::REPLICATION_KEY},
										ConfigurationKeys::REPLICATION_DEFAULT);
	int nb_nodes = get_cluster_info().get_size();

	LogDistribution("There are "+to_string(nb_nodes)+" nodes and the replication factor is "+ to_string(r));
	int nb_combinaisons = k_among_n(r,nb_nodes);

	LogDistribution("Dividing the data in "+to_string(nb_combinaisons)+" sets.");

	// distribute data
	vector<shared_ptr<Object>> objects = get_dataset().get_objects();
	vector<shared_ptr<Object>>::iterator obj_it = objects.begin();
	shared_ptr<Object> cut;

	vector<vector<shared_ptr<Object>>> object_sets(nb_combinaisons);

	ObjectSize small_set_size = to_distribute / nb_combinaisons;
	LogDistribution("The sets should have a size of "+to_string(small_set_size)+" (+1-0).");

	// Distribute round robin
	for (int i = 0; i < nb_combinaisons; ++i){
		ObjectSize per_grp = ceil((1.0 * to_distribute) / (nb_combinaisons-i));
		ObjectSize added = 0;

		assert(per_grp >= small_set_size);

		// Assign first the last cut item
		if (cut){
			ObjectSize obj_size = cut->get_size();
			if (obj_size > per_grp){
				shared_ptr<Object> ncut = get_dataset().divide_object(cut,per_grp);
				object_sets[i].push_back(cut);
				cut = ncut;
				added = per_grp;
			} else {
				object_sets[i].push_back(cut);
				cut.reset();
				added = obj_size;
			}
		}
		while (obj_it != objects.end() && added < per_grp){
			shared_ptr<Object> obj = *obj_it;
			ObjectSize obj_size = obj->get_size();
			if (obj_size > per_grp-added){
				cut = get_dataset().divide_object(obj,per_grp-added);
				object_sets[i].push_back(obj);
				added += obj->get_size();
				assert(added == per_grp);
			} else {
				object_sets[i].push_back(obj);
				added += obj_size;
			}	
			++obj_it;
		}
		to_distribute -= added;
	}

	// Assign data to set of nodes
	vector<int> pos(r);
	for (int i = 0; i < r; ++i){
		pos[i] = i;
	}

	vector<shared_ptr<Node>> vnodes = get_cluster_info().get_nodes();
	vector<ObjectSize> hosted_per_node(vnodes.size());
	ObjectSize per_node = ceil((1.0*total_data*r)/nb_nodes);
	
	// Number of nodes that must have one more data than the others
	int nb_to_load = (total_data*r) % (per_node-1);

	LogDistribution(to_string(nb_to_load)+" nodes should host "+to_string(per_node)+" of data.");

	int index_start = 0;
	int index_end = object_sets.size()-1;

	for (int i = 0; i < nb_combinaisons; ++i){
		// Will assign a big set of objects 
		bool big_one = true;

		for (int j = 0; j < r; ++j){
			// Remaining amount of data to put on the first node of the set
			ObjectSize to_fill = per_node-hosted_per_node[pos[j]];
			// Number of bigger sized sets to put
			// size(bigger_sets)=1+size(small_sets)
			// So to_fill%small_set_size=nb_ofbig_sets_to_add
			if (to_fill % small_set_size == 0){
				big_one = false;
				break;
			}
		}

		int index;

		if (big_one){
			// Add a big one
			index = index_start;
			index_start++;
		} else {
			// add a small one
			index = index_end;
			index_end--;
		}

		for (int j = 0; j < r; ++j){
			shared_ptr<Node> n = vnodes[pos[j]];
			vector<shared_ptr<Object>>::iterator it;
			for (it = object_sets[index].begin(); it != object_sets[index].end(); ++it){
				set_data(*it,n);
				hosted_per_node[pos[j]]+=(*it)->get_size();
			}
			if (hosted_per_node[pos[j]] == per_node){
				// When a node is full, reduce the number of nodes to fill
				nb_to_load--;
				if (nb_to_load == 0) {
					// When there is no more nodes to fill to the high level
					// Reduce the amount of data to put on a node by 1
					per_node--;
				}
			}
		}

		pos[r-1]++;
		for (int j = r-1; j >= 0; --j){
			if (pos[j]>=nb_nodes-r+1+j){
				if (j>0){
					pos[j-1] = pos[j-1]+1;
					for (int k = j; k < r; ++k){
						pos[k] = pos[k-1]+1;
					}
				} else {
					return ;
				}
			} else {
				break;
			}
		}
	}
}
