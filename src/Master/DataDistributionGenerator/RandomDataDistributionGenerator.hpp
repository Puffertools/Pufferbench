/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef RANDOMDATADISTRIBUTIONGENERATOR_HPP
#define RANDOMDATADISTRIBUTIONGENERATOR_HPP

#include "AbstractDataDistributionGenerator.hpp" 

class Config;

/**
 * \brief Simple implementation of an AbstractDataDistributionGenerator.
 *
 * Allocates data randomly to nodes, ensure the replication 
 * factor and that each replica is on a different node.
 */
class RandomDataDistributionGenerator : public AbstractDataDistributionGenerator {
public:
	/** 
	 * Constructor
	 *
	 * \param conf Configuration of the run
	 */
	RandomDataDistributionGenerator(Config& conf);

	/**
	 * Assign replicas to nodes randomly.
	 * The replication factor in ensured, as well as the fact 
	 * that each replica is assigned to a different node.
	 */
	virtual void allocate();
};

#endif
