/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "ClusterInfo.hpp"

#include <sstream>
#include <assert.h>
#include <string>

#include "FileManager.hpp"
#include "Log.hpp"

#include "Node.hpp"

using namespace std;

ClusterInfo::ClusterInfo(int _size){
	for (int i = 0; i < _size; ++i){
		nodes.push_back(make_shared<Node>(i));
	}
}

ClusterInfo::~ClusterInfo(){}

const vector<shared_ptr<Node>>& ClusterInfo::get_nodes(){
	return nodes;
}

int ClusterInfo::get_size(){
	return nodes.size(); 
}

void ClusterInfo::commission(int _nb_nodes){
	if (decommissioned_nodes>0)
		throw AlreadyInDecommissionException();
	commissioned_nodes += _nb_nodes;
	int size = get_size();
	for (int i = 0; i < _nb_nodes; ++i){
		shared_ptr<Node> nd = make_shared<Node>(size+i);
		nd->mark_in_commission();
		nodes.push_back(nd);
	}
}

void ClusterInfo::decommission_node(const shared_ptr<Node>& node){
	if (commissioned_nodes>0)
		throw AlreadyInCommissionException();
	if (!node->is_in_decommission()){
		LogTrace("Mark node "+to_string(node->get_nodeID())+" to be decommissioned.");
		node->mark_in_decommission();
		decommissioned_nodes++;
	}
}

bool ClusterInfo::is_in_decommission(){
	return decommissioned_nodes>0;
}

bool ClusterInfo::is_in_commission(){
	return commissioned_nodes>0;
}

int ClusterInfo::number_of_commissioned_nodes(){
	return commissioned_nodes;
}

int ClusterInfo::number_of_decommissioned_nodes(){
	return decommissioned_nodes;
}

const char* ClusterInfo::AlreadyInDecommissionException::what() const noexcept {
	return "AlreadyInDecommissionException: A node already in decommission can not be decommissioned again.";
}

const char* ClusterInfo::AlreadyInCommissionException::what() const noexcept {
	return "AlreadyInCommissionException: A node already being commission can not be commissioned again.";
}
