/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "Node.hpp"

using namespace std;

Node::Node(int _id): node_id(_id){
}

Node::~Node(){}

NodeID Node::get_nodeID() const{
	return node_id;
}

void Node::mark_in_decommission(){
	in_decommission = true;
}

bool Node::is_in_decommission(){
	return in_decommission;
}

void Node::mark_in_commission(){
	in_commission = true;
}

bool Node::is_in_commission(){
	return in_commission;
}
