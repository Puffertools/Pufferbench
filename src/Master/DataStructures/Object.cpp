/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "Object.hpp"

using namespace std;

Object::Object(ObjectID _id, ObjectSize _size): object_id(_id), size(_size){}

ObjectID Object::get_objectID(){
	return object_id;
}

ObjectSize Object::get_size(){
	return size;
}

void Object::set_size(ObjectSize _size){
	size = _size;
}
