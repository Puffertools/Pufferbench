/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "DataSet.hpp"

#include <sstream>
#include <assert.h>

#include "FileManager.hpp"
#include "Log.hpp"

#include "Object.hpp"

using namespace std;

DataSet::DataSet() {}

void DataSet::add_object(ObjectSize size){
	stored_objects.push_back(make_shared<Object>(next_unique_objectID,size));
	next_unique_objectID++;
	if (size > max_size)
		max_size = size;
	total_size += size;
}

const vector<shared_ptr<Object>>& DataSet::get_objects(){
	return stored_objects;
}

int DataSet::get_size(){
	return stored_objects.size();
}

ObjectSize DataSet::get_largest_object_size(){
	return max_size;
}

ObjectSize DataSet::get_total_size(){
	return total_size; 
}

shared_ptr<Object> DataSet::divide_object(const shared_ptr<Object>& obj, ObjectSize nsize){
	ObjectSize size = obj->get_size();
	
	assert(nsize < size);

	shared_ptr<Object> sub = make_shared<Object>(next_unique_objectID,size-nsize);
	next_unique_objectID++;
	obj->set_size(nsize);

	// Do not change total size
	stored_objects.push_back(sub);
	
	// recompute max_size if needed
	if (size == max_size) {
		max_size = 0;
		vector<shared_ptr<Object>>::const_iterator it;
		for (it = stored_objects.begin(); it != stored_objects.end(); ++it){
			ObjectSize s = (*it)->get_size(); 
			if (s > max_size){
				max_size = s;
			}
		}		
	}
	// Return the new object
	return sub;
}
