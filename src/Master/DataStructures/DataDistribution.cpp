/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "DataDistribution.hpp"

#include <algorithm>

#include "Node.hpp"
#include "Object.hpp"
#include "ClusterInfo.hpp"
#include "DataSet.hpp"

using namespace std;

DataDistribution::DataDistribution(ClusterInfo& _cluster_info,
	DataSet& _data_set, int replication): 
		cluster_info(_cluster_info),
		dataset(_data_set),
		replication_factor(replication){
}

void DataDistribution::set(const shared_ptr<Object>& obj, const shared_ptr<Node>& node){
	// Look for object on node
	vector<shared_ptr<Object>>& hosted_objects = objects_per_node[node->get_nodeID()];
	vector<shared_ptr<Object>>::iterator it;
	it = find(hosted_objects.begin(),hosted_objects.end(),obj);
	if (it != hosted_objects.end()){
		// Data is on node
		throw DataAlreadyOnNodeException(); 
	}
	hosted_objects.push_back(obj);
	hosted_per_node[node->get_nodeID()] += obj->get_size();
	nodes_per_object[obj->get_objectID()].push_back(node);
}

void DataDistribution::remove(const shared_ptr<Object>& obj, const shared_ptr<Node>& node){
	// Look for object on node
	vector<shared_ptr<Object>>& hosted_objects = objects_per_node[node->get_nodeID()];
	vector<shared_ptr<Object>>::iterator it;
	it = find(hosted_objects.begin(),hosted_objects.end(),obj);
	if (it == hosted_objects.end()){
		// Data is not on node
		throw DataNotPresentOnNodeException(); 
	}
	hosted_objects.erase(it);

	// Remove from nodes_per_object
	
	vector<shared_ptr<Node>>& hosting_nodes = nodes_per_object[obj->get_objectID()];
	vector<shared_ptr<Node>>::iterator it2;
	it2 = find(hosting_nodes.begin(),hosting_nodes.end(),node);
	if (it2 == hosting_nodes.end()){
		// Data is not on node
		throw DataAlreadyOnNodeException(); 
	}
	hosting_nodes.erase(it2);

	hosted_per_node[node->get_nodeID()] -= obj->get_size();	
}

bool DataDistribution::host(const shared_ptr<Node>& node, const shared_ptr<Object>& obj){
	// Look for object on node
	vector<shared_ptr<Object>>& hosted_objects = objects_per_node[node->get_nodeID()];
	vector<shared_ptr<Object>>::iterator it;
	it = find(hosted_objects.begin(),hosted_objects.end(),obj);
	// Check if item is present
	return it != hosted_objects.end();		
}

const vector<shared_ptr<Object>>& DataDistribution::get_objects_on_node(const shared_ptr<Node>& node){
	return objects_per_node[node->get_nodeID()];
}

const vector<shared_ptr<Node>>& DataDistribution::get_nodes_hosting_object(const shared_ptr<Object>& obj){
	return nodes_per_object[obj->get_objectID()];
}

ClusterInfo& DataDistribution::get_cluster_info(){
	return cluster_info;
}

DataSet& DataDistribution::get_dataset(){
	return dataset;
}

ObjectSize DataDistribution::get_amount_data_hosted(const shared_ptr<Node>& node){
	return hosted_per_node[node->get_nodeID()];
}

int DataDistribution::get_replication_factor(){
	return replication_factor;
}

const char* DataDistribution::DataAlreadyOnNodeException::what() const noexcept{
	return "DataAlreadyOnNodeException: Can not put an object on a node that already host it.";
}

const char* DataDistribution::DataNotPresentOnNodeException::what() const noexcept{
	return "DataNotPresentOnNodeException: Can not move an object from a node that does not host it.";
}
