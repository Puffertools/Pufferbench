/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef DATADISTRIBUTION_HPP
#define DATADISTRIBUTION_HPP

#include <map>
#include <vector>
#include <exception>
#include <string>
#include <memory>

#include "Defs.hpp"

class Node;
class Object;
class ClusterInfo;
class DataSet;

class AbstractDataDistributionGenerator;
class AbstractDataTransferScheduler;

// For testing
class DataDistributionTest;

/**
 * \brief Representation of the distribution of Object on Node
 *
 * Class recording the distribution of Object on nodes.
 */
class DataDistribution {
	/// AbstractDataDistributionGenerator can access private functions
	friend AbstractDataDistributionGenerator;
	/// AbstractDataTransferScheduler can access private functions to 
	/// ensure that orders are properly generated.
	friend AbstractDataTransferScheduler;

	/// For testing
	friend DataDistributionTest;	
private:
	/// Objects that are stored on a node
	std::map<NodeID,std::vector<std::shared_ptr<Object>>> objects_per_node;
	
	/// Nodes that store an object
	std::map<ObjectID,std::vector<std::shared_ptr<Node>>> nodes_per_object;
	
	/// Amount of data stored per node
	std::map<NodeID,ObjectSize> hosted_per_node;

	/// Cluster on which the data is distributed.
	ClusterInfo& cluster_info;

	/// DataSet that is distributed on the cluster.
	DataSet& dataset;

	/// Expected replication factor
	const int replication_factor;

	/**
	 * Put an object on a node.
	 * Can only be called by AbstractDataDistributionGenerator, and AbstractDataTransferScheduler.
	 *
	 * Throws DataAlreadyOnNodeException if the data is already on the node.
	 *
	 * \param obj Object to place
	 * \param node Node that must receive the object.
	 */
	void set(const std::shared_ptr<Object>& obj, const std::shared_ptr<Node>& node);
	
	/**
	 * Remove an object from a node.
	 * Can only be called by AbstractDataDistributionGenerator, and AbstractDataTransferScheduler.
	 *
	 * Throws DataNotPresentOnNodeException if the data is not on the node.
	 *
	 * \param obj Object to remove
	 * \param node Node that have the object.
	 */
	void remove(const std::shared_ptr<Object>& obj, const std::shared_ptr<Node>& node);
	
public:
	
	/**
	 * Constructor.
	 *
	 * \param cluster_info Information about the cluster
	 * \param data_set DataSet to distribute on the nodes
	 * \param replication Replication factor of the objects
	 */
	DataDistribution(ClusterInfo& cluster_info, DataSet& data_set, int replication);

	/**
	 * Indicates if a node hosts some object.
	 *
	 * \param node Node to consider
	 * \param obj Object to consided
	 * \return true if the node host the object, false if not.
	 */
	bool host(const std::shared_ptr<Node>& node, const std::shared_ptr<Object>& obj);

	/**
	 * Returns the objects placed on a node.
	 *
	 * \param node Node
	 * \return vector of objects present on the node.
	 */
	const std::vector<std::shared_ptr<Object>>& get_objects_on_node(const std::shared_ptr<Node>& node);

	/**
	 * Returns the list of nodes hosting an object
	 *
	 * \param obj Object
	 * \return vector of the nodes hosting said object.
	 */
    const std::vector<std::shared_ptr<Node>>& get_nodes_hosting_object(const std::shared_ptr<Object>& obj);

	/**
	 * Returns the informations about the cluster.
	 *
	 * \return ClusterInfo of the cluster.
	 */
	ClusterInfo& get_cluster_info();
	
	/**
	 * Returns the DataSet.
	 *
	 * \return the DataSet that must be distributed among nodes.
	 */
	DataSet& get_dataset();

	/**
	 * Returns the amount of data hosted on a specific node.
	 *
	 * \param node Node
	 * \return amount of data stored on the node.
	 */
	ObjectSize get_amount_data_hosted(const std::shared_ptr<Node>& node);

	/**
	 * Return the replication factor requested for the data distribution.
	 *
	 * \return replication factor of the data distribution.
	 */
	int get_replication_factor();


	/**
	 * \brief Raised when data is set on a node that already has it.
	 *
	 * Raised by set(Object*, Node*)
	 */
	class DataAlreadyOnNodeException: public std::exception {
		/**
		 * \return message describing the exception.
		 */
		virtual const char* what() const noexcept;
	};

	/**
	 * \brief Raised when data is removed from a node that does not have it.
	 *
	 * Raised by remove(Object*,Node*)
	 */
	class DataNotPresentOnNodeException: public std::exception {
		/**
		 * \return message describing the exception.
		 */
		virtual const char* what() const noexcept;
	};
};

#endif
