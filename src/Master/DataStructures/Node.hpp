/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef NODE_HPP
#define NODE_HPP

#include "Defs.hpp"

struct Order;

/**
 * \brief Description of a Node.
 *
 * Class describing a node for the Master side.
 */
class Node{
private:
	/// Id of the node, should be unique
	const NodeID node_id;
	
	/// Mark if the node is in decommission
	bool in_decommission = false;

	/// Mark if the node is newly commissioned
	bool in_commission = false;

public:
	/**
	 * Constructor
	 *
	 * \param id NodeID of the node, must be unique
	 */
	Node(int id);
	
	/**
	 * Destructor
	 */
	~Node();

	/** 
	 * Returns the nodeID of this node.
	 *
	 * \return NodeID of the node.
	 */
	NodeID get_nodeID() const;

	/**
	 * Indicates whether the node is marked as in decommission or not.
	 *
	 * \return whether the node is in-decommission or not.
	 */
	bool is_in_decommission();

	/**
	 * Mark the node as in decommission.
	 */
	void mark_in_decommission();

	/**
	 * Indicates whether the node is marked as in commission or not.
	 *
	 * \return whether the node is in commission or not.
	 */
	bool is_in_commission();

	/**
	 * Mark the node as in commission.
	 */
	void mark_in_commission();
};
#endif
