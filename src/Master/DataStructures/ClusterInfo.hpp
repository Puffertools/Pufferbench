/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef CLUSTERINFO_HPP
#define CLUSTERINFO_HPP

#include <vector>
#include <exception>
#include <memory>

#include "Defs.hpp"

class Node;

/** 
 * \brief Informations about the cluster
 *
 * General informations about the cluster: which nodes are in it,
 * is it in a commission phase or a decommission phase, etc...
 */
class ClusterInfo {
private:
	/// Vector of nodes in the cluster
	/// Warning: each NodeID must match its position in 
	/// the vector
	std::vector<std::shared_ptr<Node>> nodes;

	/// Number of decommissioned nodes
	int decommissioned_nodes = 0;
	/// Number of commissioned nodes
	int commissioned_nodes = 0;

public:
	/**
	 * Create a cluster and populate it with 
	 * nodes.
	 *
	 * \param _size Number of nodes to put in the cluster.
	 */
	ClusterInfo(int _size);
	
	/**
	 * Destructor.
	 * Clean the nodes created by the constructor.
	 */
	~ClusterInfo();

	/**
	 * Returns all the nodes present in the cluster. 
	 *
	 * \return the vector of nodes.
	 */
	const std::vector<std::shared_ptr<Node>>& get_nodes();

	/**
	 * Returns the size of the cluster.
	 *
	 * \return the number of nodes.
	 */
	int get_size();

	/**
	 * Add some empty nodes to the cluster
	 *
	 * \param _nb_nodes number of nodes to commission
	 */
	void commission(int _nb_nodes);

	/**
	 * Decommission a node
	 *
	 * \param node Node to decommission
	 */
	void decommission_node(const std::shared_ptr<Node>& node);

	/**
	 * Indicates if the cluster is in a decommission phase.
	 *
	 * \return true if a decommission is expected
	 */ 
	bool is_in_decommission();
	
	/**
	 * Indicates if the cluster is in a commission phase.
	 *
	 * \return true if a commission is expected
	 */ 
	bool is_in_commission();

	/**
	 * Gives the number of commissionned nodes.
	 *
	 * \return number of commissioned nodes.
	 */
	int number_of_commissioned_nodes();
	
	/** 
	 * Gives the number of commissioned nodes.
	 *
	 * \return the number of commissioned nodes.
	 */
	int number_of_decommissioned_nodes();

	/**
	 * \brief Exception raised when a commission is done during a decommision.
	 *
	 * Exception that is raised when trying to commission nodes to a cluster that
	 * is in decommission.
	 */
	class AlreadyInDecommissionException: public std::exception{
		/**
		 * \return message describing the exception.
		 */
		virtual const char* what() const noexcept;
	};
	
	/**
	 * \brief Exception raised when a decommission is done during a commision.
	 *
	 * Exception that is raised when trying to decommission nodes from a cluster that
	 * is in commission.
	 */
	class AlreadyInCommissionException: public std::exception{
		/**
		 * \return message describing the exception.
		 */
		virtual const char* what() const noexcept;
	};
};

#endif
