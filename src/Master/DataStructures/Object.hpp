/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef OBJECT_HPP
#define OBJECT_HPP

#include <string>

#include "Defs.hpp"

class DataSet;

/**
 * \brief Representation of an object
 *
 * Representation of an object for the master.
 */
class Object {
private:
	/// Only class allowed to change the size of the object
	friend DataSet;
	
	/// ObjectID of this object, should be unique
	const ObjectID object_id;

	/// Size of the object
	ObjectSize size;

	/**
	 * Change the size of the object.
	 * It should only be used to cut objects in parts.
	 * That can only be done by DataSet.
	 * 
	 * \param size New size of the object.
	 */
	void set_size(ObjectSize size);

public:
	
	/**
	 * Constructor
	 *
	 * \param id ID of the Object, should be unique
	 * \param size Size of the object
	 */
	Object(ObjectID id,ObjectSize size);

	/**
	 * Return the Id of the object
	 * \return id of the object.
	 */
	ObjectID get_objectID();
	
	/** 
	 * Return the size of the object.
	 * \return size of the object.
	 */
	ObjectSize get_size();
};

#endif
