/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef SLAVE_HPP
#define SLAVE_HPP

class Config;

/**
 * \brief Manager of a slave during the replay
 */
class Slave{
public:
	/**
	 * Run a slave 
	 *
	 * \param conf Configuration of the run
	 */
	void run(Config& conf);
};


#endif
