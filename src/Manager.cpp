/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "Manager.hpp"

#include <mpi.h>
#include <memory>

#include "Log.hpp"
#include "GlobalConfiguration.hpp"
#include "Config.hpp"
#include "ConfigurationKeys.hpp"

#include "Master.hpp"
#include "Slave.hpp"

#include "FileManager.hpp"

using namespace std;

Manager::Manager(int _log_levels, const string& _result_file):
					log_levels(_log_levels), result_file(_result_file){}


void Manager::run_one(Config& conf){
	int wsize,rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD,&wsize);	


	if (rank == 0){
		LogTrace("Creating output directory on the master.");

		string dir_name = "0000000000";
		string tmp_dir_name = to_string(conf.get_experiment_number());

		if (dir_name.length() > tmp_dir_name.length()){
			dir_name.replace(dir_name.length()-tmp_dir_name.length(),tmp_dir_name.length(),tmp_dir_name);
		} else {
			dir_name = tmp_dir_name;
		}

		FileManager::instance()->open_current_dir(dir_name);
	
		FileManager::instance()->add_run_log(log_levels);

		// Print simuconf into file
		FileManager::instance()->print_file("configuration.yml",conf.get_yaml());
	}

	LogImportant("----------------------------------------------");
	LogImportant("Running simulation "+to_string(conf.get_experiment_number())+
					"/"+to_string(total_nb_experiments));
	LogImportant("----------------------------------------------");


	// Record exp in the configuration
	conf.log();

	bool run_replays = conf.get_parameter<bool>({ConfigurationKeys::RUN_REPLAYS_KEY}, 
													ConfigurationKeys::RUN_REPLAYS_DEFAULT);

	if (run_replays){
		LogInfo("Replaying the data transfers on the nodes.");
	} else {
		LogInfo("No replay of the data transfers.");
	}

	if (run_replays){
		int nb_delta = conf.get_parameter<int>({ConfigurationKeys::RUN_SECTION,
													ConfigurationKeys::DELTA_NODES_KEY},
													ConfigurationKeys::DELTA_NODES_DEFAULT);
		int nb_initial = conf.get_parameter<int>({ConfigurationKeys::RUN_SECTION,
															ConfigurationKeys::INITIAL_NUMBER_KEY},
															ConfigurationKeys::INITIAL_NUMBER_DEFAULT);
		int replication = conf.get_parameter<int>({ConfigurationKeys::REPLICATION_KEY},
															ConfigurationKeys::REPLICATION_DEFAULT);
		// Check if there is something to do
		if (nb_delta == 0){
			LogWarn("The configuration of the run is invalid:");
			LogWarn(" | - No nodes are commissioned or decommissioned.");
			LogWarn("Skipping this run.");
			if (rank == 0)
				FileManager::instance()->close_run_log();
			return;
		}

		// Check world size
		if ( nb_initial + nb_delta <= 0){
			LogWarn("The configuration of the run is invalid:");
			LogWarn(" | - Initial number of nodes: "+to_string(nb_initial));
			LogWarn(" | - Delta nodes: "+to_string(nb_delta));
			LogWarn("Skipping this run.");
			if (rank == 0)
				FileManager::instance()->close_run_log();
			return;
		}
		// Check world size
		if (nb_initial+nb_delta>wsize){
			LogWarn("The configuration of the run is invalid:");
			LogWarn(" | - Initial number of nodes: "+to_string(nb_initial));
			LogWarn(" | - Delta nodes: "+to_string(nb_delta));
			LogWarn(" | - Total available physical nodes: "+to_string(wsize));
			LogWarn("Skipping this run.");
			if (rank == 0)
				FileManager::instance()->close_run_log();
			return;
		}
		// If replication factor can be kept
		if (nb_initial+nb_delta < replication){
			LogWarn("The replication factor cannot be ensured for this run due to the configuration:");
			LogWarn(" | - Initial number of nodes: "+to_string(nb_initial));
			LogWarn(" | - Delta nodes: "+to_string(nb_delta));
			LogWarn(" | - Replication factor: "+to_string(replication));
			if (rank == 0)
				FileManager::instance()->close_run_log();
			return ;
		}
	}
	// Set the random seed
	int seed = conf.get_parameter({ConfigurationKeys::SEEDS_KEY},ConfigurationKeys::SEEDS_DEFAULT);
	srand(seed);
	LogInfo("Random number generator seed set to "+to_string(seed));
	
	Log::instance()->flush();
	if (rank == 0){
		Master master;
		
		double duration = master.run(conf,run_replays);
	
		if (run_replays){
			string res = conf.get_csv_header() + ",duration\n";
			res += conf.get_csv() + "," + to_string(duration);
			FileManager::instance()->print_file("results.csv",res);	
	
			string content = conf.get_csv()+","+to_string(duration);
			FileManager::instance()->print_global_file(result_file,content);
		}

		FileManager::instance()->close_run_log();
	} else {
		if (run_replays){
			Slave slave;
			slave.run(conf);
		}
	}
	Log::instance()->flush();
}

void Manager::run(GlobalConfiguration& gconf,int start_from){
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int exp_nb = -1;
	
	int start = 0;
	if (rank == 0) {
		start = start_from;
	}

	// Send the starting exp to all nodes
	MPI_Bcast(&start,1,MPI_INT,0,MPI_COMM_WORLD);

	bool output_initialized = false;

	gconf.reset();

	total_nb_experiments = gconf.get_number_configurations();

	LogTrace("Starting to iterate on the configuration.");
	LogInfo("Starting from experiment "+to_string(start_from));


	while (gconf.has_next()){
		exp_nb++;
		Config conf = gconf.get_next_configuration();
		
		if (exp_nb >= start){
			conf.set_experiment_number(exp_nb);
	
			if (rank == 0 && !output_initialized){
				string content = conf.get_csv_header()+",duration";
				FileManager::instance()->print_global_file(result_file,content);
				output_initialized = true;
			}
	
			run_one(conf);	
		}
	}

	LogTrace("All configuration options have been launched, terminating.");
}


